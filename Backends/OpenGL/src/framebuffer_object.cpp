/**
 * @file framebuffer_object.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the framebuffer_object.hpp header.
 */

#include "SimpleGL_gl/framebuffer_object.hpp"

namespace sgl {
    Renderbuffer* Renderbuffer::current = nullptr;

    Renderbuffer::Renderbuffer(void) {
        glGenRenderbuffers(1, &handle);
    }

    Renderbuffer::~Renderbuffer(void) {
        glDeleteRenderbuffers(1, &handle);

        /* Set current to nullptr if this was the bound renderbuffer object */
        if (Renderbuffer::current == this) {
            Renderbuffer::current = nullptr;
        }
    }

    GLuint Renderbuffer::getHandle() {
        return handle;
    }

    void Renderbuffer::bind() {
        /* If already bound return immediately */
        if (Renderbuffer::current == this) {
            return;
        }

        /* Bind renderbuffer object */
        glBindRenderbuffer(GL_RENDERBUFFER, handle);
        Renderbuffer::current = this;
    }

    void Renderbuffer::storage(int width, int height) {
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
    }

    FramebufferGL* FramebufferGL::current = nullptr;

    FramebufferGL::FramebufferGL(void) {
        glGenFramebuffers(1, &handle);
    }

    FramebufferGL::~FramebufferGL(void) {
        glDeleteFramebuffers(1, &handle);

        /* Set current to nullptr if this was the bound framebuffer object */
        if (FramebufferGL::current == this) {
            FramebufferGL::current = nullptr;
        }
    }

    GLuint FramebufferGL::getHandle() {
        return handle;
    }

    void FramebufferGL::bind() {
        /* If already bound return immediately */
        if (FramebufferGL::current == this) {
            return;
        }

        /* Bind framebuffer object */
        glBindFramebuffer(GL_FRAMEBUFFER, handle);
        FramebufferGL::current = this;
    }

    void FramebufferGL::setTexture(Texture* texture) {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture->getHandle(), 0);
    }

    void FramebufferGL::setRenderbuffer(Renderbuffer* buffer) {
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, buffer->getHandle());
    }
}
