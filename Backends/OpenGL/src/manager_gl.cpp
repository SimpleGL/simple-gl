/**
 * @file manager_gl.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the manager_gl.hpp header.
 */

#include "SimpleGL_gl/manager_gl.hpp"

#include <fstream>
#include <map>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <SimpleGL/logging.hpp>

using namespace std;

namespace sgl {

    void TextureManager::remove(std::string name) {
        /* Dispose Texture */
        Texture* texture = get(name);
        delete texture;

        /* Call base method */
        Manager::remove(name);
    }

    void TextureManager::clear() {
        /* Dispose all textures */
        for (map<string, Texture*>::iterator it = objects.begin(); it != objects.end(); it++) {
            Texture* texture = it->second;
            delete texture;
        }

        /* Call base method */
        Manager::clear();
    }

    Texture* TextureManager::load(std::string path, std::string name) {
        /* Load bitmap file and create image */
        stbi_set_flip_vertically_on_load(true);
        TextureData image;
        image.pixels = stbi_load(path.c_str(), &image.width, &image.height, &image.components, 4);

        /* Generate Texture */
        Texture* texture = new Texture();
        texture->bind();

        /* Set texture parameters */
        texture->setWrapS(TextureWrap::CLAMP_TO_BORDER);
        texture->setWrapT(TextureWrap::CLAMP_TO_BORDER);
        texture->setMinFilter(TextureFilter::LINEAR);
        texture->setMagFilter(TextureFilter::LINEAR);

        /* Upload texture data */
        texture->uploadImage(image);
        stbi_image_free(image.pixels);

        /* Put texture in storage and return it */
        put(name, texture);
        return texture;
    }

    void ShaderManager::remove(std::string name) {
        /* Dispose Shader */
        Shader* shader = get(name);
        delete shader;

        /* Call base method */
        Manager::remove(name);
    }

    void ShaderManager::clear() {
        /* Dispose all shaders */
        for (map<string, Shader*>::iterator it = objects.begin(); it != objects.end(); it++) {
            Shader* shader = it->second;
            delete shader;
        }

        /* Call base method */
        Manager::clear();
    }

    Shader* ShaderManager::loadShader(std::string path, std::string name, ShaderType type) {
        /* Opening file stream */
        ifstream fileStream(path);
        if (fileStream.is_open()) {
            /* Reading line by line */
            string source;
            while (fileStream.good()) {
                string line;
                getline(fileStream, line);
                source.append(line + "\n");
            }
            fileStream.close();

            /* Generate Shader */
            Shader* shader = new Shader(type);

            /* Compile Shader */
            shader->source(source);
            shader->compile();
            if (!shader->getCompileStatus()) {
                Logger::logError(shader->getInfoLog());
            }

            /* Store shader and return it */
            put(name, shader);
            return shader;
        } else {
            Logger::logError("Could not open file " + path);
            return new Shader(type);
        }
    }

    ShaderProgram* ShaderManager::loadShaderProgram(std::string vertexPath, std::string fragmentPath, std::string vertexName, std::string fragmentName) {
        /* Load shaders */
        Shader* vertexShader = loadShader(vertexPath, vertexName, ShaderType::VERTEX_SHADER);
        Shader* fragmentShader = loadShader(fragmentPath, fragmentName, ShaderType::FRAGMENT_SHADER);

        /* Link shaders */
        ShaderProgram* shaderProgram = new ShaderProgram();
        shaderProgram->attach(vertexShader);
        shaderProgram->attach(fragmentShader);
        shaderProgram->link();

        /* Check status */
        if (!shaderProgram->getLinkStatus()) {
            Logger::logError(shaderProgram->getInfoLog());
        }

        /* Return program */
        return shaderProgram;
    }

    ShaderProgram* ShaderManager::createShaderProgram(std::string vertexName, std::string fragmentName) {
        /* Get shaders */
        Shader* vertexShader = get(vertexName);
        Shader* fragmentShader = get(fragmentName);

        /* Link shaders */
        ShaderProgram* shaderProgram = new ShaderProgram();
        shaderProgram->attach(vertexShader);
        shaderProgram->attach(fragmentShader);
        shaderProgram->link();

        /* Check status */
        if (!shaderProgram->getLinkStatus()) {
            Logger::logError(shaderProgram->getInfoLog());
        }

        /* Return program */
        return shaderProgram;
    }
}
