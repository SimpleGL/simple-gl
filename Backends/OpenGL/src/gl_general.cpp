/**
 * @file gl_general.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the gl_general.hpp header.
 */

#include "SimpleGL_gl/gl_general.hpp"

#include <string>

#include <SimpleGL/logging.hpp>

#include "SimpleGL_gl/backend_gl.hpp"

using namespace std;
using namespace glm;

namespace sgl {

    GL::GL(void) {
        /* Nothing to do here */
    }

    void GL::checkError() {
        GLenum error;
        while ((error = glGetError()) != GL_NO_ERROR) {
            Logger::logError("OpenGL error " + to_string(error) + ":");
            switch (error) {
                case GL_INVALID_ENUM:
                    Logger::logError("GL_INVALID_ENUM: An unacceptable value is specified for an enumerated argument.");
                    break;

                case GL_INVALID_VALUE:
                    Logger::logError("GL_INVALID_VALUE: A numeric argument is out of range.");
                    break;

                case GL_INVALID_OPERATION:
                    Logger::logError("GL_INVALID_OPERATION: The specified operation is not allowed in the current state.");
                    break;

                case GL_INVALID_FRAMEBUFFER_OPERATION:
                    Logger::logError("GL_INVALID_FRAMEBUFFER_OPERATION: The framebuffer object is not complete.");
                    break;

                case GL_OUT_OF_MEMORY:
                    Logger::logError("GL_OUT_OF_MEMORY: There is not enough memory left to execute the command.");
                    break;
            }
        }
    }

    void GL::enable(Capability cap) {
        glEnable(to_GLenum(cap));
    }

    void GL::disable(Capability cap) {
        glDisable(to_GLenum(cap));
    }

    void GL::clearColor(float r, float g, float b, float a) {
        clearColor(vec4(r, g, b, a));
    }

    void GL::clearColor(glm::vec4 color) {
        /* Values need to be normalized */
        vec4 normal = normalize(color);
        glClearColor(normal.r, normal.g, normal.b, normal.a);
    }

    void GL::clear(ClearBit mask) {
        glClear(to_GLbitfield(mask));
    }

    void GL::drawArrays(Primitive mode, int first, int count) {
        glDrawArrays(to_GLenum(mode), first, count);
    }

    void GL::blendFunc(BlendFactor source, BlendFactor destination) {
        glBlendFunc(to_GLenum(source), to_GLenum(destination));
    }

    void GL::cullFace(CullFaceMode mode) {
        glCullFace(to_GLenum(mode));
    }

    void GL::depthFunc(DepthFunction func) {
        glDepthFunc(to_GLenum(func));
    }
}
