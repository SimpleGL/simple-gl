/**
 * @file application_gl.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the application_gl.hpp header.
 */

#include "SimpleGL_gl/application_gl.hpp"

#include "SimpleGL_gl/gl_general.hpp"
#include "SimpleGL_gl/glfw_window_gl.hpp"

namespace sgl {

    ApplicationGL::ApplicationGL(int width, int height, std::string title) : Application() {
        window = new WindowGL(width, height, title);
        batch = new BatchGL();
        textureManager = new TextureManager();
        shaderManager = new ShaderManager();
    }

    ApplicationGL::~ApplicationGL(void) {
        /* Delete Texture Manager */
        delete textureManager;

        /* Delete Shader Manager */
        delete shaderManager;

        /* Delete batch */
        delete batch;
    }

    void ApplicationGL::clear() {
        GL::clear(ClearBit::COLOR_BUFFER_BIT | ClearBit::DEPTH_BUFFER_BIT);
    }

    void ApplicationGL::dispose() {
        textureManager->clear();
        shaderManager->clear();
    }
}
