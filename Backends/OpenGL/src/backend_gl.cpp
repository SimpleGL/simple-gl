/**
 * @file backend_gl.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the backend_gl.hpp header.
 */

#include "SimpleGL_gl/backend_gl.hpp"

#include <cstdlib>

#include <GLFW/glfw3.h>

#include <SimpleGL/logging.hpp>

using namespace std;

namespace sgl {
    GLVersion contextVersion;

    GLVersion::GLVersion() : major{1}, minor{0}
    {
        /* Nothing to do here */
    }

    GLVersion::GLVersion(int major, int minor) : major{major}, minor{minor}
    {
        /* Nothing to do here */
    }

    bool GLVersion::operator<(const GLVersion& other) {
        return (major < other.major) || ((major == other.major) && (minor < other.minor));
    }

    bool GLVersion::operator<=(const GLVersion& other) {
        return (*this < other) || (*this == other);
    }

    bool GLVersion::operator==(const GLVersion& other) {
        return (major == other.major) && (minor == other.minor);
    }

    bool GLVersion::operator!=(const GLVersion& other) {
        return !(*this == other);
    }

    bool GLVersion::operator>=(const GLVersion& other) {
        return (*this > other) || (*this == other);
    }

    bool GLVersion::operator>(const GLVersion& other) {
        return (major > other.major) || ((major == other.major) && (minor > other.minor));
    }

    void initGL() {
        /* Create dummy context for checking extensions */
        Logger::logInfo("Creating dummy GL context...");
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
        GLFWwindow* context = glfwCreateWindow(1, 1, "", NULL, NULL);
        if (!context) {
            Logger::logError("Failed to create a GL context!");
            glfwTerminate();
            exit(EXIT_FAILURE);
        }
        glfwMakeContextCurrent(context);

        /* Initialize GLEW */
        Logger::logInfo("Initializing GLEW...");
        glewExperimental = GL_TRUE;
        if (glewInit() != GLEW_OK) {
            Logger::logError("Could not init GLEW!");
            exit(EXIT_FAILURE);
        }

        Logger::logInfo("Checking OpenGL version...");
        /* OpenGL 3.2 */
        if (GLEW_VERSION_3_2) {
            Logger::logInfo("OpenGL 3.2 is supported!");
            GLVersion version(3, 2);
        } else {
            Logger::logError("OpenGL 3.2 is not supported, you may want to update your graphics driver.");
            exit(EXIT_FAILURE);
        }

        Logger::logInfo("Checking extensions...");
        /* ARB_vertex_buffer_object */
        if (GLEW_ARB_vertex_buffer_object) {
            Logger::logInfo("Vertex Buffer Objects are supported!");
        } else {
            Logger::logError("Vertex Buffer Objects aren't supported!");
            exit(EXIT_FAILURE);
        }

        /* ARB_map_buffer_range */
        if (GLEW_ARB_map_buffer_range) {
            Logger::logInfo("Mapping buffer ranges are supported!");
        } else {
            Logger::logError("Mapping buffer ranges aren't supported!");
            exit(EXIT_FAILURE);
        }

        /* ARB_vertex_array_object */
        if (GLEW_ARB_vertex_array_object) {
            Logger::logInfo("Vertex Array Objects are supported!");
        } else {
            Logger::logError("Vertex Array Objects aren't supported!");
            exit(EXIT_FAILURE);
        }

        /* ARB_shader_objects */
        if (GLEW_ARB_shader_objects) {
            Logger::logInfo("Shader Objects are supported!");
        } else {
            Logger::logError("Shader Objects aren't supported!");
            exit(EXIT_FAILURE);
        }

        /* ARB_vertex_shader */
        if (GLEW_ARB_vertex_shader) {
            Logger::logInfo("Vertex Shaders are supported!");
        } else {
            Logger::logError("Vertex Shaders aren't supported!");
            exit(EXIT_FAILURE);
        }

        /* ARB_fragment_shader */
        if (GLEW_ARB_fragment_shader) {
            Logger::logInfo("Fragment Shaders are supported!");
        } else {
            Logger::logError("Fragment Shaders aren't supported!");
            exit(EXIT_FAILURE);
        }

        /* ARB_framebuffer_object */
        if (GLEW_ARB_framebuffer_object) {
            Logger::logInfo("Framebuffers are supported!");
        } else {
            Logger::logError("Framebuffers aren't supported!");
            exit(EXIT_FAILURE);
        }

        /* Destroy dummy context */
        glfwDestroyWindow(context);
    }
}
