/**
 * @file vertex_buffer_object.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the vertex_buffer_object.hpp header.
 */

#include "SimpleGL_gl/vertex_buffer_object.hpp"

#include "SimpleGL_gl/backend_gl.hpp"

using namespace std;

namespace sgl {
    Buffer* Buffer::current = nullptr;

    Buffer::Buffer(void) {
        glGenBuffers(1, &handle);
    }

    Buffer::~Buffer(void) {
        glDeleteBuffers(1, &handle);

        /* Set current to nullptr if this was the bound vertex buffer object */
        if (Buffer::current == this) {
            Buffer::current = nullptr;
        }
    }

    GLuint Buffer::getHandle() {
        return handle;
    }

    void Buffer::bind(BufferTarget target) {
        /* If already bound return immediately */
        if (Buffer::current == this) {
            return;
        }

        /* Bind buffer object */
        glBindBuffer(to_GLenum(target), handle);
        Buffer::current = this;
    }

    void Buffer::uploadData(BufferTarget target, long size, float* data, BufferUsage usage) {
        glBufferData(to_GLenum(target), size, data, to_GLenum(usage));
    }

    void Buffer::uploadData(BufferTarget target, long size, std::vector<float> data, BufferUsage usage) {
        glBufferData(to_GLenum(target), size, data.data(), to_GLenum(usage));
    }

    void Buffer::uploadData(BufferTarget target, long size, std::vector<glm::vec2> data, BufferUsage usage) {
        glBufferData(to_GLenum(target), size, data.data(), to_GLenum(usage));
    }

    void Buffer::uploadData(BufferTarget target, long size, std::vector<glm::vec3> data, BufferUsage usage) {
        glBufferData(to_GLenum(target), size, data.data(), to_GLenum(usage));
    }

    void Buffer::uploadData(BufferTarget target, long size, std::vector<glm::vec4> data, BufferUsage usage) {
        glBufferData(to_GLenum(target), size, data.data(), to_GLenum(usage));
    }

    void Buffer::uploadData(BufferTarget target, long size, BufferUsage usage) {
        glBufferData(to_GLenum(target), size, nullptr, to_GLenum(usage));
    }

    void Buffer::uploadSubData(BufferTarget target, long offset, long size, float* data) {
        glBufferSubData(to_GLenum(target), offset, size, data);
    }

    void Buffer::uploadSubData(BufferTarget target, long offset, long size, std::vector<float> data) {
        glBufferSubData(to_GLenum(target), offset, size, data.data());
    }

    void Buffer::uploadSubData(BufferTarget target, long offset, long size, std::vector<glm::vec2> data) {
        glBufferSubData(to_GLenum(target), offset, size, data.data());
    }

    void Buffer::uploadSubData(BufferTarget target, long offset, long size, std::vector<glm::vec3> data) {
        glBufferSubData(to_GLenum(target), offset, size, data.data());
    }

    void Buffer::uploadSubData(BufferTarget target, long offset, long size, std::vector<glm::vec4> data) {
        glBufferSubData(to_GLenum(target), offset, size, data.data());
    }

    void* Buffer::map(BufferTarget target, MapAccess access) {
        return glMapBuffer(to_GLenum(target), to_GLenum(access));
    }

    void* Buffer::mapRange(BufferTarget target, long offset, long length, MapRangeAccess access) {
        return glMapBufferRange(to_GLenum(target), offset, length, to_GLbitfield(access));
    }

    void Buffer::unmap(BufferTarget target) {
        glUnmapBuffer(to_GLenum(target));
    }
}
