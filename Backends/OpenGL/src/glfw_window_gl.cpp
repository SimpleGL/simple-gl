/**
 * @file glfw_window_gl.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the glfw_window_gl.hpp header.
 */

#include "SimpleGL_gl/glfw_window_gl.hpp"

#include <stdexcept>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <SimpleGL/glfw_callback.hpp>

using namespace std;

namespace sgl {

    WindowGL::WindowGL(int width, int height, std::string title, bool vsync) : Window() {
        size.x = width;
        size.y = height;
        this->title = title;

        /* Reset to default window hints */
        glfwDefaultWindowHints();

        /* Not resizeable */
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        /* Setting window hints to get an OpenGL 3.2+ core profile context */
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

        /* Creating the GLFW window */
        handle = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
        if (!handle) {
            throw runtime_error("Could not create window!");
        }

        /* Make the OpenGL context current for the calling thread */
        glfwMakeContextCurrent(handle);

        /* Initialize GLEW */
        glewExperimental = GL_TRUE;
        if (glewInit() != GLEW_OK) {
            throw runtime_error("Could not init GLEW for the current GL context!");
        }

        /* Center window to screen */
        centerWindow();

        /* Set vertical synchronization if requested */
        setVSync(vsync);

        /* Set default callbacks */
        KeyCallback* keyCallback = new KeyCallback();
        setKeyCallback(keyCallback);
        MouseButtonCallback* mouseButtonCallback = new MouseButtonCallback();
        setMouseButtonCallback(mouseButtonCallback);
    }

    void WindowGL::setVSync(bool vsync) {
        if (vsync) {
            glfwSwapInterval(1);
        } else {
            glfwSwapInterval(0);
        }
    }

    void WindowGL::swapBuffers() {
        glfwSwapBuffers(handle);
    }
}
