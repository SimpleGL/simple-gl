/**
 * @file shader_objects.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the shader_objects.hpp header.
 */

#include "SimpleGL_gl/shader_objects.hpp"

#include <vector>

#include <glm/gtc/type_ptr.hpp>

#include "SimpleGL_gl/backend_gl.hpp"

using namespace std;
using namespace glm;

namespace sgl {

    Shader::Shader(ShaderType type) {
        handle = glCreateShader(to_GLenum(type));
    }

    Shader::~Shader(void) {
        glDeleteShader(handle);
    }

    GLuint Shader::getHandle() {
        return handle;
    }

    void Shader::source(std::string source) {
        const char* c_str = source.c_str();
        glShaderSource(handle, 1, &c_str, nullptr);
    }

    void Shader::compile() {
        glCompileShader(handle);
    }

    bool Shader::getCompileStatus() {
        GLint status;
        glGetShaderiv(handle, GL_COMPILE_STATUS, &status);
        return status == GL_TRUE;
    }

    std::string Shader::getInfoLog() {
        GLint logSize;
        glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logSize);
        vector<char> message(logSize);
        glGetShaderInfoLog(handle, logSize, NULL, message.data());
        return string(message.data());
    }

    ShaderProgram* ShaderProgram::current = nullptr;

    ShaderProgram::ShaderProgram(void) {
        handle = glCreateProgram();
    }

    ShaderProgram::~ShaderProgram(void) {
        glDeleteProgram(handle);

        /* Set current to nullptr if this was the used shader program */
        if (ShaderProgram::current == this) {
            ShaderProgram::current = nullptr;
        }
    }

    GLuint ShaderProgram::getHandle() {
        return handle;
    }

    void ShaderProgram::attach(Shader* shader) {
        glAttachShader(handle, shader->getHandle());
    }

    void ShaderProgram::link() {
        glLinkProgram(handle);
    }

    void ShaderProgram::use() {
        /* If already used return immediately */
        if (ShaderProgram::current == this) {
            return;
        }

        /* Use shader program */
        glUseProgram(handle);
        ShaderProgram::current = this;
    }

    bool ShaderProgram::getLinkStatus() {
        GLint status;
        glGetProgramiv(handle, GL_LINK_STATUS, &status);
        return status == GL_TRUE;
    }

    std::string ShaderProgram::getInfoLog() {
        GLint logSize;
        glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &logSize);
        vector<char> message(logSize);
        glGetProgramInfoLog(handle, logSize, NULL, message.data());
        return string(message.data());
    }

    void ShaderProgram::bindFragmentDataLocation(unsigned int colorNumber, std::string name) {
        glBindFragDataLocation(handle, colorNumber, name.c_str());
    }

    int ShaderProgram::getAttributeLocation(std::string name) {
        return glGetAttribLocation(handle, name.c_str());
    }

    int ShaderProgram::getUniformLocation(std::string name) {
        return glGetUniformLocation(handle, name.c_str());
    }

    void ShaderProgram::setUniform(int location, int value) {
        glUniform1i(location, value);
    }

    void ShaderProgram::setUniform(int location, float value) {
        glUniform1f(location, value);
    }

    void ShaderProgram::setUniform(int location, glm::vec2 value) {
        glUniform2fv(location, 1, value_ptr(value));
    }

    void ShaderProgram::setUniform(int location, glm::vec3 value) {
        glUniform3fv(location, 1, value_ptr(value));
    }

    void ShaderProgram::setUniform(int location, glm::vec4 value) {
        glUniform4fv(location, 1, value_ptr(value));
    }

    void ShaderProgram::setUniform(int location, glm::mat2 value) {
        glUniformMatrix2fv(location, 1, GL_FALSE, value_ptr(value));
    }

    void ShaderProgram::setUniform(int location, glm::mat3 value) {
        glUniformMatrix3fv(location, 1, GL_FALSE, value_ptr(value));
    }

    void ShaderProgram::setUniform(int location, glm::mat4 value) {
        glUniformMatrix4fv(location, 1, GL_FALSE, value_ptr(value));
    }

    void ShaderProgram::setUniform(std::string name, int value) {
        int location = getUniformLocation(name);
        setUniform(location, value);
    }

    void ShaderProgram::setUniform(std::string name, float value) {
        int location = getUniformLocation(name);
        setUniform(location, value);
    }

    void ShaderProgram::setUniform(std::string name, glm::vec2 value) {
        int location = getUniformLocation(name);
        setUniform(location, value);
    }

    void ShaderProgram::setUniform(std::string name, glm::vec3 value) {
        int location = getUniformLocation(name);
        setUniform(location, value);
    }

    void ShaderProgram::setUniform(std::string name, glm::vec4 value) {
        int location = getUniformLocation(name);
        setUniform(location, value);
    }

    void ShaderProgram::setUniform(std::string name, glm::mat2 value) {
        int location = getUniformLocation(name);
        setUniform(location, value);
    }

    void ShaderProgram::setUniform(std::string name, glm::mat3 value) {
        int location = getUniformLocation(name);
        setUniform(location, value);
    }

    void ShaderProgram::setUniform(std::string name, glm::mat4 value) {
        int location = getUniformLocation(name);
        setUniform(location, value);
    }
}
