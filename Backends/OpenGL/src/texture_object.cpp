/**
 * @file texture_object.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the texture_object.hpp header.
 */

#include "SimpleGL_gl/texture_object.hpp"

#include "SimpleGL_gl/backend_gl.hpp"

namespace sgl {
    Texture* Texture::current = nullptr;

    Texture::Texture(void) {
        glGenTextures(1, &handle);
    }

    Texture::~Texture(void) {
        glDeleteTextures(1, &handle);

        /* Set current to nullptr if this was the bound texture object */
        if (Texture::current == this) {
            Texture::current = nullptr;
        }
    }

    GLuint Texture::getHandle() {
        return handle;
    }

    int Texture::getWidth() {
        return width;
    }

    int Texture::getHeight() {
        return height;
    }

    void Texture::bind() {
        /* If already bound return immediately */
        if (Texture::current == this) {
            return;
        }

        /* Bind buffer object */
        glBindTexture(GL_TEXTURE_2D, handle);
        Texture::current = this;
    }

    void Texture::generateMipMap() {
        glGenerateMipmap(GL_TEXTURE_2D);
    }

    void Texture::setMinFilter(TextureFilter filter) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, to_GLint(filter));
    }

    void Texture::setMagFilter(TextureFilter filter) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, to_GLint(filter));
    }

    void Texture::setWrapS(TextureWrap wrap) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, to_GLint(wrap));
    }

    void Texture::setWrapT(TextureWrap wrap) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, to_GLint(wrap));
    }

    void Texture::uploadImage(int width, int height, unsigned char* data) {
        this->width = width;
        this->height = height;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    }

    void Texture::uploadImage(TextureData image) {
        uploadImage(image.width, image.height, image.pixels);
    }
}
