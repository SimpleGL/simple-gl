/**
 * @file batching_gl.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the batching_gl.hpp header.
 */

#include "SimpleGL_gl/batching_gl.hpp"

#include <glm/glm.hpp>

#include <SimpleGL/logging.hpp>

using namespace glm;

namespace sgl {
    std::string BatchGL::vertexSource =
            "#version 150 core\n"

            "uniform mat4 model;"
            "uniform mat4 view;"
            "uniform mat4 projection;"

            "in vec3 position;"
            "in vec4 color;"
            "in vec2 texCoord;"

            "out vec4 vertexColor;"
            "out vec2 textureCoord;"

            "void main() {"
            "    vertexColor = color;"
            "    textureCoord = texCoord;"
            "    mat4 mvp = projection * view * model;"
            "    gl_Position = mvp * vec4(position, 1.0f);"
            "}";

    std::string BatchGL::fragmentSource =
            "#version 150 core\n"

            "uniform sampler2D texImage;"

            "in vec4 vertexColor;"
            "in vec2 textureCoord;"

            "out vec4 fragColor;"

            "void main() {"
            "    vec4 textureColor = texture(texImage, textureCoord);"
            "    fragColor = vertexColor * textureColor;"
            "}";

    Texture* BatchGL::defaultTexture;

    ShaderProgram* BatchGL::defaultShaderProgram;

    BatchGL::BatchGL(int capacity) : Batch() {
        /* Generate default texture, this texture contains 1 white pixel */
        /* This texture will work like some kind of a bitmask */
        BatchGL::defaultTexture = new Texture();
        unsigned char data[] = {0xFF, 0xFF, 0xFF, 0xFF};
        BatchGL::defaultTexture->bind();
        BatchGL::defaultTexture->uploadImage(1, 1, data);

        /* Bind vertex array object */
        vao = new VertexArray();
        vao->bind();

        /* Upload null data to vertex buffer object */
        vbo = new Buffer();
        vbo->bind(BufferTarget::ARRAY_BUFFER);
        vbo->uploadData(BufferTarget::ARRAY_BUFFER, capacity * sizeof (vec3), BufferUsage::DYNAMIC_DRAW);

        /* Upload null data to color buffer object */
        cbo = new Buffer();
        cbo->bind(BufferTarget::ARRAY_BUFFER);
        cbo->uploadData(BufferTarget::ARRAY_BUFFER, capacity * sizeof (vec4), BufferUsage::DYNAMIC_DRAW);

        /* Upload null data to texture coordinates buffer object */
        tbo = new Buffer();
        tbo->bind(BufferTarget::ARRAY_BUFFER);
        tbo->uploadData(BufferTarget::ARRAY_BUFFER, capacity * sizeof (vec2), BufferUsage::DYNAMIC_DRAW);

        /* Upload null data to normal buffer object */
        nbo = new Buffer();
        nbo->bind(BufferTarget::ARRAY_BUFFER);
        nbo->uploadData(BufferTarget::ARRAY_BUFFER, capacity * sizeof (vec3), BufferUsage::DYNAMIC_DRAW);

        /* Compile Vertex Shader */
        Shader vertexShader(ShaderType::VERTEX_SHADER);
        vertexShader.source(BatchGL::vertexSource);
        vertexShader.compile();

        /* Check if compiling was succesful */
        if (!vertexShader.getCompileStatus()) {
            Logger::logError(vertexShader.getInfoLog());
        }

        /* Compile Fragment Shader */
        Shader fragmentShader(ShaderType::FRAGMENT_SHADER);
        fragmentShader.source(BatchGL::fragmentSource);
        fragmentShader.compile();

        /* Check if compiling was succesful */
        if (!fragmentShader.getCompileStatus()) {
            Logger::logError(fragmentShader.getInfoLog());
        }

        /* Link shader program */
        shaderProgram = new ShaderProgram();
        shaderProgram->attach(&vertexShader);
        shaderProgram->attach(&fragmentShader);
        shaderProgram->link();

        /* Check if linking was successful */
        if (!shaderProgram->getLinkStatus()) {
            Logger::logError(shaderProgram->getInfoLog());
        }

        /* Store default shader program */
        BatchGL::defaultShaderProgram = shaderProgram;

        /* Use shader program */
        shaderProgram->use();

        /* Specify vertex attributes */
        specifyVertexAttributes();

        /* Bind fragment out color */
        shaderProgram->bindFragmentDataLocation(0, "fragColor");

        /* Set texture uniform */
        shaderProgram->setUniform("texImage", 0);

        /* Apply default MVP */
        applyTransform();
    }

    BatchGL::~BatchGL(void) {
        /* Delete VAO */
        delete vao;

        /* Delete VBOs */
        delete vbo;
        delete cbo;
        delete tbo;
        delete nbo;

        /* Delete program */
        delete shaderProgram;
    }

    void BatchGL::begin() {
        begin(Primitive::TRIANGLES);
    }

    void BatchGL::begin(Primitive mode) {
        /* Start drawing only if the batch isn't already drawing */
        if (!drawing) {
            this->mode = mode;
            drawing = true;
            numVertices = 0;
        }
    }

    void BatchGL::flush() {
        /* Only flush if there is at least one vertex */
        if (numVertices > 0) {
            /* Fill colors */
            if (colors.size() == 0) {
                /* No color: Fill with white */
                while (colors.size() < vertices.size()) {
                    color(vec4(1.0f));
                }
            } else if (colors.size() < vertices.size()) {
                /* Less colors than vertices: Fill with last color */
                vec4 lastColor = colors.back();
                while (colors.size() < vertices.size()) {
                    color(lastColor);
                }
            }

            /* Fill texCoords */
            if (texCoords.size() == 0) {
                /* No texture coordinates, bind mask texture */
                BatchGL::defaultTexture->bind();
            }
            while (texCoords.size() < vertices.size()) {
                texCoord(vec2(1.0f));
            }

            /* Fill normals */
            while (normals.size() < vertices.size()) {
                normal(vec3(0.0f));
            }

            /* Use shader program */
            shaderProgram->use();

            /* Apply transformation */
            applyTransform();

            /* Upload vertex data */
            vbo->bind(BufferTarget::ARRAY_BUFFER);
            vbo->uploadSubData(BufferTarget::ARRAY_BUFFER, 0, numVertices * sizeof (vec3), vertices);

            /* Upload color data */
            cbo->bind(BufferTarget::ARRAY_BUFFER);
            cbo->uploadSubData(BufferTarget::ARRAY_BUFFER, 0, numVertices * sizeof (vec4), colors);

            /* Upload texCoord data */
            tbo->bind(BufferTarget::ARRAY_BUFFER);
            tbo->uploadSubData(BufferTarget::ARRAY_BUFFER, 0, numVertices * sizeof (vec2), texCoords);

            /* Upload normal data */
            nbo->bind(BufferTarget::ARRAY_BUFFER);
            nbo->uploadSubData(BufferTarget::ARRAY_BUFFER, 0, numVertices * sizeof (vec3), normals);

            /* Draw batch */
            GL::drawArrays(mode, 0, numVertices);

            /* Clear lists */
            vertices.clear();
            colors.clear();
            texCoords.clear();
            normals.clear();
            numVertices = 0;
        }
    }

    void BatchGL::specifyVertexAttributes() {
        /* Specify vertex pointer, vertices are vec3 */
        vbo->bind(BufferTarget::ARRAY_BUFFER);
        int positionAttribute = shaderProgram->getAttributeLocation("position");
        vao->enableAttribute(positionAttribute);
        vao->pointAttribute(positionAttribute, 3, ValueType::FLOAT, false, sizeof (vec3), 0);

        /* Specify color pointer, colors are vec4 */
        cbo->bind(BufferTarget::ARRAY_BUFFER);
        int colorAttribute = shaderProgram->getAttributeLocation("color");
        vao->enableAttribute(colorAttribute);
        vao->pointAttribute(colorAttribute, 4, ValueType::FLOAT, false, sizeof (vec4), 0);

        /* Specify texCoord pointer, texCoords are vec2 */
        tbo->bind(BufferTarget::ARRAY_BUFFER);
        int texCoordAttribute = shaderProgram->getAttributeLocation("texCoord");
        vao->enableAttribute(texCoordAttribute);
        vao->pointAttribute(texCoordAttribute, 2, ValueType::FLOAT, false, sizeof (vec2), 0);

        /* Specify normal pointer, normals are vec3 */
        nbo->bind(BufferTarget::ARRAY_BUFFER);
        int normalAttribute = shaderProgram->getAttributeLocation("normal");
        vao->enableAttribute(normalAttribute);
        vao->pointAttribute(normalAttribute, 3, ValueType::FLOAT, false, sizeof (vec3), 0);
    }

    void BatchGL::applyTransform() {
        /* Only change if transformation was changed */
        if (transformation.changed) {
            /* Set model matrix */
            shaderProgram->setUniform("model", transformation.model);

            /* Set view matrix */
            shaderProgram->setUniform("view", transformation.view);

            /* Set projection matrix */
            shaderProgram->setUniform("projection", transformation.projection);

            /* Transformation applied */
            transformation.changed = false;
        }
    }

    void BatchGL::resetShaderProgram() {
        setShaderProgram(BatchGL::defaultShaderProgram);
    }

    ShaderProgram* BatchGL::getShaderProgram() {
        return shaderProgram;
    }

    void BatchGL::setShaderProgram(ShaderProgram* program) {
        shaderProgram = program;

        /* Specify vertex attributes */
        shaderProgram->use();
        specifyVertexAttributes();

        /* Force applyTransform */
        transformation.changed = true;
        applyTransform();
    }
}
