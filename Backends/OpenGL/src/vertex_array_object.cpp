/**
 * @file vertex_array_object.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the vertex_array_object.hpp header.
 */

#include "SimpleGL_gl/vertex_array_object.hpp"

#include "SimpleGL_gl/backend_gl.hpp"

namespace sgl {
    VertexArray* VertexArray::current = nullptr;

    VertexArray::VertexArray(void) {
        glGenVertexArrays(1, &handle);
    }

    VertexArray::~VertexArray(void) {
        glDeleteVertexArrays(1, &handle);

        /* Set current to nullptr if this was the bound vertex array object */
        if (VertexArray::current == this) {
            VertexArray::current = nullptr;
        }
    }

    GLuint VertexArray::getHandle() {
        return handle;
    }

    void VertexArray::bind() {
        /* If already bound return immediately */
        if (VertexArray::current == this) {
            return;
        }

        /* Bind vertex array object */
        glBindVertexArray(handle);
        VertexArray::current = this;
    }

    void VertexArray::enableAttribute(unsigned int index) {
        glEnableVertexAttribArray(index);
    }

    void VertexArray::disableAttribute(unsigned int index) {
        glDisableVertexAttribArray(index);
    }

    void VertexArray::pointAttribute(unsigned int index, int size, ValueType type, bool normalized, int stride, long offset) {
        void* pointer = (void*) (long(NULL) + offset);
        glVertexAttribPointer(index, size, to_GLenum(type), normalized, stride, pointer);
    }
}
