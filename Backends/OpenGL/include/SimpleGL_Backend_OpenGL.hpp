/**
 * @file SimpleGL_Backend_OpenGL.hpp
 * @author Heiko Brumme
 * 
 * This file is meant to get included by projects using the OpenGL backend.
 *
 * In this file all the headers of the OpenGL backend should get included, so
 * that the user just have to include this header.
 */

#ifndef SIMPLEGL_BACKEND_OPENGL_HPP
#define SIMPLEGL_BACKEND_OPENGL_HPP

/* SimpleGL core */
#include <SimpleGL_Core.hpp>

/* SimpleGL OpenGL backend */
#include "SimpleGL_gl/backend_gl.hpp"
#include "SimpleGL_gl/application_gl.hpp"
#include "SimpleGL_gl/glfw_window_gl.hpp"
#include "SimpleGL_gl/batching_gl.hpp"
#include "SimpleGL_gl/manager_gl.hpp"

/* OpenGL wrapper */
#include "SimpleGL_gl/gl_general.hpp"
#include "SimpleGL_gl/vertex_array_object.hpp"
#include "SimpleGL_gl/vertex_buffer_object.hpp"
#include "SimpleGL_gl/shader_objects.hpp"
#include "SimpleGL_gl/texture_object.hpp"
#include "SimpleGL_gl/framebuffer_object.hpp"

#endif /* SIMPLEGL_BACKEND_OPENGL_HPP */
