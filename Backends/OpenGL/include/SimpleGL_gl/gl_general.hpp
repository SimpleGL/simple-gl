/**
 * @file gl_general.hpp
 * @author Heiko Brumme
 *
 * This file contains general methods for OpenGL.
 */

#ifndef GL_GENERAL_HPP
#define GL_GENERAL_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <GL/glew.h>

#include <glm/glm.hpp>

#include "backend_gl.hpp"

namespace sgl {

    /**
     * @enum Capability
     *
     * This enum wraps OpenGL capabilities.
     */
    enum class Capability : GLenum {

        /** Equivalent to GL_BLEND. */
        BLEND = GL_BLEND,

        /** Equivalent to GL_BLEND. */
        CULL_FACE = GL_CULL_FACE,

        /** Equivalent to GL_DEPTH_TEST. */
        DEPTH_TEST = GL_DEPTH_TEST
    };

    /**
     * @enum ClearBit
     *
     * This enum wraps the clear buffer bits.
     */
    enum class ClearBit : GLbitfield {

        /** Equivalent to GL_COLOR_BUFFER_BIT. */
        COLOR_BUFFER_BIT = GL_COLOR_BUFFER_BIT,

        /** Equivalent to GL_DEPTH_BUFFER_BIT. */
        DEPTH_BUFFER_BIT = GL_DEPTH_BUFFER_BIT,

        /** Equivalent to GL_STENCIL_BUFFER_BIT. */
        STENCIL_BUFFER_BIT = GL_STENCIL_BUFFER_BIT
    };

    /**
     * @enum Primitive
     *
     * This enum wraps Primitive types of OpenGL.
     */
    enum class Primitive : GLenum {

        /** Equivalent to GL_POINTS. */
        POINTS = GL_POINTS,

        /** Equivalent to GL_LINES. */
        LINES = GL_LINES,

        /** Equivalent to GL_LINE_STRIP. */
        LINE_STRIP = GL_LINE_STRIP,

        /** Equivalent to GL_LINE_LOOP. */
        LINE_LOOP = GL_LINE_LOOP,

        /** Equivalent to GL_TRIANGLES. */
        TRIANGLES = GL_TRIANGLES,

        /** Equivalent to GL_TRIANGLE_STRIP. */
        TRIANGLE_STRIP = GL_TRIANGLE_STRIP,

        /** Equivalent to GL_TRIANGLE_FAN. */
        TRIANGLE_FAN = GL_TRIANGLE_FAN
    };

    /**
     * @enum BlendFactor
     *
     * This enum wraps the blend factos.
     */
    enum class BlendFactor : GLenum {

        /** Equivalent to GL_ZERO. */
        ZERO = GL_ZERO,

        /** Equivalent to GL_ONE. */
        ONE = GL_ONE,

        /** Equivalent to GL_SRC_COLOR. */
        SRC_COLOR = GL_SRC_COLOR,

        /** Equivalent to GL_ONE_MINUS_SRC_COLOR. */
        ONE_MINUS_SRC_COLOR = GL_ONE_MINUS_SRC_COLOR,

        /** Equivalent to GL_DST_COLOR. */
        DST_COLOR = GL_DST_COLOR,

        /** Equivalent to GL_ONE_MINUS_DST_COLOR. */
        ONE_MINUS_DST_COLOR = GL_ONE_MINUS_DST_COLOR,

        /** Equivalent to GL_SRC_ALPHA. */
        SRC_ALPHA = GL_SRC_ALPHA,

        /** Equivalent to GL_ONE_MINUS_SRC_ALPHA. */
        ONE_MINUS_SRC_ALPHA = GL_ONE_MINUS_SRC_ALPHA,

        /** Equivalent to GL_DST_ALPHA. */
        DST_ALPHA = GL_DST_ALPHA,

        /** Equivalent to GL_ONE_MINUS_DST_ALPHA. */
        ONE_MINUS_DST_ALPHA = GL_ONE_MINUS_DST_ALPHA,

        /** Equivalent to GL_CONSTANT_COLOR. */
        CONSTANT_COLOR = GL_CONSTANT_COLOR,

        /** Equivalent to GL_ONE_MINUS_CONSTANT_COLOR. */
        ONE_MINUS_CONSTANT_COLOR = GL_ONE_MINUS_CONSTANT_COLOR,

        /** Equivalent to GL_CONSTANT_ALPHA. */
        CONSTANT_ALPHA = GL_CONSTANT_ALPHA,

        /** Equivalent to GL_ONE_MINUS_CONSTANT_ALPHA. */
        ONE_MINUS_CONSTANT_ALPHA = GL_ONE_MINUS_CONSTANT_ALPHA,

        /** Equivalent to GL_SRC_ALPHA_SATURATE. */
        SRC_ALPHA_SATURATE = GL_SRC_ALPHA_SATURATE,

        /** Equivalent to GL_SRC1_COLOR. */
        SRC1_COLOR = GL_SRC1_COLOR,

        /** Equivalent to GL_ONE_MINUS_SRC1_COLOR. */
        ONE_MINUS_SRC1_COLOR = GL_ONE_MINUS_SRC1_COLOR,

        /** Equivalent to GL_SRC1_ALPHA. */
        SRC1_ALPHA = GL_SRC1_ALPHA,

        /** Equivalent to GL_ONE_MINUS_SRC1_ALPHA. */
        ONE_MINUS_SRC1_ALPHA = GL_ONE_MINUS_SRC1_ALPHA
    };

    /**
     * @enum CullFaceMode
     *
     * This enum wraps the face culling modes.
     */
    enum class CullFaceMode : GLenum {

        /** Equivalent to GL_FRONT. */
        FRONT = GL_FRONT,

        /** Equivalent to GL_BACK. */
        BACK = GL_BACK,

        /** Equivalent to GL_FRONT_AND_BACK. */
        FRONT_AND_BACK = GL_FRONT_AND_BACK
    };

    /**
     * @enum DepthFunction
     *
     * This enum wraps the depth functions.
     */
    enum class DepthFunction : GLenum {

        /** Equivalent to GL_NEVER. */
        NEVER = GL_NEVER,

        /** Equivalent to GL_LESS. */
        LESS = GL_LESS,

        /** Equivalent to GL_EQUAL. */
        EQUAL = GL_EQUAL,

        /** Equivalent to GL_LEQUAL. */
        LEQUAL = GL_LEQUAL,

        /** Equivalent to GL_GREATER. */
        GREATER = GL_GREATER,

        /** Equivalent to GL_NOTEQUAL. */
        NOTEQUAL = GL_NOTEQUAL,

        /** Equivalent to GL_GEQUAL. */
        GEQUAL = GL_GEQUAL,

        /** Equivalent to GL_ALWAYS. */
        ALWAYS = GL_ALWAYS
    };

    /**
     * Overloaded OR operator for the clear buffer bit.
     *
     * @param left The left clear buffer bit.
     * @param right The right clear buffer bit.
     */
    inline ClearBit operator|(ClearBit left, ClearBit right) {
        GLbitfield bitmask = to_GLbitfield(left) | to_GLbitfield(right);
        return static_cast<ClearBit> (bitmask);
    };

    /**
     * @class GL
     *
     * This static class wraps general OpenGL functions.
     */
    class SIMPLEGL_GL_EXPORT GL {

    private:
        /** Disable instantiation. */
        GL(void);

    public:
        /** Checks if an OpenGL error occured. */
        static void checkError();

        /**
         * Enable server-side GL capabilities.
         *
         * @param cap The capability to enable.
         */
        static void enable(Capability cap);

        /**
         * Disable server-side GL capabilities.
         *
         * @param cap The capability to disable.
         */
        static void disable(Capability cap);

        /**
         * Sets the clear color.
         *
         * @param r The red component.
         * @param g The green component.
         * @param b The blue component.
         * @param a The alpha component.
         */
        static void clearColor(float r, float g, float b, float a);

        /**
         * Sets the clear color.
         *
         * @param color The RGBA color.
         */
        static void clearColor(glm::vec4 color);

        /**
         * Clears the specified buffers.
         *
         * @param mask The bitfield containing the clear buffer bits.
         */
        static void clear(ClearBit mask);

        /**
         * Renders vertex data with the current bound VBO.
         *
         * @param mode The Primitive mode to draw.
         * @param first The index of the array.
         * @param count The number of vertices to draw.
         */
        static void drawArrays(Primitive mode, int first, int count);

        /**
         * Specify pixel arithmetic.
         *
         * @param source Specifies how the red, green, blue, and alpha source blending factors are computed.
         * @param destination Specifies how the red, green, blue, and alpha destination blending factors are computed.
         */
        static void blendFunc(BlendFactor source, BlendFactor destination);

        /**
         * Specify whether front- or back-facing facets can be culled.
         *
         * @param mode Specifies whether front- or back-facing facets are candidates for culling.
         */
        static void cullFace(CullFaceMode mode);

        /**
         * Specify the value used for depth buffer comparisons.
         *
         * @param func Specifies the depth comparison function.
         */
        static void depthFunc(DepthFunction func);
    };
}

#endif /* GL_GENERAL_HPP */
