/**
 * @file glfw_window_gl.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to generate a GLFW window with OpenGL context.
 */

#ifndef GLFW_WINDOW_GL_HPP
#define GLFW_WINDOW_GL_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <string>

#include <SimpleGL/glfw_window.hpp>

namespace sgl {

    /**
     * @class WindowGL
     * 
     * This class provides a GLFW window with OpenGL context.
     */
    class SIMPLEGL_GL_EXPORT WindowGL : public Window {

    public:
        /**
         * Creates a new window with the specified parameters. By default vertical synchronization is disabled.
         *
         * @param width The width of the window.
         * @param height The height of the window.
         * @param title The title of the window.
         * @param vsync Set this to true, if you want to have vertical synchronization enabled.
         */
        WindowGL(int width, int height, std::string title, bool vsync = false);

        /**
         * Enables or disables vertical synchronization.
         *
         * @param vsync Set this to true, if you want to have vertical synchronization enabled.
         */
        void setVSync(bool vsync);

        /** Swaps the framebuffers of this window. */
        void swapBuffers() override;
    };
}

#endif /* GLFW_WINDOW_GL_HPP */
