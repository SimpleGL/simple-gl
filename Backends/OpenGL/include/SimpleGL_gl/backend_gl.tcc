/**
 * @file backend_gl.tcc
 * @author Heiko Brumme
 * 
 * This files contains the template implementations of the backend_gl.hpp header.
 */

#include "SimpleGL_gl/backend_gl.hpp"

#include <type_traits>

using namespace std;

namespace sgl {

    template<typename E>
    GLint to_GLint(E e) {
        return static_cast<typename underlying_type<E>::type> (e);
    }

    template<typename E>
    GLenum to_GLenum(E e) {
        return static_cast<typename underlying_type<E>::type> (e);
    }

    template<typename E>
    GLbitfield to_GLbitfield(E e) {
        return static_cast<typename underlying_type<E>::type> (e);
    }
}
