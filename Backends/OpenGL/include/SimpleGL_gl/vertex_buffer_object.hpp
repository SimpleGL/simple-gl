/**
 * @file vertex_buffer_object.hpp
 * @author Heiko Brumme
 *
 * This file contains classes to create buffer objects.
 */

#ifndef VERTEX_BUFFER_OBJECT_HPP
#define VERTEX_BUFFER_OBJECT_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <vector>

#include <GL/glew.h>

#include <glm/glm.hpp>

namespace sgl {

    /**
     * @enum BufferTarget
     *
     * This enum wraps the buffer object's targets.
     */
    enum class BufferTarget : GLenum {

        /** Equivalent to GL_ARRAY_BUFFER. */
        ARRAY_BUFFER = GL_ARRAY_BUFFER,

        /** Equivalent to GL_ELEMENT_ARRAY_BUFFER. */
        ELEMENT_ARRAY_BUFFER = GL_ELEMENT_ARRAY_BUFFER
    };

    /**
     * @enum BufferUsage
     *
     * This enum wraps the buffer object's usages.
     */
    enum class BufferUsage : GLenum {

        /** Equivalent to GL_STATIC_DRAW. */
        STATIC_DRAW = GL_STATIC_DRAW,

        /** Equivalent to GL_STATIC_READ. */
        STATIC_READ = GL_STATIC_READ,

        /** Equivalent to GL_STATIC_COPY. */
        STATIC_COPY = GL_STATIC_COPY,

        /** Equivalent to GL_DYNAMIC_DRAW. */
        DYNAMIC_DRAW = GL_DYNAMIC_DRAW,

        /** Equivalent to GL_DYNAMIC_READ. */
        DYNAMIC_READ = GL_DYNAMIC_READ,

        /** Equivalent to GL_DYNAMIC_COPY. */
        DYNAMIC_COPY = GL_DYNAMIC_COPY,

        /** Equivalent to GL_STREAM_DRAW. */
        STREAM_DRAW = GL_STREAM_DRAW,

        /** Equivalent to GL_STREAM_READ. */
        STREAM_READ = GL_STREAM_READ,

        /** Equivalent to GL_STREAM_COPY. */
        STREAM_COPY = GL_STREAM_COPY
    };

    /**
     * @enum MapAccess
     *
     * This enum wraps the buffer object's map accesses.
     */
    enum class MapAccess : GLenum {

        /** Equivalent to GL_READ_ONLY. */
        READ_ONLY = GL_READ_ONLY,

        /** Equivalent to GL_WRITE_ONLY. */
        WRITE_ONLY = GL_WRITE_ONLY,

        /** Equivalent to GL_READ_WRITE. */
        READ_WRITE = GL_READ_WRITE
    };

    /**
     * @enum MapRangeAccess
     *
     * This enum wraps the buffer object's map range access bits.
     */
    enum class MapRangeAccess : GLbitfield {

        /** Equivalent to GL_MAP_READ_BIT. */
        MAP_READ_BIT = GL_MAP_READ_BIT,

        /** Equivalent to GL_MAP_WRITE_BIT. */
        MAP_WRITE_BIT = GL_MAP_WRITE_BIT
    };

    /**
     * @class Buffer
     *
     * This class wraps an OpenGL buffer object.
     */
    class SIMPLEGL_GL_EXPORT Buffer {

    private:
        /** Points at the currently bound buffer object. */
        static Buffer* current;

        /** The handle for the buffer object. */
        GLuint handle;

    public:
        /** Generates a new buffer object. */
        Buffer(void);

        /** Deletes the buffer object. */
        ~Buffer(void);

        /**
         * Returns the handle of the buffer object.
         *
         * @return The handle of the buffer object.
         */
        GLuint getHandle();

        /**
         * Binds the buffer object with the specified target.
         *
         * @param target Target to bind.
         */
        void bind(BufferTarget target);

        /**
         * Uploads the specified data to the buffer object.
         *
         * @param target Target to upload.
         * @param size Size in bytes of the data to upload.
         * @param data The data to upload as a float array.
         * @param usage Usage of the data.
         */
        void uploadData(BufferTarget target, long size, float* data, BufferUsage usage);

        /**
         * Uploads the specified data to the buffer object.
         *
         * @param target Target to upload.
         * @param size Size in bytes of the data to upload.
         * @param data The data to upload as a float vector.
         * @param usage Usage of the data.
         */
        void uploadData(BufferTarget target, long size, std::vector<float> data, BufferUsage usage);

        /**
         * Uploads the specified data to the buffer object.
         *
         * @param target Target to upload.
         * @param size Size in bytes of the data to upload.
         * @param data The data to upload as a float vector.
         * @param usage Usage of the data.
         */
        void uploadData(BufferTarget target, long size, std::vector<glm::vec2> data, BufferUsage usage);

        /**
         * Uploads the specified data to the buffer object.
         *
         * @param target Target to upload.
         * @param size Size in bytes of the data to upload.
         * @param data The data to upload as a float vector.
         * @param usage Usage of the data.
         */
        void uploadData(BufferTarget target, long size, std::vector<glm::vec3> data, BufferUsage usage);

        /**
         * Uploads the specified data to the buffer object.
         *
         * @param target Target to upload.
         * @param size Size in bytes of the data to upload.
         * @param data The data to upload as a float vector.
         * @param usage Usage of the data.
         */
        void uploadData(BufferTarget target, long size, std::vector<glm::vec4> data, BufferUsage usage);

        /**
         * Uploads NULL data to the buffer object to allocate memory.
         *
         * @param target Target to upload.
         * @param size Size in bytes of the data store.
         * @param usage Usage of the data.
         */
        void uploadData(BufferTarget target, long size, BufferUsage usage);

        /**
         * Uploads the specified data to the buffer object starting at the offset.
         *
         * @param target Target to upload.
         * @param offset Offset where the data should get uploaded in bytes.
         * @param size Size in bytes of the data to upload.
         * @param data The data to upload as a float array.
         */
        void uploadSubData(BufferTarget target, long offset, long size, float* data);

        /**
         * Uploads the specified data to the buffer object starting at the offset.
         *
         * @param target Target to upload.
         * @param offset Offset where the data should get uploaded in bytes.
         * @param size Size in bytes of the data to upload.
         * @param data The data to upload as a float array.
         */
        void uploadSubData(BufferTarget target, long offset, long size, std::vector<float> data);

        /**
         * Uploads the specified data to the buffer object starting at the offset.
         *
         * @param target Target to upload.
         * @param offset Offset where the data should get uploaded in bytes.
         * @param size Size in bytes of the data to upload.
         * @param data The data to upload as a float array.
         */
        void uploadSubData(BufferTarget target, long offset, long size, std::vector<glm::vec2> data);

        /**
         * Uploads the specified data to the buffer object starting at the offset.
         *
         * @param target Target to upload.
         * @param offset Offset where the data should get uploaded in bytes.
         * @param size Size in bytes of the data to upload.
         * @param data The data to upload as a float array.
         */
        void uploadSubData(BufferTarget target, long offset, long size, std::vector<glm::vec3> data);

        /**
         * Uploads the specified data to the buffer object starting at the offset.
         *
         * @param target Target to upload.
         * @param offset Offset where the data should get uploaded in bytes.
         * @param size Size in bytes of the data to upload.
         * @param data The data to upload as a float array.
         */
        void uploadSubData(BufferTarget target, long offset, long size, std::vector<glm::vec4> data);

        /**
         * Maps the buffer object.
         *
         * @param target Target to map.
         * @param access Access policy of the mapping.
         *
         * @return A pointer to the data storage.
         */
        void* map(BufferTarget target, MapAccess access);

        /**
         * Maps the buffer object starting at the offset.
         *
         * @param target Target to map.
         * @param offset Offset where the mapping should start.
         * @param length Length of the mapping in bytes.
         * @param access Access policy of the mapping.
         *
         * @return A pointer to the data storage.
         */
        void* mapRange(BufferTarget target, long offset, long length, MapRangeAccess access);

        /**
         * Unmaps the buffer object.
         *
         * @param target Target to unmap.
         */
        void unmap(BufferTarget target);
    };
}

#endif /* VERTEX_BUFFER_OBJECT_HPP */
