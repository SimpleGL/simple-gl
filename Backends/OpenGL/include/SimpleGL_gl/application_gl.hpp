/**
 * @file application_gl.hpp
 * @author Heiko Brumme
 * 
 * This file contains the abstract OpenGL Application class.
 */

#ifndef APPLICATION_GL_HPP
#define APPLICATION_GL_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <string>

#include <SimpleGL/application.hpp>

#include "batching_gl.hpp"
#include "manager_gl.hpp"

namespace sgl {

    /**
     * @class ApplicationGL
     *
     * This class should get extended by the user when using the OpenGL backend.
     */
    class SIMPLEGL_GL_EXPORT ApplicationGL : public Application {

    protected:
        /** The batch of this application. */
        BatchGL* batch;

        /** The texture manager of this application. */
        TextureManager* textureManager;

        /** The shader manager of this application. */
        ShaderManager* shaderManager;

    public:
        /**
         * Creates a new application.
         *
         * @param width The width of the window, default is 640 pixel.
         * @param height The height of the window, default is 480 pixel.
         * @param title The title of the window, default is "SimpleGL Application".
         */
        ApplicationGL(int width = 640, int height = 480, std::string title = "SimpleGL Application (OpenGL backend)");

        /** Destroys the application. */
        ~ApplicationGL(void);

        /** This method clears the color and depth buffers. */
        void clear() override;

        /** Disposes the application. */
        void dispose() override;
    };
}

#endif /* APPLICATION_GL_HPP */
