/**
 * @file manager_gl.hpp
 * @author Heiko Brumme
 * 
 * This file contains classes for managing OpenGL objects.
 */

#ifndef MANAGER_GL_HPP
#define MANAGER_GL_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <string>

#include <SimpleGL/manager.hpp>

#include "texture_object.hpp"
#include "shader_objects.hpp"

namespace sgl {

    /**
     * @class TextureManager
     *
     * This class provides an class for managing textures.
     */
    class SIMPLEGL_GL_EXPORT TextureManager : public Manager<Texture> {

    public:
        /**
         * Removes and disposes a texture from the storage.
         *
         * @param name The name of the texture.
         */
        void remove(std::string name) override;

        /** Removes all textures and disposes them. */
        void clear() override;

        /**
         * Loads a texture from file and stores it.
         *
         * @param path File path to the image.
         * @param name The name of the texture.
         *
         * @return The loaded texture.
         */
        Texture* load(std::string path, std::string name);
    };

    /**
     * @class ShaderManager
     *
     * This class provides an class for managing shaders.
     */
    class SIMPLEGL_GL_EXPORT ShaderManager : public Manager<Shader> {

    public:
        /**
         * Removes and disposes a shader from the storage.
         *
         * @param name The name of the shader.
         */
        void remove(std::string name) override;

        /** Removes all shaders and disposes them. */
        void clear() override;

        /**
         * Loads a shader from file and stores it.
         *
         * @param path File path to the shader source.
         * @param name The name of the shader.
         * @param type The type of the shader, either VERTEX_SHADER or FRAGMENT_SHADER.
         *
         * @return The loaded shader.
         */
        Shader* loadShader(std::string path, std::string name, ShaderType type);

        /**
         * Creates a shader program from a vertex shader file and a fragment shader file.
         *
         * @param vertexPath File path to the vertex shader source.
         * @param fragmentPath File path to the fragment shader source.
         * @param vertexName The name for the vertex shader.
         * @param fragmentName The name for the fragment shader.
         *
         * @return A shader program with the specified shader sources.
         */
        ShaderProgram* loadShaderProgram(std::string vertexPath, std::string fragmentPath, std::string vertexName, std::string fragmentName);

        /**
         * Creates a shader program from two stored shaders.
         *
         * @param vertexName The name of the stored vertex shader.
         * @param fragmentName The name of the stored fragment shader.
         *
         * @return A shader program with the specified shaders.
         */
        ShaderProgram* createShaderProgram(std::string vertexName, std::string fragmentName);
    };
}

#endif /* MANAGER_GL_HPP */
