/**
 * @file framebuffer_object.hpp
 * @author Heiko Brumme
 *
 * This file stores all classes to create a framebuffer object.
 */

#ifndef FRAMEBUFFER_OBJECT_HPP
#define FRAMEBUFFER_OBJECT_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <GL/glew.h>

#include "texture_object.hpp"

namespace sgl {

    /**
     * @class Renderbuffer
     * 
     * This class wraps an OpenGL renderbuffer object.
     */
    class SIMPLEGL_GL_EXPORT Renderbuffer {

    private:
        /** Points at the currently bound renderbuffer object. */
        static Renderbuffer* current;

        /** The handle for the renderbuffer object. */
        GLuint handle;

    public:
        /** Generates a new renderbuffer object. */
        Renderbuffer(void);

        /** Deletes the renderbuffer object. */
        ~Renderbuffer(void);

        /**
         * Returns the handle of the renderbuffer object.
         *
         * @return The handle of the renderbuffer object.
         */
        GLuint getHandle();

        /** Binds the renderbuffer object. */
        void bind();

        /**
         * Allocates data storage, format and dimensions of the renderbuffer object.
         *
         * @param width The width of the renderbuffer object in pixel.
         * @param height The height of the renderbuffer object in pixel.
         */
        void storage(int width, int height);
    };

    /**
     * @class FramebufferGL
     * 
     * This class wraps an OpenGL framebuffer object.
     */
    class SIMPLEGL_GL_EXPORT FramebufferGL {

    private:
        /** Points at the currently bound framebuffer object. */
        static FramebufferGL* current;

        /** The handle for the framebuffer object. */
        GLuint handle;

    public:
        /** Generates a new framebuffer object. */
        FramebufferGL(void);

        /** Deletes the framebuffer object. */
        ~FramebufferGL(void);

        /**
         * Returns the handle of the framebuffer object.
         *
         * @return The handle of the framebuffer object.
         */
        GLuint getHandle();

        /** Binds the framebuffer object. */
        void bind();

        /**
         * Sets a texture object as a logical buffer to the framebuffer object.
         *
         * @param texture The texture to set.
         */
        void setTexture(Texture* texture);

        /**
         * Sets a renderbuffer object as a logical buffer to the framebuffer object.
         *
         * @param buffer The renderbuffer object to set.
         */
        void setRenderbuffer(Renderbuffer* buffer);
    };
}

#endif /* FRAMEBUFFER_OBJECT_HPP */
