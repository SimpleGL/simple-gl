/**
 * @file batching_gl.hpp
 * @author Heiko Brumme
 * 
 * This file contains the class for batch rendering with OpenGL.
 */

#ifndef BATCHING_GL_HPP
#define BATCHING_GL_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <string>

#include <SimpleGL/batching.hpp>

#include "gl_general.hpp"
#include "vertex_array_object.hpp"
#include "vertex_buffer_object.hpp"
#include "shader_objects.hpp"
#include "texture_object.hpp"

namespace sgl {

    /**
     * @class BatchGL
     *
     * This class is used for batch rendering with OpenGL.
     */
    class SIMPLEGL_GL_EXPORT BatchGL : public Batch {

    private:
        /** Stores the default Vertex Shader Source. */
        static std::string vertexSource;

        /** Stores the default Fragment Shader Source. */
        static std::string fragmentSource;

        /** Stores the default texture object. */
        static Texture* defaultTexture;

        /** Stores the default shader program. */
        static ShaderProgram* defaultShaderProgram;

        /** The vertex array object of this batch. */
        VertexArray* vao;

        /** The vertex buffer object to store vertices. */
        Buffer* vbo;

        /** The vertex buffer object to store colors. */
        Buffer* cbo;

        /** The vertex buffer object to store texture coordinates. */
        Buffer* tbo;

        /** The vertex buffer object to store normals. */
        Buffer* nbo;

        /** The Shader Program used for this batch. */
        ShaderProgram* shaderProgram;

        /** The mode which is used for the current drawing. */
        Primitive mode;

    public:
        /**
         * Creates a new batch with specified capacity.
         * If no capacity is specified it will use a default capacity of 1048576 vertices.
         *
         * @param capacity The capacity of this batch.
         */
        BatchGL(int capacity = 1024 * 1024);

        /** Destroys a batch. */
        ~BatchGL(void);

        /** Starts drawing with TRIANGLES mode. */
        void begin() override;

        /**
         * Starts drawing with specified mode.
         *
         * @param mode The mode to use for drawing.
         */
        void begin(Primitive mode);

        /** Flushes the batch, could be used if the batch would overflow. */
        void flush() override;

        /** Specifies the vertex attributes. */
        void specifyVertexAttributes();

        /** Applies the MVP transformation if necessary. */
        void applyTransform();

        /** Resets to the default shader program. */
        void resetShaderProgram();

        /**
         * Gets the current shader program of the batch.
         *
         * @return The current shader program.
         */
        ShaderProgram* getShaderProgram();

        /**
         * Sets the current shader program of the batch.
         *
         * @param program The new shader program.
         */
        void setShaderProgram(ShaderProgram* program);
    };
}

#endif /* BATCHING_GL_HPP */
