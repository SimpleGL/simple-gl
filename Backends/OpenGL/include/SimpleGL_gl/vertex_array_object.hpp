/**
 * @file vertex_array_object.hpp
 * @author Heiko Brumme
 *
 * This file contains classes to create vertex array objects.
 */

#ifndef VERTEX_ARRAY_OBJECT_HPP
#define VERTEX_ARRAY_OBJECT_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <GL/glew.h>

namespace sgl {

    /**
     * @enum ValueType
     *
     * This enum wraps the vertex array object's value types.
     */
    enum class ValueType : GLenum {

        /** Equivalent to GL_BYTE. */
        BYTE = GL_BYTE,

        /** Equivalent to GL_UNSIGNED_BYTE. */
        UNSIGNED_BYTE = GL_UNSIGNED_BYTE,

        /** Equivalent to GL_SHORT. */
        SHORT = GL_SHORT,

        /** Equivalent to GL_UNSIGNED_SHORT. */
        UNSIGNED_SHORT = GL_UNSIGNED_SHORT,

        /** Equivalent to GL_INT. */
        INT = GL_INT,

        /** Equivalent to GL_UNSIGNED_INT. */
        UNSIGNED_INT = GL_UNSIGNED_INT,

        /** Equivalent to GL_FLOAT. */
        FLOAT = GL_FLOAT,

        /** Equivalent to GL_DOUBLE. */
        DOUBLE = GL_DOUBLE
    };

    /**
     * @class VertexArray
     *
     * This class wraps an OpenGL vertex array object.
     */
    class SIMPLEGL_GL_EXPORT VertexArray {

    private:
        /** Points at the currently bound vertex array object. */
        static VertexArray* current;

        /** The handle for the vertex array object. */
        GLuint handle;

    public:
        /** Generates a new vertex array object. */
        VertexArray(void);

        /** Deletes the vertex array object. */
        ~VertexArray(void);

        /**
         * Returns the handle of the vertex array object.
         *
         * @return The handle of the vertex array object.
         */
        GLuint getHandle();

        /** Binds the vertex array object. */
        void bind();

        /**
         * Enables a vertex attribute.
         *
         * @param index Location value of the attribute.
         */
        void enableAttribute(unsigned int index);

        /**
         * Disables a vertex attribute.
         *
         * @param index Location value of the attribute.
         */
        void disableAttribute(unsigned int index);

        /**
         * Sets the vertex attribute pointer.
         *
         * @param index Location value of the attribute.
         * @param size Number of values per vertex.
         * @param type Type of the value.
         * @param normalized Specifies if fixed-point data values should be normalized.
         * @param stride Offset between consecutive generic vertex attributes in bytes.
         * @param offset Offset of the first component of the first generic vertex attribute in bytes.
         */
        void pointAttribute(unsigned int index, int size, ValueType type, bool normalized, int stride, long offset);
    };
}

#endif /* VERTEX_ARRAY_OBJECT_HPP */
