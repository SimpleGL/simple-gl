/**
 * @file backend_gl.hpp
 * @author Heiko Brumme
 * 
 * This file contains some general functions for the OpenGL Backend.
 */

#ifndef BACKEND_GL_HPP
#define BACKEND_GL_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <GL/glew.h>

namespace sgl {

    /**
     * @struct GLVersion
     *
     * Structure for saving the OpenGL version.
     */
    struct SIMPLEGL_GL_EXPORT GLVersion {

        /** Constant for the major version. */
        const int major;

        /** Constant for the minor version. */
        const int minor;

        /** Default constructor. */
        GLVersion();

        /** Constructor for initializing major and minor version. */
        GLVersion(int major, int minor);

        /**
         * Overloaded less than operator.
         *
         * @param other The other version.
         */
        bool operator<(const GLVersion& other);

        /**
         * Overloaded less than or equal to operator.
         *
         * @param other The other version.
         */
        bool operator<=(const GLVersion& other);

        /**
         * Overloaded equal to operator.
         *
         * @param other The other version.
         */
        bool operator==(const GLVersion& other);

        /**
         * Overloaded unequal to operator.
         *
         * @param other The other version.
         */
        bool operator!=(const GLVersion& other);

        /**
         * Overloaded greater than or equal to operator.
         *
         * @param other The other version.
         */
        bool operator>=(const GLVersion& other);

        /**
         * Overloaded greater than operator.
         *
         * @param other The other version.
         */
        bool operator>(const GLVersion& other);
    };

    /** Stores the OpenGL version structure. */
    extern SIMPLEGL_GL_EXPORT GLVersion contextVersion;

    /** Initializes the OpenGL backend, this will also check available extensions and should be called after sgl::init(). */
    SIMPLEGL_GL_EXPORT void initGL();

    /**
     * Gets the GLint value from an enum class.
     * 
     * @param e The type of the enum class.
     * 
     * @return The underlying GLint value.
     */
    template<typename E>
    GLint to_GLint(E e);

    /**
     * Gets the GLenum value from an enum class.
     * 
     * @param e The type of the enum class.
     * 
     * @return The underlying GLenum value.
     */
    template<typename E>
    GLenum to_GLenum(E e);

    /**
     * Gets the GLbitfield value from an enum class.
     * 
     * @param e The type of the enum class.
     * 
     * @return The underlying GLbitfield value.
     */
    template<typename E>
    GLbitfield to_GLbitfield(E e);
}

#include "backend_gl.tcc"

#endif /* BACKEND_GL_HPP */
