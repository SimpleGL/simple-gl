/**
 * @file texture_object.hpp
 * @author Heiko Brumme
 *
 * This file contains classes to create texture objects.
 */

#ifndef TEXTURE_OBJECT_HPP
#define TEXTURE_OBJECT_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <GL/glew.h>

namespace sgl {

    /**
     * @enum TextureFilter
     *
     * This enum wraps the texture object's filter.
     */
    enum class TextureFilter : GLint {

        /** Equivalent to GL_NEAREST. */
        NEAREST = GL_NEAREST,

        /** Equivalent to GL_LINEAR. */
        LINEAR = GL_LINEAR
    };

    /**
     * @enum TextureWrap
     *
     * This enum wraps the texture object's wrapping functions.
     */
    enum class TextureWrap : GLint {

        /** Equivalent to GL_CLAMP_TO_EDGE. */
        CLAMP_TO_EDGE = GL_CLAMP_TO_EDGE,

        /** Equivalent to GL_CLAMP_TO_BORDER. */
        CLAMP_TO_BORDER = GL_CLAMP_TO_BORDER,

        /** Equivalent to GL_REPEAT. */
        REPEAT = GL_REPEAT,

        /** Equivalent to GL_MIRRORED_REPEAT. */
        MIRRORED_REPEAT = GL_MIRRORED_REPEAT
    };

    /**
     * @struct TextureData
     *
     * This structure stores texture image data.
     */
    struct SIMPLEGL_GL_EXPORT TextureData {

        /** The width of the image. */
        int width;

        /** The height of the image. */
        int height;

        /** The number of color channels. */
        int components;

        /** The pixel data of the image. */
        unsigned char* pixels;
    };

    /**
     * @class Texture
     *
     * This class wraps an OpenGL texture object.
     */
    class SIMPLEGL_GL_EXPORT Texture {

    private:
        /** Points at the currently bound texture object. */
        static Texture* current;

        /** The handle for the texture object. */
        GLuint handle;

        /** The width of the texture object in pixel. */
        int width;

        /** The height of the texture object in pixel. */
        int height;

    public:
        /** Generates a new texture object. */
        Texture(void);

        /** Deletes the texture object. */
        ~Texture(void);

        /**
         * Returns the handle of the texture object.
         *
         * @return The handle of the texture object.
         */
        GLuint getHandle();

        /**
         * Returns the width of the texture object.
         *
         * @return The width of the texture object in pixel.
         */
        int getWidth();

        /**
         * Returns the height of the texture object.
         *
         * @return The height of the texture object in pixel.
         */
        int getHeight();

        /** Binds the texture object. */
        void bind();

        /** Generates mipmaps for the texture object. */
        void generateMipMap();

        /**
         * Sets the minification filter of the texture object.
         *
         * @param filter The filter to set.
         */
        void setMinFilter(TextureFilter filter);

        /**
         * Sets the magnification filter of the texture object.
         *
         * @param filter The filter to set.
         */
        void setMagFilter(TextureFilter filter);

        /**
         * Sets the wrapping function of the texture object's coordinate S.
         *
         * @param wrap The wrapping function to set.
         */
        void setWrapS(TextureWrap wrap);

        /**
         * Sets the wrapping function of the texture object's coordinate T.
         *
         * @param wrap The wrapping function to set.
         */
        void setWrapT(TextureWrap wrap);

        /**
         * Uploads image data to the texture object.
         *
         * @param width The width of the image.
         * @param height The height of the image.
         * @param data The image data.
         */
        void uploadImage(int width, int height, unsigned char* data);

        /**
         * Uploads image data to the texture object.
         *
         * @param image The image data.
         */
        void uploadImage(TextureData image);
    };
}

#endif /* TEXTURE_OBJECT_HPP */
