/**
 * @file shader_objects.hpp
 * @author Heiko Brumme
 *
 * This file contains classes to create shader objects and shader programs.
 */

#ifndef SHADER_OBJECTS_HPP
#define SHADER_OBJECTS_HPP

#include <SimpleGL_gl/SimpleGL_gl_Export.h>

#include <string>

#include <GL/glew.h>

#include <glm/glm.hpp>

namespace sgl {

    /**
     * @enum ShaderType
     *
     * This enum wraps the shader types.
     */
    enum class ShaderType : GLenum {

        /** Equivalent to GL_VERTEX_SHADER. */
        VERTEX_SHADER = GL_VERTEX_SHADER,

        /** Equivalent to GL_FRAGMENT_SHADER. */
        FRAGMENT_SHADER = GL_FRAGMENT_SHADER
    };

    /**
     * @class Shader
     *
     * This class wraps an OpenGL shader object.
     */
    class SIMPLEGL_GL_EXPORT Shader {

    private:
        /** The handle for the shader object. */
        GLuint handle;

    public:
        /**
         * Creates a new shader object with specified type.
         *
         * @param type The shader object type.
         */
        Shader(ShaderType type);

        /** Deletes the shader object. */
        ~Shader(void);

        /**
         * Returns the handle of the shader object.
         *
         * @return The handle of the shader object.
         */
        GLuint getHandle();

        /**
         * Sets the source code of the shader object.
         *
         * @param source Source code to set.
         */
        void source(std::string source);

        /** Compiles the shader object. */
        void compile();

        /**
         * Checks the compile status of the shader object.
         *
         * @return true if the compilation was successful, else false.
         */
        bool getCompileStatus();

        /**
         * Gets the info log of the shader object.
         *
         * @return Information log of the shader object.
         */
        std::string getInfoLog();
    };

    /**
     * @class ShaderProgram
     *
     * This class wraps an OpenGL shader program. */
    class SIMPLEGL_GL_EXPORT ShaderProgram {

    private:
        /** Points at the currently used shader program. */
        static ShaderProgram* current;

        /** The handle for the shader program. */
        GLuint handle;

    public:
        /** Creates a new shader program. */
        ShaderProgram(void);

        /** Deletes the shader program. */
        ~ShaderProgram(void);

        /**
         * Returns the handle of the shader program.
         *
         * @return The handle of the shader program.
         */
        GLuint getHandle();

        /**
         * Attaches a shader object to the shader program.
         *
         * @param shader The shader object to attach.
         */
        void attach(Shader* shader);

        /** Links the shader program. */
        void link();

        /** Uses the shader program for the current rendering state. */
        void use();

        /**
         * Checks the link status of the shader program.
         *
         * @return true if the linking was successful, else false.
         */
        bool getLinkStatus();

        /**
         * Gets the info log of the shader program.
         *
         * @return Information log of the shader program.
         */
        std::string getInfoLog();

        /**
         * Binds the fragment out color variable.
         *
         * @param colorNumber The color number to bind.
         * @param name Name of the out variable.
         */
        void bindFragmentDataLocation(unsigned int colorNumber, std::string name);

        /**
         * Gets the location of an attribute variable with specified name.
         *
         * @param name The name of the attribute variable.
         *
         * @return The location of the attribute variable.
         */
        int getAttributeLocation(std::string name);

        /**
         * Gets the location of an uniform variable with specified name.
         *
         * @param name The name of the uniform variable.
         *
         * @return The location of the uniform variable.
         */
        int getUniformLocation(std::string name);

        /**
         * Sets the value of an uniform variable.
         *
         * @param location The location of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(int location, int value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param location The location of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(int location, float value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param location The location of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(int location, glm::vec2 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param location The location of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(int location, glm::vec3 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param location The location of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(int location, glm::vec4 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param location The location of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(int location, glm::mat2 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param location The location of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(int location, glm::mat3 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param location The location of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(int location, glm::mat4 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param name The name of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(std::string name, int value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param name The name of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(std::string name, float value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param name The name of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(std::string name, glm::vec2 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param name The name of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(std::string name, glm::vec3 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param name The name of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(std::string name, glm::vec4 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param name The name of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(std::string name, glm::mat2 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param name The name of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(std::string name, glm::mat3 value);

        /**
         * Sets the value of an uniform variable.
         *
         * @param name The name of the uniform variable.
         * @param value The value to be used.
         */
        void setUniform(std::string name, glm::mat4 value);
    };
}

#endif /* SHADER_OBJECTS_HPP */
