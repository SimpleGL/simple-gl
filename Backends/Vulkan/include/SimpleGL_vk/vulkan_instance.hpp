/**
 * @file vulkan_instance.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to create a Vulkan instance.
 */

#ifndef VULKAN_INSTANCE_HPP
#define VULKAN_INSTANCE_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <string>
#include <vector>

#include <vulkan/vulkan.h>

namespace sgl {

    /**
     * @class Instance
     * 
     * This class wraps a Vulkan instance.
     */
    class SIMPLEGL_VK_EXPORT Instance {

    private:
        /** The handle of the Vulkan instance. */
        VkInstance handle;

    public:
        /**
         * Creates a new Vulkan instance.
         * 
         * @param debugMode Indicates if the instance should include debug layers and extensions, default is false.
         */
        Instance(bool debugMode = false);

        /** Destroys the Vulkan instance. */
        ~Instance(void);

        /**
         * Returns the handle of the Vulkan instance.
         *
         * @return The handle of the Vulkan instance.
         */
        VkInstance getHandle();

        /**
         * Returns a function pointer for an instance function. This is only necessary for extension functions.
         * 
         * @param name The name of the function.
         * 
         * @return A function pointer for the instance function.
         */
        PFN_vkVoidFunction getProcAddr(std::string name);

        /**
         * Enumerates the physical devices accessible to this instance.
         * 
         * @return A list of accessible physical devices.
         */
        std::vector<VkPhysicalDevice> enumeratePhysicalDevices();
    };
}

#endif /* VULKAN_INSTANCE_HPP */
