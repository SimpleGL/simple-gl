/**
 * @file surface.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to create a Vulkan surface.
 */

#ifndef SURFACE_HPP
#define SURFACE_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vulkan/vulkan.h>

namespace sgl {

    /* Forward declaration needed because of a circular dependency */
    class WindowVK;

    /**
     * @class Surface
     * 
     * This class wraps a Vulkan surface.
     */
    class SIMPLEGL_VK_EXPORT Surface {

    private:
        /** Stores the handle of the Vulkan surface. */
        VkSurfaceKHR handle;

    public:
        /**
         * Creates a new window surface.
         * 
         * @param window The GLFW window to use.
         */
        Surface(WindowVK* window);

        /** Destroys the window surface. */
        ~Surface(void);

        /**
         * Returns the hande of the surface.
         * 
         * @return The hande of the surface.
         */
        VkSurfaceKHR getHandle();
    };
}

#endif /* SURFACE_HPP */
