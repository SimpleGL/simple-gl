/**
 * @file glfw_window_vk.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to generate a GLFW window with a Vulkan instance.
 */

#ifndef GLFW_WINDOW_VK_HPP
#define GLFW_WINDOW_VK_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <string>

#include <SimpleGL/glfw_window.hpp>

#include "vulkan_context.hpp"

namespace sgl {

    /* Forward declaration needed because of a circular dependency */
    class Surface;

    /**
     * @class WindowVK
     * 
     * This class provides a GLFW window with a Vulkan instance.
     */
    class SIMPLEGL_VK_EXPORT WindowVK : public Window {

    private:
        /** Stores the Vulkan context. */
        VulkanContext* context;

        /** Stores the window surface. */
        Surface* surface;

    public:
        /**
         * Creates a new window with the specified parameters.
         *
         * @param width The width of the window.
         * @param height The height of the window.
         * @param title The title of the window.
         */
        WindowVK(int width, int height, std::string title);

        /** Destroys the window. */
        ~WindowVK(void);

        /** Swaps the framebuffers of this window. */
        void swapBuffers() override;
    };
}

#endif /* GLFW_WINDOW_VK_HPP */
