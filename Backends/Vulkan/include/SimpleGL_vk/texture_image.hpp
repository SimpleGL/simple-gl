/**
 * @file texture_image.hpp
 * @author Heiko Brumme
 * 
 * This file contains classes for creating Vulkan texture images.
 */

#ifndef TEXTURE_IMAGE_HPP
#define TEXTURE_IMAGE_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vector>

#include <vulkan/vulkan.h>

namespace sgl {

    /**
     * @class TextureImage
     * 
     * This class wraps a Vulkan texture image.
     */
    class SIMPLEGL_VK_EXPORT TextureImage {

    private:
        /** Stores the currently used texture image. */
        static TextureImage* current;

        /** The handle of the texture image. */
        VkImage handle;

        /** The device memory of the texture image. */
        VkDeviceMemory memory;

        /** The image view of the texture image. */
        VkImageView imageView;

        /** The texture sampler for the image. */
        VkSampler textureSampler;

        /** The width of the texture image. */
        uint32_t width;

        /** The height of the texture image. */
        uint32_t height;

        /**
         * Performs a image layout transition.
         * 
         * @param oldLayout The old layout of the image.
         * @param newLayout The image layout to transit to.
         */
        void perfomImageLayoutTransition(VkImageLayout oldLayout, VkImageLayout newLayout);

        /**
         * Copies a buffer to a image.
         * 
         * @param buffer The buffer to copy from.
         * @param image The image to copy to.
         */
        void copyBufferToImage(VkBuffer buffer, VkImage image);

    public:
        /**
         * Returns the currently used texture image.
         * 
         * @return The currently used texture image.
         */
        static TextureImage* getCurrent();

        /**
         * Sets the currently used texture image.
         * 
         * @param current The texture image to set.
         */
        static void setCurrent(TextureImage* current);

        /**
         * Creates a new texture image.
         * 
         * @param width The width of the image.
         * @param height The height of the image.
         */
        TextureImage(uint32_t width, uint32_t height);

        /** Destroys the texture image. */
        ~TextureImage(void);

        /**
         * Returns the handle of the texture image.
         * 
         * @return The handle of the texture image.
         */
        VkImage getHandle();

        /**
         * Returns the image view of the texture image.
         * 
         * @return The image view of the texture image.
         */
        VkImageView getImageView();

        /**
         * Returns the texture sampler of the image.
         * 
         * @return The texture sampler of the image.
         */
        VkSampler getSampler();

        /**
         * Returns the width of the texture image.
         * 
         * @return The width of the texture image.
         */
        uint32_t getWidth();

        /**
         * Returns the height of the texture image.
         * 
         * @return The height of the texture image.
         */
        uint32_t getHeight();

        /**
         * Uploads data to the texture image.
         * 
         * @param data The data to upload.
         */
        void upload(std::vector<uint8_t> data);
    };
}

#endif /* TEXTURE_IMAGE_HPP */
