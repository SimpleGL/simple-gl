/**
 * @file debug_report.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to create a debug callback.
 */

#ifndef DEBUG_REPORT_HPP
#define DEBUG_REPORT_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <string>

#include <vulkan/vulkan.h>

namespace sgl {

    /**
     * @class DebugReportCallback
     * 
     * This class wraps a Vulkan debug report callback.
     */
    class SIMPLEGL_VK_EXPORT DebugReportCallback {

    protected:
        /** Stores the current instance of the debug report callback. */
        static DebugReportCallback* current;

        /** The handle of the debug report callback. */
        VkDebugReportCallbackEXT handle;

    public:
        /**
         * Returns the current debug report callback.
         * 
         * @return The current debug report callback.
         */
        static DebugReportCallback* getCurrent();

        /**
         * Sets the current debug report callback.
         * 
         * @param callback The debug report callback to set.
         */
        static void setCurrent(DebugReportCallback* callback);

        /**
         * Dispatches the member function.
         * 
         * @param flags indicates the VkDebugReportFlagBitsEXT that triggered this callback.
         * @param objectType is a VkDebugReportObjectTypeEXT value specifying the type of object being used or created at the time the event was triggered.
         * @param object is the object where the issue was detected. If objectType is VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT, object is undefined.
         * @param location is a component (layer, driver, loader) defined value that indicates the location of the trigger. This is an optional value.
         * @param messageCode is a layer-defined value indicating what test triggered this callback.
         * @param pLayerPrefix is a null-terminated string that is an abbreviation of the name of the component making the callback. pLayerPrefix is only valid for the duration of the callback.
         * @param pMessage is a null-terminated string detailing the trigger conditions. pMessage is only valid for the duration of the callback.
         * @param pUserData is the user data given when the VkDebugReportCallbackEXT was created.
         * 
         * @return a VkBool32, which is interpreted in a layer-specified manner. The application should always return VK_FALSE. The VK_TRUE value is reserved for use in layer development.
         */
        static VkBool32 dispatch(VkDebugReportFlagsEXT flags,
                                 VkDebugReportObjectTypeEXT objectType,
                                 uint64_t object,
                                 size_t location,
                                 int32_t messageCode,
                                 const char* pLayerPrefix,
                                 const char* pMessage,
                                 void* pUserData);

        /** Creates a debug report callback. */
        DebugReportCallback(void);

        /** Destroys a debug report callback. */
        ~DebugReportCallback(void);

        /**
         * Returns the handle of the debug report callback.
         * 
         * @return The handle of the debug report callback.
         */
        VkDebugReportCallbackEXT getHandle();

        /** 
         * This function gets called whenever a Vulkan debug report occurs.
         *
         * @param messageType The type of the message.
         * @param messageCode A message code.
         * @param message Message of the debug report.
         */
        virtual void invoke(std::string messageType, int messageCode, std::string message);
    };
}

#endif /* DEBUG_REPORT_HPP */
