/**
 * @file logical_device.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to create a logical device.
 */

#ifndef LOGICAL_DEVICE_HPP
#define LOGICAL_DEVICE_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vulkan/vulkan.h>

namespace sgl {

    /**
     * @struct Queues
     * 
     * This struct stores relevant device queues.
     */
    struct Queues {

        /** The handle of the graphics queue. */
        VkQueue graphics;

        /** The handle of the presentation queue. */
        VkQueue presentation;
    };

    /**
     * @class Device
     * 
     * This class wraps a Vulkan logical device.
     */
    class SIMPLEGL_VK_EXPORT Device {

    private:
        /** The handle of the logical device. */
        VkDevice handle;

        /** Stores the queues of the logical device. */
        Queues queues;

    public:
        /**
         * Creates a new logical device.
         * 
         * @param debugMode Indicates if the logical device should include debug layers and extensions, default is false.
         */
        Device(bool debugMode = false);

        /** Destroys the logical device. */
        ~Device(void);

        /**
         * Returns the handle of the logical device.
         * 
         * @return The handle of the logical device.
         */
        VkDevice getHandle();

        /**
         * Returns the device queues.
         * 
         * @return The device queues.
         */
        Queues getQueues();
    };
}

#endif /* LOGICAL_DEVICE_HPP */
