/**
 * @file vertex_buffer.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class for creating Vulkan buffer objects.
 */

#ifndef VERTEX_BUFFER_HPP
#define VERTEX_BUFFER_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vector>

#include <vulkan/vulkan.h>

#include <glm/glm.hpp>

namespace sgl {

    /**
     * @class VertexBuffer
     * 
     * This class wraps a Vulkan vertex buffer.
     */
    class SIMPLEGL_VK_EXPORT VertexBuffer {

    private:
        /** The handle of the vertex buffer. */
        VkBuffer handle;

        /** The device memory of the vertex buffer. */
        VkDeviceMemory memory;

    public:
        /**
         * Creates a new vertex buffer.
         * 
         * @param size The size of the buffer in bytes.
         */
        VertexBuffer(uint32_t size);

        /** Destroys the vertex buffer. */
        ~VertexBuffer(void);

        /**
         * Returns the handle of the vertex buffer.
         * 
         * @return The handle of the vertex buffer.
         */
        VkBuffer getHandle();

        /**
         * Uploads data to the vertex buffer.
         * 
         * @param data The data to upload.
         */
        void upload(std::vector<float> data);

        /**
         * Uploads data to the vertex buffer.
         * 
         * @param data The data to upload.
         */
        void upload(std::vector<glm::vec2> data);

        /**
         * Uploads data to the vertex buffer.
         * 
         * @param data The data to upload.
         */
        void upload(std::vector<glm::vec3> data);

        /**
         * Uploads data to the vertex buffer.
         * 
         * @param data The data to upload.
         */
        void upload(std::vector<glm::vec4> data);
    };

    /**
     * @class UniformBuffer
     * 
     * This class wraps a Vulkan uniform buffer.
     */
    class SIMPLEGL_VK_EXPORT UniformBuffer {

    private:
        /** The handle of the uniform buffer. */
        VkBuffer handle;

        /** The device memory of the uniform buffer. */
        VkDeviceMemory memory;

    public:
        /**
         * Creates a new uniform buffer.
         * 
         * @param size The size of the buffer in bytes.
         */
        UniformBuffer(uint32_t size);

        /** Destroys the vertex buffer. */
        ~UniformBuffer(void);

        /**
         * Returns the handle of the uniform buffer.
         * 
         * @return The handle of the uniform buffer.
         */
        VkBuffer getHandle();

        /**
         * Uploads data to the uniform buffer.
         * 
         * @param data The data to upload.
         */
        void upload(std::vector<glm::mat2> data);

        /**
         * Uploads data to the uniform buffer.
         * 
         * @param data The data to upload.
         */
        void upload(std::vector<glm::mat3> data);

        /**
         * Uploads data to the uniform buffer.
         * 
         * @param data The data to upload.
         */
        void upload(std::vector<glm::mat4> data);
    };
}

#endif /* VERTEX_BUFFER_HPP */
