/**
 * @file framebuffer.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to create a Vulkan framebuffer.
 */

#ifndef FRAMEBUFFER_HPP
#define FRAMEBUFFER_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vulkan/vulkan.h>

#include "render_pass.hpp"

namespace sgl {

    /**
     * @struct DepthStencilBuffer
     * 
     * This struct stores the data of a depth stencil buffer.
     */
    struct DepthStencilBuffer {

        /** Stores the depth stencil image. */
        VkImage image;

        /** Stores the depth stencil device memory. */
        VkDeviceMemory memory;

        /** Stores the depth stencil image view. */
        VkImageView imageView;

        /** Stores the depth stencil format. */
        VkFormat format;
    };

    /**
     * @class FramebufferVK
     * 
     * This class wraps a Vulkan framebuffer.
     */
    class SIMPLEGL_VK_EXPORT FramebufferVK {

    private:
        /** The handle of the framebuffer. */
        VkFramebuffer handle;

        /** Stores the depth stencil buffer data. */
        DepthStencilBuffer depthStencil;

        /** Creates a depth stencil buffer. */
        void createDepthStencilBuffer();

    public:
        /**
         * Creates a new framebuffer.
         * 
         * @param renderPass The render pass for this framebuffer.
         * @param imageView The image view for the framebuffer.
         */
        FramebufferVK(RenderPass* renderPass, VkImageView imageView);

        /** Destroys the framebuffer. */
        ~FramebufferVK(void);

        /**
         * Returns the handle of the framebuffer.
         * 
         * @return The handle of the framebuffer.
         */
        VkFramebuffer getHandle();

        /**
         * Returns the depth stencil buffer of the framebuffer.
         * 
         * @return The depth stencil buffer of the framebuffer.
         */
        DepthStencilBuffer getDepthStencilBuffer();
    };
}

#endif /* FRAMEBUFFER_HPP */
