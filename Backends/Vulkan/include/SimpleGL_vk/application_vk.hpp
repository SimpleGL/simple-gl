/**
 * @file application_vk.hpp
 * @author Heiko Brumme
 * 
 * This file contains the abstract Vulkan Application class.
 */

#ifndef APPLICATION_VK_HPP
#define APPLICATION_VK_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <string>

#include <SimpleGL/application.hpp>

#include "batching_vk.hpp"
#include "manager_vk.hpp"

namespace sgl {

    /**
     * @class ApplicationVK
     * 
     * This class should get extended by the user when using the Vulkan backend.
     */
    class SIMPLEGL_VK_EXPORT ApplicationVK : public Application {

    protected:
        /** The batch of this application. */
        BatchVK* batch;

        /** The shader module manager of this application. */
        ShaderModuleManager* shaderModuleManager;

        /** The texture image manager of this application. */
        TextureImageManager* textureImageManager;

    public:
        /**
         * Creates a new application.
         *
         * @param width The width of the window, default is 640 pixel.
         * @param height The height of the window, default is 480 pixel.
         * @param title The title of the window, default is "SimpleGL Application".
         */
        ApplicationVK(int width = 640, int height = 480, std::string title = "SimpleGL Application (Vulkan backend)");

        /** Destroys the application. */
        ~ApplicationVK(void);

        /** This method clears the color and depth buffers. */
        void clear() override;

        /** Disposes the application. */
        void dispose() override;
    };
}

#endif /* APPLICATION_VK_HPP */
