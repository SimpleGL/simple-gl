/**
 * @file physical_device.hpp
 * @author Heiko Brumme
 * 
 * This file contains classes to handle Vulkan physical devices.
 */

#ifndef PHYSICAL_DEVICE_HPP
#define PHYSICAL_DEVICE_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vector>

#include <vulkan/vulkan.h>

#include "surface.hpp"

namespace sgl {

    /**
     * @struct QueueFamilyIndices
     * 
     * This struct stores relevant queue family indices.
     */
    struct QueueFamilyIndices {

        /** The index of the graphics queue family. */
        int32_t graphics;

        /** The index of the presentation queue family. */
        int32_t presentation;

        /**
         * Checks if the queue family indices are valid.
         * 
         * @return true, if the queue family indices are valid.
         */
        bool isValid();
    };

    /**
     * @struct SwapchainSupportDetails
     * 
     * This struct stores necessary data for swapchain support.
     */
    struct SwapchainSupportDetails {

        /** The basic capabilities of a surface. */
        VkSurfaceCapabilitiesKHR surfaceCapabilities;

        /** The supported surface formats. */
        std::vector<VkSurfaceFormatKHR> surfaceFormats;

        /** The supported present modes. */
        std::vector<VkPresentModeKHR> presentModes;
    };

    /**
     * @class PhysicalDevice
     * 
     * This class wraps a Vulkan physical device and represents a graphics card.
     */
    class SIMPLEGL_VK_EXPORT PhysicalDevice {

    private:
        /** The handle of the physical device. */
        VkPhysicalDevice handle;

        /** Stores the queue family indices of the physical device. */
        QueueFamilyIndices queueFamilyIndices;

        /** Stores the swapchain support details of the physical device. */
        SwapchainSupportDetails swapchainSupportDetails;

        /**
         * Creates a new physical device object.
         * 
         * @param handle The Vulkan handle for the physical device.
         * @param surface The Window surface to check for a presentation queue.
         */
        PhysicalDevice(VkPhysicalDevice handle, Surface* surface);

        /** Checks if the physical device supports all necessary requirements. */
        void validate();

    public:
        /**
         * Returns a list of available physical devices.
         * 
         * @param surface The Window surface to check for a presentation queue.
         * 
         * @return A list of available physical devices.
         */
        static std::vector<PhysicalDevice*> getAvailablePhysicalDevices(Surface* surface);

        /**
         * Returns the most suitable physical device.
         * 
         * @param physicalDevices The list of available physical devices.
         * 
         * @return The most suitable physical device.
         */
        static PhysicalDevice* getSuitablePhysicalDevice(std::vector<PhysicalDevice*> physicalDevices);

        /** Destructor for the physical device. */
        ~PhysicalDevice(void);

        /**
         * Returns the handle of the physical device.
         * 
         * @return The handle of the physical device.
         */
        VkPhysicalDevice getHandle();

        /**
         * Returns the queue family indices of the physical device.
         * 
         * @return The queue family indices of the physical device.
         */
        QueueFamilyIndices getQueueFamilyIndices();

        /**
         * Returns the swapchain support details.
         * 
         * @return The swapchain support details.
         */
        SwapchainSupportDetails getSwapchainSupportDetails();

        /**
         * Returns the properties of this physical device.
         * 
         * @return The properties of this physical device.
         */
        VkPhysicalDeviceProperties getProperties();

        /**
         * Returns The features of this physical device.
         * 
         * @return the features of this physical device.
         */
        VkPhysicalDeviceFeatures getFeatures();

        /**
         * Returns the queue family properties of this physical device.
         * 
         * @return The queue family properties of this physical device.
         */
        std::vector<VkQueueFamilyProperties> getQueueFamilyProperties();
    };
}

#endif /* PHYSICAL_DEVICE_HPP */
