/**
 * @file batching_vk.hpp
 * @author Heiko Brumme
 * 
 * This file contains the class for batch rendering with Vulkan.
 */

#ifndef BATCHING_VK_HPP
#define BATCHING_VK_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vector>

#include <SimpleGL/batching.hpp>

#include "render_pass.hpp"
#include "graphics_pipeline.hpp"
#include "vertex_buffer.hpp"
#include "command_buffer.hpp"
#include "descriptor_set.hpp"
#include "texture_image.hpp"

namespace sgl {

    /**
     * @class BatchVK
     * 
     * This class is used for batch rendering with Vulkan.
     */
    class SIMPLEGL_VK_EXPORT BatchVK : public Batch {

    private:
        /** Stores the default vertex code. */
        static std::vector<uint8_t> vertexCode;

        /** Stores the default fragment code. */
        static std::vector<uint8_t> fragmentCode;

        /** Stores the default texture object. */
        static TextureImage* defaultTexture;

        /** Stores the render pass for this batch. */
        RenderPass* renderPass;

        /** Stores the graphics pipeline for this batch. */
        GraphicsPipeline* graphicsPipeline;

        /** Stores the buffer for position data. */
        VertexBuffer* positionBuffer;

        /** Stores the buffer for color data. */
        VertexBuffer* colorBuffer;

        /** Stores the buffer for texture coordinate data. */
        VertexBuffer* texCoordBuffer;

        /** Stores the buffer for normal data. */
        VertexBuffer* normalBuffer;

        /** Stores the uniform buffer for transformation matrices. */
        UniformBuffer* transformationBuffer;

        /** Stores the descriptor set for the batch. */
        DescriptorSet* descriptorSet;

        /** Stores the command buffer for this batch. */
        CommandBuffer* commandBuffer;

    public:
        /**
         * Creates a new batch with specified capacity.
         * If no capacity is specified it will use a default capacity of 1048576 vertices.
         *
         * @param capacity The capacity of this batch.
         */
        BatchVK(int capacity = 1024 * 1024);

        /** Destroys a batch. */
        ~BatchVK(void);

        /** Flushes the batch, could be used if the batch would overflow. */
        void flush() override;
    };
}

#endif /* BATCHING_VK_HPP */
