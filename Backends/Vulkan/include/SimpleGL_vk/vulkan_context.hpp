/**
 * @file vulkan_context.hpp
 * @author Heiko Brumme
 * 
 * This file contains a container class which holds relevant Vulkan objects.
 */

#ifndef VULKAN_CONTEXT_HPP
#define VULKAN_CONTEXT_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vector>

#include <glm/glm.hpp>

#include "vulkan_instance.hpp"
#include "debug_report.hpp"
#include "surface.hpp"
#include "physical_device.hpp"
#include "logical_device.hpp"
#include "swapchain.hpp"
#include "render_pass.hpp"
#include "framebuffer.hpp"
#include "command_buffer.hpp"
#include "descriptor_set.hpp"
#include "synchronization.hpp"

namespace sgl {

    /**
     * @class VulkanContext
     * 
     * This class contains all relevant Vulkan objects to setup a Vulkan application.
     */
    class SIMPLEGL_VK_EXPORT VulkanContext {

    private:
        /** Points at the current Vulkan context. */
        static VulkanContext* current;

        /** Stores the Vulkan instance for this context. */
        Instance* instance;

        /** Stores the debug report callback for this context. */
        DebugReportCallback* debugReportCallback;

        /** Stores the available physical devices for this context. */
        std::vector<PhysicalDevice*> availablePhysicalDevices;

        /** Stores the physical device for this context. */
        PhysicalDevice* physicalDevice;

        /** Stores the logical device for this context. */
        Device* device;

        /** Stores the swapchain for this context. */
        Swapchain* swapchain;

        /** Stores the swapchain framebuffer for this context. */
        std::vector<FramebufferVK*> swapchainFramebuffers;

        /** Stores the command pool for this context. */
        CommandPool* commandPool;

        /** Stores the descriptor pool for this context. */
        DescriptorPool* descriptorPool;

        /** Stores the fence for checking if rendering was finished */
        Fence* renderFence;

    public:
        /**
         * Returns the current Vulkan context.
         * 
         * @return The current Vulkan context.
         */
        static VulkanContext* getCurrent();

        /**
         * Sets the current Vulkan context.
         * 
         * @param context The Vulkan context to set.
         */
        static void setCurrent(VulkanContext* context);

        /** Creates a new Vulkan context. */
        VulkanContext(void);

        /** Destroys the Vulkan context. */
        ~VulkanContext(void);

        /**
         * Returns the Vulkan instance of this context.
         * 
         * @return The Vulkan instance of this context.
         */
        Instance* getInstance();

        /**
         * Returns the debug report callback of this context.
         * 
         * @return The debug report callback of this context.
         */
        DebugReportCallback* getDebugReportCallback();

        /**
         * Returns the available physical devices of this context.
         * 
         * @return The available physical devices of this context.
         */
        std::vector<PhysicalDevice*> getAvailablePhysicalDevices();

        /**
         * Returns the physical device of this context.
         * 
         * @return The physical device of this context.
         */
        PhysicalDevice* getPhysicalDevice();

        /**
         * Returns the logical device of this context.
         * 
         * @return The logical device of this context.
         */
        Device* getDevice();

        /**
         * Returns the swapchain of this context.
         * 
         * @return The swapchain of this context.
         */
        Swapchain* getSwapchain();

        /**
         * Returns the number of swapchain images.
         * 
         * @return The number of swapchain images.
         */
        uint32_t getSwapchainCount();

        /**
         * Returns the current swapchain image index.
         * 
         * @return The current swapchain image index.
         */
        uint32_t getSwapchainIndex();

        /**
         * Returns the current swapchain framebuffers of this context.
         * 
         * @return The current swapchain framebuffers of this context.
         */
        FramebufferVK* getSwapchainFramebuffer();

        /**
         * Returns the command pool of this context.
         * 
         * @return The command pool of this context.
         */
        CommandPool* getCommandPool();

        /**
         * Returns the descriptor pool of this context.
         * 
         * @return The descriptor pool of this context.
         */
        DescriptorPool* getDescriptorPool();

        /**
         * Returns the current fence for signaling that a command buffer has finished rendering.
         * 
         * @return The current fence for signaling that a command buffer has finished rendering.
         */
        Fence* getRenderFence();

        /** Creates a debug report callback, if compiled in debug mode. */
        void setupDebugCallback();

        /**
         * Enumerates all available physical devices and picks a suitable physical device.
         * 
         * @param surface The window surface for checking swapchain support.
         */
        void pickPhysicalDevice(Surface* surface);

        /** Creates the logical device for rendering. */
        void createLogicalDevice();

        /**
         * Creates a swapchain for this context.
         * 
         * @param surface The surface for the swapchain.
         * @param size The window size.
         */
        void createSwapchain(Surface* surface, glm::ivec2 size);

        /**
         * Creates the swapchain framebuffers for this context.
         * 
         * @param renderPass The render pass to use.
         */
        void createFramebuffers(RenderPass* renderPass);

        /** Creates the command pool for this context. */
        void createCommandPool();

        /** Create the descriptor pool for this context. */
        void createDescriptorPool();

        /** Creates the render fence. */
        void createRenderFence();
    };
}

#endif /* VULKAN_CONTEXT_HPP */
