/**
 * @file render_pass.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to create a render pass.
 */

#ifndef RENDER_PASS_HPP
#define RENDER_PASS_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vulkan/vulkan.h>

namespace sgl {

    /**
     * @class RenderPass
     * 
     * This class wraps a Vulkan render pass.
     */
    class SIMPLEGL_VK_EXPORT RenderPass {

    private:
        /** The handle of the render pass. */
        VkRenderPass handle;

    public:
        /** Creates a new render pass. */
        RenderPass(void);

        /** Destroys the render pass. */
        ~RenderPass(void);

        /**
         * Returns the handle of the render pass.
         * 
         * @return The handle of the render pass.
         */
        VkRenderPass getHandle();
    };
}

#endif /* RENDER_PASS_HPP */
