/**
 * @file backend_vk.hpp
 * @author Heiko Brumme
 * 
 * This file contains some general functions for the Vulkan Backend.
 */

#ifndef BACKEND_VK_HPP
#define BACKEND_VK_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

namespace sgl {
    /** Initializes the Vulkan backend, this will also check available extensions and should be called after sgl::init(). */
    SIMPLEGL_VK_EXPORT void initVK();
}

#endif /* BACKEND_VK_HPP */
