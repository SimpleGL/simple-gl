/**
 * @file graphics_pipeline.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to create a graphics pipeline.
 */

#ifndef GRAPHICS_PIPELINE_HPP
#define GRAPHICS_PIPELINE_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vector>

#include <vulkan/vulkan.h>

#include "render_pass.hpp"
#include "shader_module.hpp"

namespace sgl {

    /**
     * @class GraphicsPipeline
     * 
     * This class wraps a Vulkan graphics pipeline.
     */
    class SIMPLEGL_VK_EXPORT GraphicsPipeline {

    private:
        /** The handle of the graphics pipeline. */
        VkPipeline handle;

        /** The descriptor set layout of the graphics pipeline. */
        VkDescriptorSetLayout descriptorSetLayout;

        /** The pipeline layout of the graphics pipeline. */
        VkPipelineLayout pipelineLayout;

    public:
        /**
         * Creates a new graphics pipeline.
         * 
         * @param renderPass The render pass to use.
         * @param shaderModules The shader modules to use.
         */
        GraphicsPipeline(RenderPass* renderPass, std::vector<ShaderModule*> shaderModules);

        /** Destroys the graphics pipeline. */
        ~GraphicsPipeline(void);

        /**
         * Returns the handle of the graphics pipeline.
         * 
         * @return The handle of the graphics pipeline.
         */
        VkPipeline getHandle();

        /**
         * Returns the descriptor set layout of the graphics pipeline.
         * 
         * @return The descriptor set layout of the graphics pipeline.
         */
        VkDescriptorSetLayout getDescriptorSetLayout();

        /**
         * Returns the pipeline layout of the graphics pipeline.
         * 
         * @return The pipeline layout of the graphics pipeline.
         */
        VkPipelineLayout getPipelineLayout();
    };
}

#endif /* GRAPHICS_PIPELINE_HPP */

