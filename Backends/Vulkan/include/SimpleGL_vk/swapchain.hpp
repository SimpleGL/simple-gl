/**
 * @file swapchain.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to create a Vulkan swapchain.
 */

#ifndef SWAPCHAIN_HPP
#define SWAPCHAIN_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vector>

#include <vulkan/vulkan.h>

#include <glm/glm.hpp>

#include "surface.hpp"

namespace sgl {

    /**
     * @struct SwapchainData
     * 
     * This struct stores relevant swapchain data.
     */
    struct SwapchainData {

        /** The available swapchain images. */
        std::vector<VkImage> images;

        /** The image views for the swapchain images. */
        std::vector<VkImageView> imageViews;

        /** The swapchain image format. */
        VkFormat imageFormat;

        /** The swapchain extent. */
        VkExtent2D extent;

        /** The current swapchain image index. */
        uint32_t index;
    };

    /**
     * @class Swapchain
     * 
     * This class wraps a Vulkan swapchain.
     */
    class SIMPLEGL_VK_EXPORT Swapchain {

    private:
        /** The handle of the swapchain. */
        VkSwapchainKHR handle;

        /** Stores the swapchain data. */
        SwapchainData swapchainData;

    public:
        /**
         * Creates a new swapchain.
         * 
         * @param surface The surface to use.
         * @param size The size of the window drawing area.
         */
        Swapchain(Surface* surface, glm::ivec2 size);

        /** Destroys a swapchain. */
        ~Swapchain(void);

        /**
         * Returns the handle of the swapchain.
         * 
         * @return The handle of the swapchain.
         */
        VkSwapchainKHR getHandle();

        /**
         * Returns the swapchain data.
         * 
         * @return The swapchain data.
         */
        SwapchainData getSwapchainData();

        /** Updates the index for the swapchain images. */
        void acquireNextImage();

        /** Presents the swapchain image. */
        void present();
    };
}

#endif /* SWAPCHAIN_HPP */
