/**
 * @file manager_vk.hpp
 * @author Heiko Brumme
 * 
 * This file contains classes for managing Vulkan objects.
 */

#ifndef MANAGER_VK_HPP
#define MANAGER_VK_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <string>

#include <SimpleGL/manager.hpp>

#include "shader_module.hpp"
#include "texture_image.hpp"

namespace sgl {

    /**
     * @class ShaderModuleManager
     * 
     * This class provides methods for managing shader modules.
     */
    class SIMPLEGL_VK_EXPORT ShaderModuleManager : public Manager<ShaderModule> {

    public:
        /**
         * Removes and disposes a shader module from the storage.
         *
         * @param name The name of the shader module.
         */
        void remove(std::string name) override;

        /** Removes all shader modules and disposes them. */
        void clear() override;

        /**
         * Loads a shader module from a binary file and stores it.
         *
         * @param path File path to the shader module binary source.
         * @param name The name of the shader module.
         * @param stageBit The shader stage flag bit. Either VERTEX_BIT or FRAGMENT_BIT.
         *
         * @return The loaded shader module.
         */
        ShaderModule* loadShaderModule(std::string path, std::string name, ShaderStageBit stageBit);
    };

    /**
     * @class TextureImageManager
     * 
     * This class provides methods for managing texture images.
     */
    class SIMPLEGL_VK_EXPORT TextureImageManager : public Manager<TextureImage> {

    public:
        /**
         * Removes and disposes a texture image from the storage.
         *
         * @param name The name of the texture image.
         */
        void remove(std::string name) override;

        /** Removes all texture images and disposes them. */
        void clear() override;

        /**
         * Loads a texture from file and stores it.
         *
         * @param path File path to the image.
         * @param name The name of the texture.
         *
         * @return The loaded texture image.
         */
        TextureImage* load(std::string path, std::string name);
    };
}

#endif /* MANAGER_VK_HPP */
