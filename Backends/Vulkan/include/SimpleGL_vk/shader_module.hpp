/**
 * @file shader_module.hpp
 * @author Heiko Brumme
 * 
 * This file contains a class to create a shader module.
 */

#ifndef SHADER_MODULE_HPP
#define SHADER_MODULE_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vector>

#include <vulkan/vulkan.h>

namespace sgl {

    /**
     * @enum ShaderStageBit
     * 
     * This enum stores supported shader stage flag bit.
     */
    enum class ShaderStageBit {

        /** Indicates to use VK_SHADER_STAGE_VERTEX_BIT */
        VERTEX_BIT,

        /** Indicates to use VK_SHADER_STAGE_FRAGMENT_BIT */
        FRAGMENT_BIT
    };

    /**
     * @class ShaderModule
     * 
     * This class wraps a Vulkan shader module.
     */
    class SIMPLEGL_VK_EXPORT ShaderModule {

    private:
        /** The handle of the shader module. */
        VkShaderModule handle;

        /** Stores the shader stage bit. */
        ShaderStageBit shaderStageBit;

    public:
        /**
         * Creates a new shader module.
         * 
         * @param code The binary code for the shader module.
         * @param stage Indicates which shader stage flag bit should get used.
         */
        ShaderModule(std::vector<uint8_t> code, ShaderStageBit stage);

        /** Destroys the shader module. */
        ~ShaderModule(void);

        /**
         * Returns the handle of the shader module.
         * 
         * @return The handle of the shader module.
         */
        VkShaderModule getHandle();

        /**
         * Returns the shader stage bit.
         * 
         * @return The shader stage bit.
         */
        VkShaderStageFlagBits getShaderStageBit();
    };
}

#endif /* SHADER_MODULE_HPP */
