/**
 * @file synchronization.hpp
 * @author Heiko Brumme
 * 
 * This file provides classes to create Vulkan synchronization objects.
 */

#ifndef SYNCHRONISATION_HPP
#define SYNCHRONISATION_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vulkan/vulkan.h>

namespace sgl {

    /**
     * @class Semaphore
     * 
     * This class wraps a Vulkan semaphore.
     */
    class SIMPLEGL_VK_EXPORT Semaphore {

    private:
        /** The handle of the semaphore. */
        VkSemaphore handle;

    public:
        /** Creates a new semaphore. */
        Semaphore(void);

        /** Destroys the semaphore. */
        ~Semaphore(void);

        /**
         * Returns the handle of the semaphore.
         * 
         * @return The handle of the semaphore.
         */
        VkSemaphore getHandle();
    };

    /**
     * @class Fence
     * 
     * This class wraps a Vulkan fence.
     */
    class SIMPLEGL_VK_EXPORT Fence {

    private:
        /** The handle of the fence. */
        VkFence handle;

    public:
        /** Creates a new fence. */
        Fence(void);

        /** Destroys the fence. */
        ~Fence(void);

        /**
         * Returns the handle of the fence.
         * 
         * @return The handle of the fence.
         */
        VkFence getHandle();
    };
}

#endif /* SYNCHRONISATION_HPP */
