/**
 * @file command_buffer.hpp
 * @author Heiko Brumme
 * 
 * This file contains classes to create Vulkan command pools and buffers.
 */

#ifndef COMMAND_BUFFER_HPP
#define COMMAND_BUFFER_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vulkan/vulkan.h>

#include "render_pass.hpp"
#include "graphics_pipeline.hpp"
#include "vertex_buffer.hpp"
#include "descriptor_set.hpp"

namespace sgl {

    /**
     * @class CommandPool
     * 
     * This class wraps a Vulkan command pool.
     */
    class SIMPLEGL_VK_EXPORT CommandPool {

    private:
        /** The handle of the command pool. */
        VkCommandPool handle;

    public:
        /** Creates a new command pool. */
        CommandPool(void);

        /** Destroys the command pool. */
        ~CommandPool(void);

        /**
         * Returns the handle of the command pool.
         * 
         * @return The handle of the command pool.
         */
        VkCommandPool getHandle();
    };

    /**
     * @class CommandBuffer
     * 
     * This class wraps a Vulkan command buffer.
     */
    class SIMPLEGL_VK_EXPORT CommandBuffer {

    private:
        /** The handle of the command buffer. */
        VkCommandBuffer handle;

    public:
        /** Creates a new command buffer. */
        CommandBuffer(void);

        /** Destroys the command buffer. */
        ~CommandBuffer(void);

        /**
         * Returns the handle of the command buffer.
         * 
         * @return The handle of the command buffer.
         */
        VkCommandBuffer getHandle();

        /** Begins recording of the command buffer. */
        void begin();

        /** Ends recording of the command buffer. */
        void end();

        /**
         * Begins a render pass.
         * 
         * @param renderPass The render pass to use.
         */
        void beginRenderPass(RenderPass* renderPass);

        /** Ends the current render pass. */
        void endRenderPass();

        /**
         * Binds a graphics pipeline to the command buffer.
         * 
         * @param pipeline The graphics pipeline to bind.
         */
        void bindPipeline(GraphicsPipeline* pipeline);

        /**
         * Binds a vertex buffer to the command buffer.
         * 
         * @param vertexBuffer The vertex buffer to bind.
         * @param binding The binding index of the vertex buffer.
         */
        void bindVertexBuffer(VertexBuffer* vertexBuffer, uint32_t binding);

        /**
         * Binds a descriptor set to the command buffer.
         * 
         * @param descriptorSet The descriptor set to bind.
         * @param graphicsPipeline The graphics pipeline to get the layout from.
         * @param binding The binding index of the descriptor set.
         */
        void bindDescriptorSet(DescriptorSet* descriptorSet, GraphicsPipeline* graphicsPipeline, uint32_t binding);

        /**
         * Draws primitives.
         * 
         * @param vertexCount The number of vertices to draw.
         */
        void draw(uint32_t vertexCount);

        /** Submits the command buffer. */
        void submit();
    };
}

#endif /* COMMAND_BUFFER_HPP */
