/**
 * @file descriptor_set.hpp
 * @author Heiko Brumme
 * 
 * This file contains classes to create Vulkan descriptor pools and sets.
 */

#ifndef DESCRIPTOR_SET_HPP
#define DESCRIPTOR_SET_HPP

#include <SimpleGL_vk/SimpleGL_vk_Export.h>

#include <vector>

#include <vulkan/vulkan.h>

#include <glm/glm.hpp>

#include "graphics_pipeline.hpp"
#include "vertex_buffer.hpp"
#include "texture_image.hpp"

namespace sgl {

    /**
     * @class DescriptorPool
     * 
     * This class wraps a Vulkan descriptor pool.
     */
    class SIMPLEGL_VK_EXPORT DescriptorPool {

    private:
        /** The handle of the descriptor pool. */
        VkDescriptorPool handle;

    public:
        /** Creates a new descriptor pool. */
        DescriptorPool(void);

        /** Destroys the descriptor pool. */
        ~DescriptorPool(void);

        /**
         * Returns the handle of the descriptor pool.
         * 
         * @return The handle of the descriptor pool.
         */
        VkDescriptorPool getHandle();
    };

    /**
     * @class DescriptorSet
     * 
     * This class wraps a Vulkan descriptor set.
     */
    class SIMPLEGL_VK_EXPORT DescriptorSet {

    private:
        /** The handle of the descriptor set. */
        VkDescriptorSet handle;

    public:
        /**
         * Creates a new descriptor set.
         * 
         * @param graphicsPipeline The graphics pipeline to get the descriptor set layout from.
         */
        DescriptorSet(GraphicsPipeline* graphicsPipeline);

        /** Destroys the descriptor set. */
        ~DescriptorSet(void);

        /**
         * Returns the handle of the descriptor set.
         * 
         * @return The handle of the descriptor set.
         */
        VkDescriptorSet getHandle();

        /**
         * Updates the descriptor set.
         * 
         * @param uniformBuffer The uniform buffer to write.
         * @param data The value to write into the descriptor set.
         */
        void update(UniformBuffer* uniformBuffer, std::vector<glm::mat2> data);

        /**
         * Updates the descriptor set.
         * 
         * @param uniformBuffer The uniform buffer to write.
         * @param data The value to write into the descriptor set.
         */
        void update(UniformBuffer* uniformBuffer, std::vector<glm::mat3> data);

        /**
         * Updates the descriptor set.
         * 
         * @param uniformBuffer The uniform buffer to write.
         * @param data The value to write into the descriptor set.
         */
        void update(UniformBuffer* uniformBuffer, std::vector<glm::mat4> data);

        /**
         * Updates the descriptor set.
         * 
         * @param textureImage The imager to write into the descriptor set.
         */
        void update(TextureImage* textureImage);
    };
}

#endif /* DESCRIPTOR_SET_HPP */
