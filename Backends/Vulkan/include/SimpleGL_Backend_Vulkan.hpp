/**
 * @file SimpleGL_Backend_Vulkan.hpp
 * @author Heiko Brumme
 * 
 * This file is meant to get included by projects using the Vulkan backend.
 *
 * In this file all the headers of the Vulkan backend should get included, so
 * that the user just have to include this header.
 */

#ifndef SIMPLEGL_BACKEND_VULKAN_HPP
#define SIMPLEGL_BACKEND_VULKAN_HPP

/* SimpleGL core */
#include <SimpleGL_Core.hpp>

/* SimpleGL Vulkan backend */
#include "SimpleGL_vk/backend_vk.hpp"
#include "SimpleGL_vk/application_vk.hpp"
#include "SimpleGL_vk/glfw_window_vk.hpp"
#include "SimpleGL_vk/batching_vk.hpp"
#include "SimpleGL_vk/manager_vk.hpp"

/* Vulkan wrapper */
#include "SimpleGL_vk/vulkan_context.hpp"
#include "SimpleGL_vk/vulkan_instance.hpp"
#include "SimpleGL_vk/debug_report.hpp"
#include "SimpleGL_vk/surface.hpp"
#include "SimpleGL_vk/physical_device.hpp"
#include "SimpleGL_vk/logical_device.hpp"
#include "SimpleGL_vk/swapchain.hpp"
#include "SimpleGL_vk/render_pass.hpp"
#include "SimpleGL_vk/shader_module.hpp"
#include "SimpleGL_vk/graphics_pipeline.hpp"
#include "SimpleGL_vk/framebuffer.hpp"
#include "SimpleGL_vk/command_buffer.hpp"
#include "SimpleGL_vk/descriptor_set.hpp"
#include "SimpleGL_vk/vertex_buffer.hpp"
#include "SimpleGL_vk/texture_image.hpp"
#include "SimpleGL_vk/synchronization.hpp"

#endif /* SIMPLEGL_BACKEND_VULKAN_HPP */
