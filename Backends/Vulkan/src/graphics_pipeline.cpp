/**
 * @file graphics_pipeline.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the graphics_pipeline.hpp header.
 */

#include "SimpleGL_vk/graphics_pipeline.hpp"

#include <stdexcept>

#include <glm/glm.hpp>

#include "SimpleGL_vk/vulkan_context.hpp"
#include "SimpleGL_vk/swapchain.hpp"

using namespace std;
using namespace glm;

namespace sgl {

    GraphicsPipeline::GraphicsPipeline(RenderPass* renderPass, std::vector<ShaderModule*> shaderModules) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        SwapchainData swapchainData = VulkanContext::getCurrent()->getSwapchain()->getSwapchainData();

        /* Initialize the shader stage create infos */
        vector<VkPipelineShaderStageCreateInfo> shaderStages(shaderModules.size());
        for (uint32_t i = 0; i < shaderStages.size(); i++) {
            shaderStages[i] = {};
            shaderStages[i].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            shaderStages[i].stage = shaderModules[i]->getShaderStageBit();
            shaderStages[i].module = shaderModules[i]->getHandle();
            shaderStages[i].pName = "main";
        }

        /* Initialize the vertex input binding descriptions */
        vector<VkVertexInputBindingDescription> vertexBindingDescriptions(4);

        /* Position vertex binding */
        vertexBindingDescriptions[0].binding = 0;
        vertexBindingDescriptions[0].stride = sizeof (vec3);
        vertexBindingDescriptions[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        /* Color vertex binding */
        vertexBindingDescriptions[1].binding = 1;
        vertexBindingDescriptions[1].stride = sizeof (vec4);
        vertexBindingDescriptions[1].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        /* TexCoord vertex binding */
        vertexBindingDescriptions[2].binding = 2;
        vertexBindingDescriptions[2].stride = sizeof (vec2);
        vertexBindingDescriptions[2].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        /* Normal vertex binding */
        vertexBindingDescriptions[3].binding = 3;
        vertexBindingDescriptions[3].stride = sizeof (vec3);
        vertexBindingDescriptions[3].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

        /* Initialize the vertex input attribute descriptions */
        vector<VkVertexInputAttributeDescription> vertexAttributeDescriptions(4);

        /* Position vertex attribute */
        vertexAttributeDescriptions[0].location = 0;
        vertexAttributeDescriptions[0].binding = 0;
        vertexAttributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
        vertexAttributeDescriptions[0].offset = 0;

        /* Color vertex attribute */
        vertexAttributeDescriptions[1].location = 1;
        vertexAttributeDescriptions[1].binding = 1;
        vertexAttributeDescriptions[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
        vertexAttributeDescriptions[1].offset = 0;

        /* TexCoord vertex attribute */
        vertexAttributeDescriptions[2].location = 2;
        vertexAttributeDescriptions[2].binding = 2;
        vertexAttributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
        vertexAttributeDescriptions[2].offset = 0;

        /* Normal vertex attribute */
        vertexAttributeDescriptions[3].location = 3;
        vertexAttributeDescriptions[3].binding = 3;
        vertexAttributeDescriptions[3].format = VK_FORMAT_R32G32B32_SFLOAT;
        vertexAttributeDescriptions[3].offset = 0;

        /* Initialize a vertex input state create info */
        VkPipelineVertexInputStateCreateInfo vertexInputState = {};
        vertexInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertexInputState.vertexBindingDescriptionCount = vertexBindingDescriptions.size();
        vertexInputState.pVertexBindingDescriptions = vertexBindingDescriptions.data();
        vertexInputState.vertexAttributeDescriptionCount = vertexAttributeDescriptions.size();
        vertexInputState.pVertexAttributeDescriptions = vertexAttributeDescriptions.data();

        /* Initialize a input assembly state create info */
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyState = {};
        inputAssemblyState.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssemblyState.primitiveRestartEnable = VK_FALSE;

        /* Initialize a viewport */
        VkViewport viewport = {};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = swapchainData.extent.width;
        viewport.height = swapchainData.extent.height;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        /* Initialize a scissor rectangle */
        VkRect2D scissor = {};
        scissor.offset.x = 0;
        scissor.offset.y = 0;
        scissor.extent = swapchainData.extent;

        /* Initialize a viewport state create info */
        VkPipelineViewportStateCreateInfo viewportState = {};
        viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.viewportCount = 1;
        viewportState.pViewports = &viewport;
        viewportState.scissorCount = 1;
        viewportState.pScissors = &scissor;

        /* Initialize a rasterization state create info */
        VkPipelineRasterizationStateCreateInfo rasterizationState = {};
        rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizationState.depthClampEnable = VK_FALSE;
        rasterizationState.rasterizerDiscardEnable = VK_FALSE;
        rasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rasterizationState.depthBiasEnable = VK_FALSE;
        rasterizationState.lineWidth = 1.0f;

        /* Initialize a multisample state create info */
        VkPipelineMultisampleStateCreateInfo multisampleState = {};
        multisampleState.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        multisampleState.sampleShadingEnable = VK_FALSE;

        /* Initialize a depth stencil state create info */
        VkPipelineDepthStencilStateCreateInfo depthStencilState = {};
        depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthStencilState.depthTestEnable = VK_TRUE;
        depthStencilState.depthWriteEnable = VK_TRUE;
        depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS;
        depthStencilState.depthBoundsTestEnable = VK_FALSE;
        depthStencilState.stencilTestEnable = VK_FALSE;

        /* Inizialize a color blend attachment state */
        VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
        colorBlendAttachment.blendEnable = VK_FALSE;
        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

        /* Initialize a color blend state create info */
        VkPipelineColorBlendStateCreateInfo colorBlendState = {};
        colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlendState.logicOpEnable = VK_FALSE;
        colorBlendState.attachmentCount = 1;
        colorBlendState.pAttachments = &colorBlendAttachment;

        /* Initialize a descriptor set layout binding */
        vector<VkDescriptorSetLayoutBinding> bindings(2);
        bindings[0].binding = 0;
        bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        bindings[0].descriptorCount = 1;
        bindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        bindings[1].binding = 1;
        bindings[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        bindings[1].descriptorCount = 1;
        bindings[1].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        bindings[1].pImmutableSamplers = nullptr;

        /* Initialize a descriptor set layout create info */
        VkDescriptorSetLayoutCreateInfo setLayoutCreateInfo = {};
        setLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        setLayoutCreateInfo.bindingCount = bindings.size();
        setLayoutCreateInfo.pBindings = bindings.data();

        /* Create a descriptor set layout */
        if (vkCreateDescriptorSetLayout(device, &setLayoutCreateInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS) {
            throw runtime_error("Could not create a descriptor set layout!");
        }

        /* Create a pipeline layout */
        VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
        pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutCreateInfo.setLayoutCount = 1;
        pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;
        if (vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
            throw runtime_error("Could not create a pipeline layout!");
        }

        /* Initialize a graphics pipeline create info */
        VkGraphicsPipelineCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        createInfo.stageCount = shaderStages.size();
        createInfo.pStages = shaderStages.data();
        createInfo.pVertexInputState = &vertexInputState;
        createInfo.pInputAssemblyState = &inputAssemblyState;
        createInfo.pViewportState = &viewportState;
        createInfo.pRasterizationState = &rasterizationState;
        createInfo.pMultisampleState = &multisampleState;
        createInfo.pDepthStencilState = &depthStencilState;
        createInfo.pColorBlendState = &colorBlendState;
        createInfo.layout = pipelineLayout;
        createInfo.renderPass = renderPass->getHandle();
        createInfo.subpass = 0;

        /* Create the graphics pipeline */
        if (vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a graphics pipeline!");
        }
    }

    GraphicsPipeline::~GraphicsPipeline(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the descriptor set layout */
        vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);

        /* Destroy the pipeline layout */
        vkDestroyPipelineLayout(device, pipelineLayout, nullptr);

        /* Destroy the graphics pipeline */
        vkDestroyPipeline(device, handle, nullptr);
    }

    VkPipeline GraphicsPipeline::getHandle() {
        return handle;
    }

    VkDescriptorSetLayout GraphicsPipeline::getDescriptorSetLayout() {
        return descriptorSetLayout;
    }

    VkPipelineLayout GraphicsPipeline::getPipelineLayout() {
        return pipelineLayout;
    }
}
