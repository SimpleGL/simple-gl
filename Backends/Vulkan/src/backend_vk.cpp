/**
 * @file backend_vk.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the backend_vk.hpp header.
 */

#include "SimpleGL_vk/backend_vk.hpp"

#include <cstdlib>
#include <cstring>

#include <vector>
#include <string>

#include <vulkan/vulkan.h>

#include <GLFW/glfw3.h>

#include <SimpleGL/logging.hpp>

using namespace std;

namespace sgl {

    void initVK() {
        Logger::logInfo("Checking Vulkan support...");
        if (glfwVulkanSupported()) {
            Logger::logInfo("Vulkan is supported!");
        } else {
            Logger::logError("Vulkan is not supported, you may want to update your graphics driver.");
            exit(EXIT_FAILURE);
        }

        Logger::logInfo("Checking Vulkan layers...");

        /* Get number of available layers */
        uint32_t availableLayerCount = 0;
        vkEnumerateInstanceLayerProperties(&availableLayerCount, nullptr);

        /* Get all available layers */
        vector<VkLayerProperties> availableLayers(availableLayerCount);
        vkEnumerateInstanceLayerProperties(&availableLayerCount, availableLayers.data());

        /* Add required layers */
        vector<const char*> requiredLayers;
        requiredLayers.push_back("VK_LAYER_LUNARG_standard_validation");

        /* Check required layers */
        for (uint32_t i = 0; i < requiredLayers.size(); i++) {
            bool found = false;
            for (uint32_t j = 0; j < availableLayers.size(); j++) {
                if (strcmp(requiredLayers[i], availableLayers[j].layerName)) {
                    found = true;
                    break;
                }
            }
            if (found) {
                Logger::logInfo(string(requiredLayers[i]) + " is supported!");
            } else {
                Logger::logError(string(requiredLayers[i]) + " is not supported.");
                exit(EXIT_FAILURE);
            }
        }

        Logger::logInfo("Checking Vulkan extensions...");

        /* Get number of available extensions */
        uint32_t availableExtensionCount = 0;
        vkEnumerateInstanceExtensionProperties(nullptr, &availableExtensionCount, nullptr);

        /* Get all available extensions */
        vector<VkExtensionProperties> availableExtensions(availableExtensionCount);
        vkEnumerateInstanceExtensionProperties(nullptr, &availableExtensionCount, availableExtensions.data());

        /* Get required GLFW extensions */
        uint32_t glfwExtensionCount = 0;
        const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

        /* Add additional required extensions */
        vector<const char*> requiredExtensions(glfwExtensions, glfwExtensions + glfwExtensionCount);
        requiredExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);

        /* Check required extensions */
        for (uint32_t i = 0; i < requiredExtensions.size(); i++) {
            bool found = false;
            for (uint32_t j = 0; j < availableExtensions.size(); j++) {
                if (strcmp(requiredExtensions[i], availableExtensions[j].extensionName)) {
                    found = true;
                    break;
                }
            }
            if (found) {
                Logger::logInfo(string(requiredExtensions[i]) + " is supported!");
            } else {
                Logger::logError(string(requiredExtensions[i]) + " is not supported.");
                exit(EXIT_FAILURE);
            }
        }
    }
}
