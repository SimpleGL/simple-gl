/**
 * @file debug_report.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the debug_report.hpp header.
 */

#include "SimpleGL_vk/debug_report.hpp"

#include <stdexcept>

#include <SimpleGL/logging.hpp>

#include "SimpleGL_vk/vulkan_context.hpp"
#include "SimpleGL_vk/vulkan_instance.hpp"

using namespace std;

namespace sgl {
    DebugReportCallback* DebugReportCallback::current = nullptr;

    DebugReportCallback* DebugReportCallback::getCurrent() {
        return current;
    }

    void DebugReportCallback::setCurrent(DebugReportCallback* callback) {
        /* Only set if callback is not a nullptr */
        if (callback != nullptr) {
            current = callback;
        }
    }

    VkBool32 DebugReportCallback::dispatch(VkDebugReportFlagsEXT flags,
                                           VkDebugReportObjectTypeEXT objectType,
                                           uint64_t object,
                                           size_t location,
                                           int32_t messageCode,
                                           const char* pLayerPrefix,
                                           const char* pMessage,
                                           void* pUserData) {
        /* Instance shouldn't be null when calling this function */
        if (current) {
            string messageType = "DEBUG REPORT";
            if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT) {
                messageType = "ERROR";
            } else if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT) {
                messageType = "WARNING";
            } else if (flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT) {
                messageType = "PERFORMANCE WARNING";
            } else if (flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT) {
                messageType = "INFO";
            } else if (flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT) {
                messageType = "DEBUG";
            }
            current->invoke(messageType, messageCode, string(pMessage));
        }

        return VK_FALSE;
    }

    DebugReportCallback::DebugReportCallback(void) {
        Instance* instance = VulkanContext::getCurrent()->getInstance();

        /* Initialize the create info */
        VkDebugReportCallbackCreateInfoEXT createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
        createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
        createInfo.pfnCallback = DebugReportCallback::dispatch;

        /* Create the Vulkan debug report callback */
        auto vkCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT) instance->getProcAddr("vkCreateDebugReportCallbackEXT");
        if (vkCreateDebugReportCallbackEXT(instance->getHandle(), &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create debug report callback!");
        }
    }

    DebugReportCallback::~DebugReportCallback(void) {
        Instance* instance = VulkanContext::getCurrent()->getInstance();

        /* Destroy the debug report callback */
        auto vkDestroyDebugReportCallbackEXT = (PFN_vkDestroyDebugReportCallbackEXT) instance->getProcAddr("vkDestroyDebugReportCallbackEXT");
        vkDestroyDebugReportCallbackEXT(instance->getHandle(), handle, nullptr);

        /* Set instance to nullptr if this was the current debug report callback */
        if (DebugReportCallback::current == this) {
            DebugReportCallback::current = nullptr;
        }
    }

    VkDebugReportCallbackEXT DebugReportCallback::getHandle() {
        return handle;
    }

    /** Default implementation of the debug report callback. */
    void DebugReportCallback::invoke(std::string messageType, int messageCode, std::string message) {
        Logger::logError("Vulkan " + messageType + " Code " + to_string(messageCode) + ": " + message);
    }
}
