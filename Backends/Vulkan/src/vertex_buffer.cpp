/**
 * @file vertex_buffer.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the vertex_buffer.hpp header.
 */

#include "SimpleGL_vk/vertex_buffer.hpp"

#include <cstring>

#include <stdexcept>

#include "SimpleGL_vk/vulkan_context.hpp"

using namespace std;
using namespace glm;

namespace sgl {

    VertexBuffer::VertexBuffer(uint32_t size) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkPhysicalDevice physicalDevice = VulkanContext::getCurrent()->getPhysicalDevice()->getHandle();

        /* Initialize a buffer create info */
        VkBufferCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        createInfo.size = size;
        createInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
        createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        /* Create the vertex buffer */
        if (vkCreateBuffer(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a vertex buffer!");
        }

        /* Get memory requirements */
        VkMemoryRequirements memoryRequirements;
        vkGetBufferMemoryRequirements(device, handle, &memoryRequirements);

        /* Get the device memory properties */
        VkPhysicalDeviceMemoryProperties memoryProperties;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

        /* Determine memory type index */
        uint32_t memoryTypeIndex = -1;
        VkMemoryPropertyFlags flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++) {
            if ((memoryRequirements.memoryTypeBits & (1 << i)) &&
                (memoryProperties.memoryTypes[i].propertyFlags & flags) == flags) {
                memoryTypeIndex = i;
                break;
            }
        }

        /* Initialize a memory allocate info */
        VkMemoryAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocateInfo.allocationSize = memoryRequirements.size;
        allocateInfo.memoryTypeIndex = memoryTypeIndex;

        /* Allocate the vertex buffer memory */
        if (vkAllocateMemory(device, &allocateInfo, nullptr, &memory) != VK_SUCCESS) {
            throw runtime_error("Could not allocate vertex buffer memory!");
        }

        /* Bind the vertex buffer memory */
        vkBindBufferMemory(device, handle, memory, 0);
    }

    VertexBuffer::~VertexBuffer(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the vertex buffer and free the memory */
        vkDestroyBuffer(device, handle, nullptr);
        vkFreeMemory(device, memory, nullptr);
    }

    VkBuffer VertexBuffer::getHandle() {
        return handle;
    }

    void VertexBuffer::upload(std::vector<float> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Upload data */
        void* mapData;
        vkMapMemory(device, memory, 0, data.size() * sizeof (float), 0, &mapData);
        memcpy(mapData, data.data(), data.size() * sizeof (float));
        vkUnmapMemory(device, memory);
    }

    void VertexBuffer::upload(std::vector<glm::vec2> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Upload data */
        void* mapData;
        vkMapMemory(device, memory, 0, data.size() * sizeof (vec2), 0, &mapData);
        memcpy(mapData, data.data(), data.size() * sizeof (vec2));
        vkUnmapMemory(device, memory);
    }

    void VertexBuffer::upload(std::vector<glm::vec3> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Upload data */
        void* mapData;
        vkMapMemory(device, memory, 0, data.size() * sizeof (vec3), 0, &mapData);
        memcpy(mapData, data.data(), data.size() * sizeof (vec3));
        vkUnmapMemory(device, memory);
    }

    void VertexBuffer::upload(std::vector<glm::vec4> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Upload data */
        void* mapData;
        vkMapMemory(device, memory, 0, data.size() * sizeof (vec4), 0, &mapData);
        memcpy(mapData, data.data(), data.size() * sizeof (vec4));
        vkUnmapMemory(device, memory);
    }

    UniformBuffer::UniformBuffer(uint32_t size) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkPhysicalDevice physicalDevice = VulkanContext::getCurrent()->getPhysicalDevice()->getHandle();

        /* Initialize a buffer create info */
        VkBufferCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        createInfo.size = size;
        createInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        /* Create the vertex buffer */
        if (vkCreateBuffer(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a uniform buffer!");
        }

        /* Get memory requirements */
        VkMemoryRequirements memoryRequirements;
        vkGetBufferMemoryRequirements(device, handle, &memoryRequirements);

        /* Get the device memory properties */
        VkPhysicalDeviceMemoryProperties memoryProperties;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

        /* Determine memory type index */
        uint32_t memoryTypeIndex = -1;
        VkMemoryPropertyFlags flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++) {
            if ((memoryRequirements.memoryTypeBits & (1 << i)) &&
                (memoryProperties.memoryTypes[i].propertyFlags & flags) == flags) {
                memoryTypeIndex = i;
                break;
            }
        }

        /* Initialize a memory allocate info */
        VkMemoryAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocateInfo.allocationSize = memoryRequirements.size;
        allocateInfo.memoryTypeIndex = memoryTypeIndex;

        /* Allocate the uniform buffer memory */
        if (vkAllocateMemory(device, &allocateInfo, nullptr, &memory) != VK_SUCCESS) {
            throw runtime_error("Could not allocate uniform buffer memory!");
        }

        /* Bind the uniform buffer memory */
        vkBindBufferMemory(device, handle, memory, 0);
    }

    UniformBuffer::~UniformBuffer(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the uniform buffer and free the memory */
        vkDestroyBuffer(device, handle, nullptr);
        vkFreeMemory(device, memory, nullptr);
    }

    VkBuffer UniformBuffer::getHandle() {
        return handle;
    }

    void UniformBuffer::upload(std::vector<glm::mat2> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Upload data */
        void* mapData;
        vkMapMemory(device, memory, 0, data.size() * sizeof (mat2), 0, &mapData);
        memcpy(mapData, data.data(), data.size() * sizeof (mat2));
        vkUnmapMemory(device, memory);
    }

    void UniformBuffer::upload(std::vector<glm::mat3> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Upload data */
        void* mapData;
        vkMapMemory(device, memory, 0, data.size() * sizeof (mat3), 0, &mapData);
        memcpy(mapData, data.data(), data.size() * sizeof (mat3));
        vkUnmapMemory(device, memory);
    }

    void UniformBuffer::upload(std::vector<glm::mat4> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Upload data */
        void* mapData;
        vkMapMemory(device, memory, 0, data.size() * sizeof (mat4), 0, &mapData);
        memcpy(mapData, data.data(), data.size() * sizeof (mat4));
        vkUnmapMemory(device, memory);
    }
}
