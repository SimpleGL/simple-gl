/**
 * @file physical_device.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the physical_device.hpp header.
 */

#include "SimpleGL_vk/physical_device.hpp"

#include <cstdlib>
#include <cstring>

#include <stdexcept>
#include <string>

#include <SimpleGL/logging.hpp>

#include "SimpleGL_vk/vulkan_context.hpp"
#include "SimpleGL_vk/vulkan_instance.hpp"

using namespace std;

namespace sgl {

    bool QueueFamilyIndices::isValid() {
        return graphics >= 0 && presentation >= 0;
    }

    std::vector<PhysicalDevice*> PhysicalDevice::getAvailablePhysicalDevices(Surface* surface) {
        Instance* instance = VulkanContext::getCurrent()->getInstance();

        /* Get all available graphic cards */
        vector<VkPhysicalDevice> devices = instance->enumeratePhysicalDevices();
        vector<PhysicalDevice*> physicalDevices(devices.size());
        for (uint32_t i = 0; i < devices.size(); i++) {
            physicalDevices[i] = new PhysicalDevice(devices[i], surface);
        }

        return physicalDevices;
    }

    PhysicalDevice* PhysicalDevice::getSuitablePhysicalDevice(std::vector<PhysicalDevice*> physicalDevices) {
        PhysicalDevice* suitablePhysicalDevice = physicalDevices[0];

        for (uint32_t i = 0; i < physicalDevices.size(); i++) {
            /* Get physical device properties */
            VkPhysicalDeviceProperties currentProperties = suitablePhysicalDevice->getProperties();
            VkPhysicalDeviceProperties candidateProperties = physicalDevices[i]->getProperties();

            /* Prefer discrete GPUs over integrated or virtual GPUs */
            if (candidateProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU &&
                currentProperties.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
                if (suitablePhysicalDevice->queueFamilyIndices.isValid()) {
                    suitablePhysicalDevice = physicalDevices[i];
                    break;
                }
            }
        }

        /* Check if the selected device supports the necessary requirements */
        suitablePhysicalDevice->validate();

        return suitablePhysicalDevice;
    }

    PhysicalDevice::PhysicalDevice(VkPhysicalDevice handle, Surface* surface) {
        this->handle = handle;
        queueFamilyIndices.graphics = -1;
        queueFamilyIndices.presentation = -1;

        /* Find the queue family indices */
        vector<VkQueueFamilyProperties> queueFamilyProperties = getQueueFamilyProperties();
        for (uint32_t i = 0; i < queueFamilyProperties.size(); i++) {
            /* Check for graphics support */
            bool graphicsSupport = queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT;
            if (queueFamilyProperties[i].queueCount > 0 && graphicsSupport) {
                queueFamilyIndices.graphics = i;
            }

            /* Check for presentation support */
            VkBool32 presentationSupport = VK_FALSE;
            vkGetPhysicalDeviceSurfaceSupportKHR(handle, i, surface->getHandle(), &presentationSupport);
            if (queueFamilyProperties[i].queueCount > 0 && presentationSupport) {
                queueFamilyIndices.presentation = i;
            }

            /* Check if necesarry indices are found */
            if (queueFamilyIndices.isValid()) {
                break;
            }
        }

        /* Get surface capabilities */
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(handle, surface->getHandle(), &swapchainSupportDetails.surfaceCapabilities);

        /* Get number of supported surface formats */
        uint32_t surfaceFormatCount = 0;
        vkGetPhysicalDeviceSurfaceFormatsKHR(handle, surface->getHandle(), &surfaceFormatCount, nullptr);

        /* Get all supported surface formats */
        if (surfaceFormatCount > 0) {
            swapchainSupportDetails.surfaceFormats.resize(surfaceFormatCount);
            vkGetPhysicalDeviceSurfaceFormatsKHR(handle, surface->getHandle(), &surfaceFormatCount, swapchainSupportDetails.surfaceFormats.data());
        }

        /* Get number of supported present modes */
        uint32_t presentModeCount = 0;
        vkGetPhysicalDeviceSurfacePresentModesKHR(handle, surface->getHandle(), &presentModeCount, nullptr);

        /* Get all supported present modes */
        if (presentModeCount > 0) {
            swapchainSupportDetails.presentModes.resize(presentModeCount);
            vkGetPhysicalDeviceSurfacePresentModesKHR(handle, surface->getHandle(), &presentModeCount, swapchainSupportDetails.presentModes.data());
        }
    }

    PhysicalDevice::~PhysicalDevice(void) {
        /* Nothing to do here */
    }

    VkPhysicalDevice PhysicalDevice::getHandle() {
        return handle;
    }

    QueueFamilyIndices PhysicalDevice::getQueueFamilyIndices() {
        return queueFamilyIndices;
    }

    SwapchainSupportDetails PhysicalDevice::getSwapchainSupportDetails() {
        return swapchainSupportDetails;
    }

    VkPhysicalDeviceProperties PhysicalDevice::getProperties() {
        VkPhysicalDeviceProperties properties;
        vkGetPhysicalDeviceProperties(handle, &properties);

        return properties;
    }

    VkPhysicalDeviceFeatures PhysicalDevice::getFeatures() {
        VkPhysicalDeviceFeatures features;
        vkGetPhysicalDeviceFeatures(handle, &features);

        return features;
    }

    std::vector<VkQueueFamilyProperties> PhysicalDevice::getQueueFamilyProperties() {
        /* Get number of queue families */
        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(handle, &queueFamilyCount, nullptr);

        /* Get all queue family properties */
        vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(handle, &queueFamilyCount, queueFamilyProperties.data());

        return queueFamilyProperties;
    }

    void PhysicalDevice::validate() {
        /* Check if the device supports the necessary queue families */
        if (queueFamilyIndices.graphics < 0) {
            throw runtime_error("Could not find a queue family that supports graphics!");
        } else if (queueFamilyIndices.presentation < 0) {
            throw runtime_error("Could not find a queue family that supports presentation!");
        }

        /* Get number of extension properties */
        uint32_t availableExtensionCount = 0;
        vkEnumerateDeviceExtensionProperties(handle, nullptr, &availableExtensionCount, nullptr);

        /* Get all extension properties */
        vector<VkExtensionProperties> availableExtensions(availableExtensionCount);
        vkEnumerateDeviceExtensionProperties(handle, nullptr, &availableExtensionCount, availableExtensions.data());

        /* Add required extensions */
        vector<const char*> requiredExtensions;
        requiredExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

        /* Check required extensions */
        for (uint32_t i = 0; i < requiredExtensions.size(); i++) {
            bool found = false;
            for (uint32_t j = 0; j < availableExtensions.size(); j++) {
                if (strcmp(requiredExtensions[i], availableExtensions[j].extensionName)) {
                    found = true;
                    break;
                }
            }
            if (found) {
                Logger::logInfo(string(requiredExtensions[i]) + " is supported!");
            } else {
                Logger::logError(string(requiredExtensions[i]) + " is not supported.");
                exit(EXIT_FAILURE);
            }
        }

        /* Check if the swapchain support is sufficient */
        if (swapchainSupportDetails.surfaceFormats.empty()) {
            throw runtime_error("Could not find a supported surface format!");
        } else if (swapchainSupportDetails.presentModes.empty()) {
            throw runtime_error("Could not find a supported present mode!");
        }
    }
}
