/**
 * @file surface.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the surface.hpp header.
 */

#include "SimpleGL_vk/surface.hpp"
#include "SimpleGL_vk/glfw_window_vk.hpp"

#include <stdexcept>

#include <GLFW/glfw3.h>

#include "SimpleGL_vk/vulkan_context.hpp"

using namespace std;

namespace sgl {

    Surface::Surface(WindowVK* window) {
        VkInstance instance = VulkanContext::getCurrent()->getInstance()->getHandle();

        /* Create the window surface */
        if (glfwCreateWindowSurface(instance, window->getHandle(), nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a window surface!");
        }
    }

    Surface::~Surface(void) {
        VkInstance instance = VulkanContext::getCurrent()->getInstance()->getHandle();

        /* Destroy the window surface */
        vkDestroySurfaceKHR(instance, handle, nullptr);
    }

    VkSurfaceKHR Surface::getHandle() {
        return handle;
    }
}
