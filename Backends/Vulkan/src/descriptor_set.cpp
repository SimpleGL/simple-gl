/**
 * @file descriptor_set.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the descriptor_set.hpp header.
 */

#include "SimpleGL_vk/descriptor_set.hpp"

#include <stdexcept>

#include "SimpleGL_vk/vulkan_context.hpp"

using namespace std;
using namespace glm;

namespace sgl {

    DescriptorPool::DescriptorPool(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Initialize a descriptor pool size */
        vector<VkDescriptorPoolSize> poolSizes(2);
        poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        poolSizes[0].descriptorCount = 1;
        poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        poolSizes[1].descriptorCount = 1;

        /* Initialize a descriptor pool create info */
        VkDescriptorPoolCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        createInfo.maxSets = 1;
        createInfo.poolSizeCount = poolSizes.size();
        createInfo.pPoolSizes = poolSizes.data();

        /* Create the descriptor pool */
        if (vkCreateDescriptorPool(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a descriptor pool!");
        }
    }

    DescriptorPool::~DescriptorPool(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the descriptor pool */
        vkDestroyDescriptorPool(device, handle, nullptr);
    }

    VkDescriptorPool DescriptorPool::getHandle() {
        return handle;
    }

    DescriptorSet::DescriptorSet(GraphicsPipeline* graphicsPipeline) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkDescriptorPool descriptorPool = VulkanContext::getCurrent()->getDescriptorPool()->getHandle();
        VkDescriptorSetLayout layout = graphicsPipeline->getDescriptorSetLayout();

        /* Initialize a descriptor set allocate info */
        VkDescriptorSetAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocateInfo.descriptorPool = descriptorPool;
        allocateInfo.descriptorSetCount = 1;
        allocateInfo.pSetLayouts = &layout;

        /* Allocate the descriptor set */
        if (vkAllocateDescriptorSets(device, &allocateInfo, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not allocate descriptor set!");
        }
    }

    DescriptorSet::~DescriptorSet(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkDescriptorPool descriptorPool = VulkanContext::getCurrent()->getDescriptorPool()->getHandle();

        /* Free the descriptor set */
        vkFreeDescriptorSets(device, descriptorPool, 1, &handle);
    }

    VkDescriptorSet DescriptorSet::getHandle() {
        return handle;
    }

    void DescriptorSet::update(UniformBuffer* uniformBuffer, std::vector<glm::mat2> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Initialize a descriptor buffer info */
        VkDescriptorBufferInfo bufferInfo = {};
        bufferInfo.buffer = uniformBuffer->getHandle();
        bufferInfo.offset = 0;
        bufferInfo.range = data.size() * sizeof (mat2);

        /* Initialize a write descriptor set */
        VkWriteDescriptorSet descriptorWrite = {};
        descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet = handle;
        descriptorWrite.dstBinding = 0;
        descriptorWrite.dstArrayElement = 0;
        descriptorWrite.descriptorCount = 1;
        descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrite.pBufferInfo = &bufferInfo;

        /* Update the descriptor set */
        vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
    }

    void DescriptorSet::update(UniformBuffer* uniformBuffer, std::vector<glm::mat3> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Initialize a descriptor buffer info */
        VkDescriptorBufferInfo bufferInfo = {};
        bufferInfo.buffer = uniformBuffer->getHandle();
        bufferInfo.offset = 0;
        bufferInfo.range = data.size() * sizeof (mat3);

        /* Initialize a write descriptor set */
        VkWriteDescriptorSet descriptorWrite = {};
        descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet = handle;
        descriptorWrite.dstBinding = 0;
        descriptorWrite.dstArrayElement = 0;
        descriptorWrite.descriptorCount = 1;
        descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrite.pBufferInfo = &bufferInfo;

        /* Update the descriptor set */
        vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
    }

    void DescriptorSet::update(UniformBuffer* uniformBuffer, std::vector<glm::mat4> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Initialize a descriptor buffer info */
        VkDescriptorBufferInfo bufferInfo = {};
        bufferInfo.buffer = uniformBuffer->getHandle();
        bufferInfo.offset = 0;
        bufferInfo.range = data.size() * sizeof (mat4);

        /* Initialize a write descriptor set */
        VkWriteDescriptorSet descriptorWrite = {};
        descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet = handle;
        descriptorWrite.dstBinding = 0;
        descriptorWrite.dstArrayElement = 0;
        descriptorWrite.descriptorCount = 1;
        descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrite.pBufferInfo = &bufferInfo;

        /* Update the descriptor set */
        vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
    }

    void DescriptorSet::update(TextureImage* textureImage) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Initialize a descriptor image info */
        VkDescriptorImageInfo imageInfo = {};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
        imageInfo.imageView = textureImage->getImageView();
        imageInfo.sampler = textureImage->getSampler();

        /* Initialize a write descriptor set */
        VkWriteDescriptorSet descriptorWrite = {};
        descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet = handle;
        descriptorWrite.dstBinding = 1;
        descriptorWrite.dstArrayElement = 0;
        descriptorWrite.descriptorCount = 1;
        descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrite.pImageInfo = &imageInfo;

        /* Update the descriptor set */
        vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
    }
}
