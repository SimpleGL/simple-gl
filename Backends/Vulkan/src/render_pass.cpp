/**
 * @file render_pass.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the render_pass.hpp header.
 */

#include "SimpleGL_vk/render_pass.hpp"

#include <stdexcept>
#include <vector>

#include "SimpleGL_vk/vulkan_context.hpp"
#include "SimpleGL_vk/swapchain.hpp"

using namespace std;

namespace sgl {

    RenderPass::RenderPass(void) {
        VkPhysicalDevice physicalDevice = VulkanContext::getCurrent()->getPhysicalDevice()->getHandle();
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        SwapchainData swapchainData = VulkanContext::getCurrent()->getSwapchain()->getSwapchainData();

        /* Initialize depth stencil format candidates */
        VkFormat depthStencilFormat = VK_FORMAT_UNDEFINED;
        vector<VkFormat> candidates = {VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D16_UNORM_S8_UINT};

        /* Find a supported format */
        for (uint32_t i = 0; i < candidates.size(); i++) {
            depthStencilFormat = candidates[i];

            /* Get physical device format properties */
            VkFormatProperties formatProperties;
            vkGetPhysicalDeviceFormatProperties(physicalDevice, depthStencilFormat, &formatProperties);

            /* Check if it is supported */
            if (formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
                break;
            } else if (formatProperties.linearTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
                break;
            }
        }
        if (depthStencilFormat == VK_FORMAT_UNDEFINED) {
            throw runtime_error("Could not find a supported depth stencil format!");
        }

        /* Initialize the color attachment description */
        VkAttachmentDescription colorAttachment = {};
        colorAttachment.format = swapchainData.imageFormat;
        colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        /* Initialize the color attachment reference */
        VkAttachmentReference colorAttachmentReference = {};
        colorAttachmentReference.attachment = 0;
        colorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        /* Initialize a depth stencil attachment description */
        VkAttachmentDescription depthStencilAttachment = {};
        depthStencilAttachment.format = depthStencilFormat;
        depthStencilAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        depthStencilAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        depthStencilAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depthStencilAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        depthStencilAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depthStencilAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        depthStencilAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        /* Initialize the depth stencil attachment reference */
        VkAttachmentReference depthStencilAttachmentReference = {};
        depthStencilAttachmentReference.attachment = 1;
        depthStencilAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        /* Initialize a subpass description */
        VkSubpassDescription subpass = {};
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &colorAttachmentReference;
        subpass.pDepthStencilAttachment = &depthStencilAttachmentReference;

        /* Initialize a subpass dependency */
        VkSubpassDependency dependency = {};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

        /* Initialize the attachments vector */
        vector<VkAttachmentDescription> attachments = {colorAttachment, depthStencilAttachment};

        /* Initialize a render pass create info */
        VkRenderPassCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        createInfo.attachmentCount = attachments.size();
        createInfo.pAttachments = attachments.data();
        createInfo.subpassCount = 1;
        createInfo.pSubpasses = &subpass;
        createInfo.dependencyCount = 1;
        createInfo.pDependencies = &dependency;

        /* Create the render pass */
        if (vkCreateRenderPass(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a render pass!");
        }
    }

    RenderPass::~RenderPass(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the render pass */
        vkDestroyRenderPass(device, handle, nullptr);
    }

    VkRenderPass RenderPass::getHandle() {
        return handle;
    }
}
