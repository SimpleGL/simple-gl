/**
 * @file vulkan_instance.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the vulkan_instance.hpp header.
 */

#include "SimpleGL_vk/vulkan_instance.hpp"

#include <stdexcept>
#include <vector>

#include <GLFW/glfw3.h>

using namespace std;

namespace sgl {

    Instance::Instance(bool debugMode) {
        /* Initialize the application info */
        VkApplicationInfo appInfo = {};
        appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInfo.pApplicationName = "SimpleGL Application (Vulkan backend)";
        appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.pEngineName = "SimpleGL";
        appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.apiVersion = VK_API_VERSION_1_0;

        /* Add required layers */
        vector<const char*> requiredLayers;
        if (debugMode) {
            requiredLayers.push_back("VK_LAYER_LUNARG_standard_validation");
        }

        /* Get required GLFW extensions */
        uint32_t glfwExtensionCount = 0;
        const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

        /* Add additional required extensions */
        vector<const char*> requiredExtensions(glfwExtensions, glfwExtensions + glfwExtensionCount);
        if (debugMode) {
            requiredExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
        }

        /* Initialize the create info */
        VkInstanceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.pApplicationInfo = &appInfo;
        createInfo.enabledLayerCount = requiredLayers.size();
        createInfo.ppEnabledLayerNames = requiredLayers.data();
        createInfo.enabledExtensionCount = requiredExtensions.size();
        createInfo.ppEnabledExtensionNames = requiredExtensions.data();

        /* Create the Vulkan instance */
        if (vkCreateInstance(&createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create Vulkan instance!");
        }
    }

    Instance::~Instance(void) {
        /* Destroy Vulkan instance */
        vkDestroyInstance(handle, nullptr);
    }

    VkInstance Instance::getHandle() {
        return handle;
    }

    PFN_vkVoidFunction Instance::getProcAddr(std::string name) {
        return vkGetInstanceProcAddr(handle, name.c_str());
    }

    std::vector<VkPhysicalDevice> Instance::enumeratePhysicalDevices() {
        /* Get number of available graphic cards */
        uint32_t physicalDeviceCount = 0;
        vkEnumeratePhysicalDevices(handle, &physicalDeviceCount, nullptr);
        if (physicalDeviceCount == 0) {
            throw runtime_error("Failed to find a GPU with Vulkan support!");
        }

        /* Get all available graphic cards */
        vector<VkPhysicalDevice> physicalDevices(physicalDeviceCount);
        vkEnumeratePhysicalDevices(handle, &physicalDeviceCount, physicalDevices.data());

        return physicalDevices;
    }
}
