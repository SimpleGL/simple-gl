/**
 * @file texture_image.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the texture_image.hpp header.
 */

#include "SimpleGL_vk/texture_image.hpp"

#include <cstring>

#include <stdexcept>

#include "SimpleGL_vk/vulkan_context.hpp"

using namespace std;

namespace sgl {

    TextureImage* TextureImage::current;

    TextureImage* TextureImage::getCurrent() {
        return current;
    }

    void TextureImage::setCurrent(TextureImage* current) {
        /* Only set if current isn't a nullptr */
        if (current != nullptr) {
            TextureImage::current = current;
        }
    }

    TextureImage::TextureImage(uint32_t width, uint32_t height) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkPhysicalDevice physicalDevice = VulkanContext::getCurrent()->getPhysicalDevice()->getHandle();
        this->width = width;
        this->height = height;

        /* Initialize a image create info */
        VkImageCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        createInfo.imageType = VK_IMAGE_TYPE_2D;
        createInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
        createInfo.extent.width = width;
        createInfo.extent.height = height;
        createInfo.extent.depth = 1;
        createInfo.mipLevels = 1;
        createInfo.arrayLayers = 1;
        createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
        createInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
        createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        /* Create the image */
        if (vkCreateImage(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a texture image!");
        }

        /* Get memory requirements */
        VkMemoryRequirements memoryRequirements;
        vkGetImageMemoryRequirements(device, handle, &memoryRequirements);

        /* Get the device memory properties */
        VkPhysicalDeviceMemoryProperties memoryProperties;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

        /* Determine memory type index */
        uint32_t memoryTypeIndex = -1;
        VkMemoryPropertyFlags flags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++) {
            if ((memoryRequirements.memoryTypeBits & (1 << i)) &&
                (memoryProperties.memoryTypes[i].propertyFlags & flags) == flags) {
                memoryTypeIndex = i;
                break;
            }
        }

        /* Initialize a memory allocate info */
        VkMemoryAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocateInfo.allocationSize = memoryRequirements.size;
        allocateInfo.memoryTypeIndex = memoryTypeIndex;

        /* Allocate the texture image memory */
        if (vkAllocateMemory(device, &allocateInfo, nullptr, &memory) != VK_SUCCESS) {
            throw runtime_error("Could not allocate texture image memory!");
        }

        /* Bind the texture image memory */
        vkBindImageMemory(device, handle, memory, 0);
    }

    TextureImage::~TextureImage(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the texture objects and free the memory */
        vkDestroySampler(device, textureSampler, nullptr);
        vkDestroyImageView(device, imageView, nullptr);
        vkDestroyImage(device, handle, nullptr);
        vkFreeMemory(device, memory, nullptr);

        /* Set current to nullptr if this texture image was used */
        if (TextureImage::current == this) {
            TextureImage::current = nullptr;
        }
    }

    VkImage TextureImage::getHandle() {
        return handle;
    }

    VkImageView TextureImage::getImageView() {
        return imageView;
    }

    VkSampler TextureImage::getSampler() {
        return textureSampler;
    }

    uint32_t TextureImage::getWidth() {
        return width;
    }

    uint32_t TextureImage::getHeight() {
        return height;
    }

    void TextureImage::upload(std::vector<uint8_t> data) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkPhysicalDevice physicalDevice = VulkanContext::getCurrent()->getPhysicalDevice()->getHandle();

        /* Initialize a staging buffer */
        VkBuffer stagingBuffer;
        VkDeviceMemory stagingBufferMemory;

        /* Initialize a buffer create info */
        VkBufferCreateInfo bufferInfo = {};
        bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferInfo.size = data.size();
        bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        /* Create a staging buffer */
        if (vkCreateBuffer(device, &bufferInfo, nullptr, &stagingBuffer) != VK_SUCCESS) {
            throw runtime_error("Could not create a staging buffer!");
        }

        /* Get memory requirements */
        VkMemoryRequirements memoryRequirements;
        vkGetBufferMemoryRequirements(device, stagingBuffer, &memoryRequirements);

        /* Get the device memory properties */
        VkPhysicalDeviceMemoryProperties memoryProperties;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

        /* Determine memory type index */
        uint32_t memoryTypeIndex = -1;
        VkMemoryPropertyFlags flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++) {
            if ((memoryRequirements.memoryTypeBits & (1 << i)) &&
                (memoryProperties.memoryTypes[i].propertyFlags & flags) == flags) {
                memoryTypeIndex = i;
                break;
            }
        }

        /* Initialize a memory allocate info */
        VkMemoryAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocateInfo.allocationSize = memoryRequirements.size;
        allocateInfo.memoryTypeIndex = memoryTypeIndex;

        /* Allocate staging buffer memory */
        if (vkAllocateMemory(device, &allocateInfo, nullptr, &stagingBufferMemory) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate buffer memory!");
        }

        /* Bind the staging buffer memory */
        vkBindBufferMemory(device, stagingBuffer, stagingBufferMemory, 0);

        /* Upload data to staging buffer */
        void* mapData;
        vkMapMemory(device, stagingBufferMemory, 0, data.size(), 0, &mapData);
        memcpy(mapData, data.data(), data.size());
        vkUnmapMemory(device, stagingBufferMemory);

        /* Upload image data from the staging buffer to the image with layout transitions */
        perfomImageLayoutTransition(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        copyBufferToImage(stagingBuffer, handle);
        perfomImageLayoutTransition(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        /* Free the staging buffer and its memory */
        vkDestroyBuffer(device, stagingBuffer, nullptr);
        vkFreeMemory(device, stagingBufferMemory, nullptr);

        /* Initialize a image view create info */
        VkImageViewCreateInfo viewInfo = {};
        viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        viewInfo.image = handle;
        viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        viewInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
        viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        viewInfo.subresourceRange.baseMipLevel = 0;
        viewInfo.subresourceRange.levelCount = 1;
        viewInfo.subresourceRange.baseArrayLayer = 0;
        viewInfo.subresourceRange.layerCount = 1;

        /* Create the image view */
        if (vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS) {
            throw runtime_error("Could not create a texture image view!");
        }

        /* Create a sampler create info */
        VkSamplerCreateInfo samplerInfo = {};
        samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        samplerInfo.magFilter = VK_FILTER_LINEAR;
        samplerInfo.minFilter = VK_FILTER_LINEAR;
        samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        samplerInfo.mipLodBias = 0.0f;
        samplerInfo.anisotropyEnable = VK_FALSE;
        samplerInfo.maxAnisotropy = 1;
        samplerInfo.compareEnable = VK_FALSE;
        samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
        samplerInfo.minLod = 0.0f;
        samplerInfo.maxLod = 0.0f;
        samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
        samplerInfo.unnormalizedCoordinates = VK_FALSE;

        /* Create the sampler */
        if (vkCreateSampler(device, &samplerInfo, nullptr, &textureSampler) != VK_SUCCESS) {
            throw runtime_error("Could not create a texture sampler!");
        }
    }

    void TextureImage::perfomImageLayoutTransition(VkImageLayout oldLayout, VkImageLayout newLayout) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkCommandPool commandPool = VulkanContext::getCurrent()->getCommandPool()->getHandle();
        VkQueue queue = VulkanContext::getCurrent()->getDevice()->getQueues().graphics;

        /* Initialize a command buffer allocate info */
        VkCommandBufferAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocateInfo.commandPool = commandPool;
        allocateInfo.commandBufferCount = 1;

        /* Allocate a temporary command buffer */
        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(device, &allocateInfo, &commandBuffer);

        /* Initialize a command buffer begin info */
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        /* Begin recording */
        vkBeginCommandBuffer(commandBuffer, &beginInfo);

        /* Determine the transition barrier masks */
        VkAccessFlags sourceAccessMask;
        VkAccessFlags destinationAccessMask;
        VkPipelineStageFlags sourceStage;
        VkPipelineStageFlags destinationStage;
        if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
            sourceAccessMask = 0;
            destinationAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
            sourceAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            destinationAccessMask = VK_ACCESS_SHADER_READ_BIT;
            sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        } else {
            throw invalid_argument("Unsupported layout transition!");
        }

        /* Initialize a image memory barrier */
        VkImageMemoryBarrier barrier = {};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.srcAccessMask = sourceAccessMask;
        barrier.dstAccessMask = destinationAccessMask;
        barrier.oldLayout = oldLayout;
        barrier.newLayout = newLayout;
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.image = handle;
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = 1;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;

        /* Pipeline barrier */
        vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);

        /* End recording */
        vkEndCommandBuffer(commandBuffer);

        /* Initialize a submit info */
        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;

        /* Submit the command */
        vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(queue);

        /* Free the temporary command buffer */
        vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
    }

    void TextureImage::copyBufferToImage(VkBuffer stagingBuffer, VkImage image) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkCommandPool commandPool = VulkanContext::getCurrent()->getCommandPool()->getHandle();
        VkQueue queue = VulkanContext::getCurrent()->getDevice()->getQueues().graphics;

        /* Initialize a command buffer allocate info */
        VkCommandBufferAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocateInfo.commandPool = commandPool;
        allocateInfo.commandBufferCount = 1;

        /* Allocate a temporary command buffer */
        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(device, &allocateInfo, &commandBuffer);

        /* Initialize a command buffer begin info */
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        /* Begin recording */
        vkBeginCommandBuffer(commandBuffer, &beginInfo);

        /* Initialize a buffer image copy */
        VkBufferImageCopy region = {};
        region.bufferOffset = 0;
        region.bufferRowLength = 0;
        region.bufferImageHeight = 0;
        region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.imageSubresource.mipLevel = 0;
        region.imageSubresource.baseArrayLayer = 0;
        region.imageSubresource.layerCount = 1;
        region.imageOffset = {0, 0, 0};
        region.imageExtent = {width, height, 1};

        /* Copy buffer to image */
        vkCmdCopyBufferToImage(commandBuffer, stagingBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

        /* End recording */
        vkEndCommandBuffer(commandBuffer);

        /* Initialize a submit info */
        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;

        /* Submit the command */
        vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(queue);

        /* Free the temporary command buffer */
        vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
    }
}
