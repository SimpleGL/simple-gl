/**
 * @file manager_vk.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the manager_vk.hpp header.
 */

#include "SimpleGL_vk/manager_vk.hpp"

#include <fstream>
#include <map>
#include <vector>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <SimpleGL/logging.hpp>

namespace sgl {

    void ShaderModuleManager::remove(std::string name) {
        /* Dispose shader module */
        ShaderModule* shaderModule = get(name);
        delete shaderModule;

        /* Call base method */
        Manager::remove(name);
    }

    void ShaderModuleManager::clear() {
        /* Dispose all shaders */
        for (map<string, ShaderModule*>::iterator it = objects.begin(); it != objects.end(); it++) {
            ShaderModule* shaderModule = it->second;
            delete shaderModule;
        }

        /* Call base method */
        Manager::clear();
    }

    ShaderModule* ShaderModuleManager::loadShaderModule(std::string path, std::string name, ShaderStageBit stageBit) {
        /* Opening file stream */
        ifstream fileStream(path, ios::ate | ios::binary);
        if (fileStream.is_open()) {
            /* Determine file size */
            size_t fileSize = fileStream.tellg();
            vector<uint8_t> fileBuffer(fileSize);

            /* Read binary file */
            fileStream.seekg(0);
            fileStream.read(reinterpret_cast<char*> (fileBuffer.data()), fileSize);

            /* Close file */
            fileStream.close();

            /* Create shader module */
            ShaderModule* shaderModule = new ShaderModule(fileBuffer, stageBit);

            /* Store shader module and return it */
            put(name, shaderModule);
            return shaderModule;
        } else {
            Logger::logError("Could not open file " + path);
            return new ShaderModule(vector<uint8_t>(), stageBit);
        }
    }

    void TextureImageManager::remove(std::string name) {
        /* Dispose texture image */
        TextureImage* textureImage = get(name);
        delete textureImage;

        /* Call base method */
        Manager::remove(name);
    }

    void TextureImageManager::clear() {
        /* Dispose all texture images */
        for (map<string, TextureImage*>::iterator it = objects.begin(); it != objects.end(); it++) {
            TextureImage* textureImage = it->second;
            delete textureImage;
        }

        /* Call base method */
        Manager::clear();
    }

    TextureImage* TextureImageManager::load(std::string path, std::string name) {
        /* Load bitmap file and create image */
        stbi_set_flip_vertically_on_load(true);
        int32_t width, height, components;
        stbi_uc* pixels = stbi_load(path.c_str(), &width, &height, &components, 0);

        /* Generate Texture image */
        TextureImage* textureImage = new TextureImage(width, height);

        /* Upload texture data */
        textureImage->upload(vector<uint8_t>(pixels, pixels + components * width * height));
        stbi_image_free(pixels);

        /* Put texture in storage and return it */
        put(name, textureImage);
        return textureImage;
    }
}
