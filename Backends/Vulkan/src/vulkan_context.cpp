/**
 * @file vulkan_context.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the vulkan_context.hpp header.
 */

#include "SimpleGL_vk/vulkan_context.hpp"

/** Tells if the library is compiled in debug mode. Debug mode is disabled if NDEBUG ("not debug") is defined. */
#ifdef NDEBUG
const bool debugMode = false;
#else
const bool debugMode = true;
#endif

namespace sgl {
    VulkanContext* VulkanContext::current = nullptr;

    VulkanContext* VulkanContext::getCurrent() {
        return current;
    }

    void VulkanContext::setCurrent(VulkanContext* context) {
        /* Only set if context is not a nullptr */
        if (context != nullptr) {
            current = context;
        }
    }

    VulkanContext::VulkanContext(void) {
        /* Create the Vulkan instance */
        instance = new Instance(debugMode);
    }

    VulkanContext::~VulkanContext(void) {
        /* Wait for completion */
        vkDeviceWaitIdle(device->getHandle());

        /* Delete the synchronization objects */
        delete renderFence;

        /* Delete the descriptor pool */
        delete descriptorPool;

        /* Delete the command pool */
        delete commandPool;

        /* Delete the framebuffers */
        for (uint32_t i = 0; i < getSwapchainCount(); i++) {
            delete swapchainFramebuffers[i];
        }

        /* Delete the swapchain */
        delete swapchain;

        /* Delete the logical device */
        delete device;

        /* Delete debug report callback */
        delete debugReportCallback;

        /* Delete Vulkan instance */
        delete instance;

        /* Set context to nullptr if this was the current context */
        if (VulkanContext::current == this) {
            VulkanContext::current = nullptr;
        }
    }

    Instance* VulkanContext::getInstance() {
        return instance;
    }

    DebugReportCallback* VulkanContext::getDebugReportCallback() {
        return debugReportCallback;
    }

    std::vector<PhysicalDevice*> VulkanContext::getAvailablePhysicalDevices() {
        return availablePhysicalDevices;
    }

    PhysicalDevice* VulkanContext::getPhysicalDevice() {
        return physicalDevice;
    }

    Device* VulkanContext::getDevice() {
        return device;
    }

    Swapchain* VulkanContext::getSwapchain() {
        return swapchain;
    }

    uint32_t VulkanContext::getSwapchainCount() {
        return swapchain->getSwapchainData().images.size();
    }

    uint32_t VulkanContext::getSwapchainIndex() {
        return swapchain->getSwapchainData().index;
    }

    FramebufferVK* VulkanContext::getSwapchainFramebuffer() {
        return swapchainFramebuffers[getSwapchainIndex()];
    }

    CommandPool* VulkanContext::getCommandPool() {
        return commandPool;
    }

    DescriptorPool* VulkanContext::getDescriptorPool() {
        return descriptorPool;
    }

    Fence* VulkanContext::getRenderFence() {
        return renderFence;
    }

    void VulkanContext::setupDebugCallback() {
        /* Setup a debug report callback */
        if (debugMode) {
            debugReportCallback = new DebugReportCallback();
            DebugReportCallback::setCurrent(debugReportCallback);
        } else {
            debugReportCallback = nullptr;
        }
    }

    void VulkanContext::pickPhysicalDevice(Surface* surface) {
        /* Pick a physical device */
        availablePhysicalDevices = PhysicalDevice::getAvailablePhysicalDevices(surface);
        physicalDevice = PhysicalDevice::getSuitablePhysicalDevice(availablePhysicalDevices);
    }

    void VulkanContext::createLogicalDevice() {
        /* Create a logical device */
        device = new Device(debugMode);
    }

    void VulkanContext::createSwapchain(Surface* surface, glm::ivec2 size) {
        /* Create a swapchain */
        swapchain = new Swapchain(surface, size);
    }

    void VulkanContext::createFramebuffers(RenderPass* renderPass) {
        /* Resize the framebuffer vector */
        swapchainFramebuffers.resize(getSwapchainCount());

        /* Create a framebuffer for each swapchain image */
        for (uint32_t i = 0; i < getSwapchainCount(); i++) {
            swapchainFramebuffers[i] = new FramebufferVK(renderPass, swapchain->getSwapchainData().imageViews[i]);
        }
    }

    void VulkanContext::createCommandPool() {
        /* Create a command pool */
        commandPool = new CommandPool();
    }

    void VulkanContext::createDescriptorPool() {
        /* Create a descriptor pool */
        descriptorPool = new DescriptorPool();
    }

    void VulkanContext::createRenderFence() {
        /* Create render fence */
        renderFence = new Fence();
    }
}
