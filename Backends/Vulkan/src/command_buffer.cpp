/**
 * @file command_buffer.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the command_buffer.hpp header.
 */

#include "SimpleGL_vk/command_buffer.hpp"

#include <stdexcept>
#include <vector>

#include "SimpleGL_vk/vulkan_context.hpp"
#include "SimpleGL_vk/physical_device.hpp"
#include "SimpleGL_vk/swapchain.hpp"

using namespace std;

namespace sgl {

    CommandPool::CommandPool(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        QueueFamilyIndices queueFamilyIndices = VulkanContext::getCurrent()->getPhysicalDevice()->getQueueFamilyIndices();

        /* Initialize a command pool create info */
        VkCommandPoolCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        createInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        createInfo.queueFamilyIndex = static_cast<uint32_t> (queueFamilyIndices.graphics);

        /* Create the command pool */
        if (vkCreateCommandPool(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a command pool!");
        }
    }

    CommandPool::~CommandPool(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the command pool */
        vkDestroyCommandPool(device, handle, nullptr);
    }

    VkCommandPool CommandPool::getHandle() {
        return handle;
    }

    CommandBuffer::CommandBuffer(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkCommandPool commandPool = VulkanContext::getCurrent()->getCommandPool()->getHandle();

        /* Initialize a command buffer allocate info */
        VkCommandBufferAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocateInfo.commandPool = commandPool;
        allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocateInfo.commandBufferCount = 1;

        /* Allocate the command buffer */
        if (vkAllocateCommandBuffers(device, &allocateInfo, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not allocate the command buffers!");
        }
    }

    CommandBuffer::~CommandBuffer(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkCommandPool commandPool = VulkanContext::getCurrent()->getCommandPool()->getHandle();

        /* Free command buffer */
        vkFreeCommandBuffers(device, commandPool, 1, &handle);
    }

    VkCommandBuffer CommandBuffer::getHandle() {
        return handle;
    }

    void CommandBuffer::begin() {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkFence fence = VulkanContext::getCurrent()->getRenderFence()->getHandle();

        /* Wait for the submit fence */
        vkWaitForFences(device, 1, &fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
        vkResetFences(device, 1, &fence);

        /* Initialize a command buffer begin info */
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        /* Begin recording */
        if (vkBeginCommandBuffer(handle, &beginInfo) != VK_SUCCESS) {
            throw runtime_error("Could not begin recording the command buffers!");
        }
    }

    void CommandBuffer::end() {
        if (vkEndCommandBuffer(handle) != VK_SUCCESS) {
            throw runtime_error("Could not record the command buffer!");
        }
    }

    void CommandBuffer::beginRenderPass(RenderPass* renderPass) {
        VkFramebuffer framebuffer = VulkanContext::getCurrent()->getSwapchainFramebuffer()->getHandle();
        SwapchainData swapchainData = VulkanContext::getCurrent()->getSwapchain()->getSwapchainData();

        /* Initialize the clear values */
        vector<VkClearValue> clearValues(2);
        clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
        clearValues[1].depthStencil = {1.0f, 0};

        /* Initialize a render pass begin info */
        VkRenderPassBeginInfo renderPassBegin = {};
        renderPassBegin.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassBegin.renderPass = renderPass->getHandle();
        renderPassBegin.framebuffer = framebuffer;
        renderPassBegin.renderArea.offset.x = 0;
        renderPassBegin.renderArea.offset.y = 0;
        renderPassBegin.renderArea.extent = swapchainData.extent;
        renderPassBegin.clearValueCount = clearValues.size();
        renderPassBegin.pClearValues = clearValues.data();

        /* Begin render pass */
        vkCmdBeginRenderPass(handle, &renderPassBegin, VK_SUBPASS_CONTENTS_INLINE);
    }

    void CommandBuffer::endRenderPass() {
        vkCmdEndRenderPass(handle);
    }

    void CommandBuffer::bindPipeline(GraphicsPipeline* pipeline) {
        vkCmdBindPipeline(handle, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->getHandle());
    }

    void CommandBuffer::bindVertexBuffer(VertexBuffer* vertexBuffer, uint32_t binding) {
        VkDeviceSize offset = 0;
        VkBuffer vertexBufferHandle = vertexBuffer->getHandle();
        vkCmdBindVertexBuffers(handle, binding, 1, &vertexBufferHandle, &offset);
    }

    void CommandBuffer::bindDescriptorSet(DescriptorSet* descriptorSet, GraphicsPipeline* graphicsPipeline, uint32_t binding) {
        VkPipelineLayout layout = graphicsPipeline->getPipelineLayout();
        VkDescriptorSet descriptorSetHandle = descriptorSet->getHandle();
        vkCmdBindDescriptorSets(handle, VK_PIPELINE_BIND_POINT_GRAPHICS, layout, 0, 1, &descriptorSetHandle, 0, nullptr);
    }

    void CommandBuffer::draw(uint32_t vertexCount) {
        vkCmdDraw(handle, vertexCount, 1, 0, 0);
    }

    void CommandBuffer::submit() {
        VkQueue queue = VulkanContext::getCurrent()->getDevice()->getQueues().graphics;
        VkFence fence = VulkanContext::getCurrent()->getRenderFence()->getHandle();

        /* Initialize a submit info */
        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &handle;

        /* Submit the command buffer */
        if (vkQueueSubmit(queue, 1, &submitInfo, fence) != VK_SUCCESS) {
            throw runtime_error("Could not submit the draw command buffer!");
        }
    }
}
