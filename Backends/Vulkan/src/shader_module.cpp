/**
 * @file shader_module.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the shader_module.hpp header.
 */

#include "SimpleGL_vk/shader_module.hpp"

#include <stdexcept>

#include "SimpleGL_vk/vulkan_context.hpp"

using namespace std;

namespace sgl {

    ShaderModule::ShaderModule(std::vector<uint8_t> code, ShaderStageBit stage) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        this->shaderStageBit = stage;

        /* Initialize a shader module create info */
        VkShaderModuleCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = code.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*> (code.data());

        /* Create the shader module */
        if (vkCreateShaderModule(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a shader module!");
        }
    }

    ShaderModule::~ShaderModule(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the shader module */
        vkDestroyShaderModule(device, handle, nullptr);
    }

    VkShaderModule ShaderModule::getHandle() {
        return handle;
    }

    VkShaderStageFlagBits ShaderModule::getShaderStageBit() {
        /* Determine the shader stage flag bit */
        VkShaderStageFlagBits stageFlagBit;
        switch (shaderStageBit) {
            case ShaderStageBit::VERTEX_BIT:
                stageFlagBit = VK_SHADER_STAGE_VERTEX_BIT;
                break;
            case ShaderStageBit::FRAGMENT_BIT:
                stageFlagBit = VK_SHADER_STAGE_FRAGMENT_BIT;
                break;
        }

        return stageFlagBit;
    }
}
