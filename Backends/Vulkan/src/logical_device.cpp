/**
 * @file logical_device.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the vulkan_context.hpp header.
 */

#include "SimpleGL_vk/logical_device.hpp"

#include <stdexcept>
#include <vector>
#include <set>

#include "SimpleGL_vk/vulkan_context.hpp"
#include "SimpleGL_vk/physical_device.hpp"

using namespace std;

namespace sgl {

    Device::Device(bool debugMode) {
        PhysicalDevice* physicalDevice = VulkanContext::getCurrent()->getPhysicalDevice();

        /* Initialize the device queue create infos */
        QueueFamilyIndices queueFamilyIndices = physicalDevice->getQueueFamilyIndices();
        set<int32_t> queueFamilies = {queueFamilyIndices.graphics, queueFamilyIndices.presentation};
        float queuePriority = 1.0f;
        vector<VkDeviceQueueCreateInfo> queueCreateInfos;
        for (set<int32_t>::iterator it = queueFamilies.begin(); it != queueFamilies.end(); it++) {
            VkDeviceQueueCreateInfo queueCreateInfo = {};
            queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueCreateInfo.queueFamilyIndex = *it;
            queueCreateInfo.queueCount = 1;
            queueCreateInfo.pQueuePriorities = &queuePriority;
            queueCreateInfos.push_back(queueCreateInfo);
        }

        /* Add required layers for compatibility reasons */
        vector<const char*> requiredLayers;
        if (debugMode) {
            requiredLayers.push_back("VK_LAYER_LUNARG_standard_validation");
        }

        /* Add required extensions */
        vector<const char*> requiredExtensions;
        requiredExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

        /* Initialize physical device features */
        VkPhysicalDeviceFeatures physicalDeviceFeatures = {};

        /* Inizialize a device create info */
        VkDeviceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        createInfo.queueCreateInfoCount = queueCreateInfos.size();
        createInfo.pQueueCreateInfos = queueCreateInfos.data();
        createInfo.enabledLayerCount = requiredLayers.size();
        createInfo.ppEnabledLayerNames = requiredLayers.data();
        createInfo.enabledExtensionCount = requiredExtensions.size();
        createInfo.ppEnabledExtensionNames = requiredExtensions.data();
        createInfo.pEnabledFeatures = &physicalDeviceFeatures;

        /* Create the device */
        if (vkCreateDevice(physicalDevice->getHandle(), &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a logical device!");
        }

        /* Get device queues */
        vkGetDeviceQueue(handle, queueFamilyIndices.graphics, 0, &queues.graphics);
        vkGetDeviceQueue(handle, queueFamilyIndices.presentation, 0, &queues.presentation);
    }

    Device::~Device(void) {
        vkDestroyDevice(handle, nullptr);
    }

    VkDevice Device::getHandle() {
        return handle;
    }

    Queues Device::getQueues() {
        return queues;
    }
}
