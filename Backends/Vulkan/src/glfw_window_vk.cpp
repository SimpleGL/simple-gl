/**
 * @file glfw_window_vk.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the glfw_window_vk.hpp header.
 */

#include "SimpleGL_vk/glfw_window_vk.hpp"
#include "SimpleGL_vk/surface.hpp"

#include <stdexcept>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <SimpleGL/glfw_callback.hpp>

using namespace std;

namespace sgl {

    WindowVK::WindowVK(int width, int height, std::string title) : Window() {
        size.x = width;
        size.y = height;
        this->title = title;

        /* Reset to default window hints */
        glfwDefaultWindowHints();

        /* Not resizeable */
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        /* Disable OpenGL context creation */
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

        /* Creating the GLFW window */
        handle = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
        if (!handle) {
            throw runtime_error("Could not create window!");
        }

        /* Create Vulkan context and set it current */
        context = new VulkanContext();
        VulkanContext::setCurrent(context);

        /* Setup a debug callback */
        context->setupDebugCallback();

        /* Create a Vulkan surface */
        surface = new Surface(this);

        /* Initialize the Vulkan device */
        context->pickPhysicalDevice(surface);
        context->createLogicalDevice();

        /* Create the swapchain */
        context->createSwapchain(surface, size);

        /* Center window to screen */
        centerWindow();

        /* Set default callbacks */
        KeyCallback* keyCallback = new KeyCallback();
        setKeyCallback(keyCallback);
        MouseButtonCallback* mouseButtonCallback = new MouseButtonCallback();
        setMouseButtonCallback(mouseButtonCallback);
    }

    WindowVK::~WindowVK(void) {
        /* Delete the window surface */
        delete surface;

        /* Delete the Vulkan context */
        delete context;
    }

    void WindowVK::swapBuffers() {
        context->getSwapchain()->present();
        context->getSwapchain()->acquireNextImage();
    }
}
