/**
 * @file application_vk.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the application_vk.hpp header.
 */

#include "SimpleGL_vk/application_vk.hpp"

#include "SimpleGL_vk/glfw_window_vk.hpp"

namespace sgl {

    ApplicationVK::ApplicationVK(int width, int height, std::string title) : Application() {
        window = new WindowVK(width, height, title);
        batch = new BatchVK();
        shaderModuleManager = new ShaderModuleManager();
        textureImageManager = new TextureImageManager();
    }

    ApplicationVK::~ApplicationVK(void) {
        /* Delete manager */
        delete shaderModuleManager;

        /* Delete batch */
        delete batch;
    }

    void ApplicationVK::clear() {
        /* Nothing to do here */
    }

    void ApplicationVK::dispose() {
        shaderModuleManager->clear();
        textureImageManager->clear();
    }
}
