/**
 * @file synchronization.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the synchronization.hpp header.
 */

#include "SimpleGL_vk/synchronization.hpp"

#include <stdexcept>

#include "SimpleGL_vk/vulkan_context.hpp"

using namespace std;

namespace sgl {

    Semaphore::Semaphore(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Initialize a semaphore create info */
        VkSemaphoreCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        /* Create the semaphore */
        if (vkCreateSemaphore(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a semaphore!");
        }
    }

    Semaphore::~Semaphore(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the semaphore */
        vkDestroySemaphore(device, handle, nullptr);
    }

    VkSemaphore Semaphore::getHandle() {
        return handle;
    }

    Fence::Fence(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Initialize a fence create info */
        VkFenceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        createInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        /* Create the fence */
        if (vkCreateFence(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a fence!");
        }
    }

    Fence::~Fence(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the fence */
        vkDestroyFence(device, handle, nullptr);
    }

    VkFence Fence::getHandle() {
        return handle;
    }
}
