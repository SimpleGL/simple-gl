/**
 * @file framebuffer.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the framebuffer.hpp header.
 */

#include "SimpleGL_vk/framebuffer.hpp"

#include <stdexcept>
#include <vector>

#include "SimpleGL_vk/vulkan_context.hpp"
#include "SimpleGL_vk/swapchain.hpp"

using namespace std;

namespace sgl {

    FramebufferVK::FramebufferVK(RenderPass* renderPass, VkImageView imageView) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        SwapchainData swapchainData = VulkanContext::getCurrent()->getSwapchain()->getSwapchainData();

        /* Create depth stencil buffer */
        createDepthStencilBuffer();
        vector<VkImageView> attachments = {imageView, depthStencil.imageView};

        /* Initialize a framebuffer create info */
        VkFramebufferCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        createInfo.renderPass = renderPass->getHandle();
        createInfo.attachmentCount = attachments.size();
        createInfo.pAttachments = attachments.data();
        createInfo.width = swapchainData.extent.width;
        createInfo.height = swapchainData.extent.height;
        createInfo.layers = 1;

        /* Create the framebuffer */
        if (vkCreateFramebuffer(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a framebuffer!");
        }
    }

    FramebufferVK::~FramebufferVK(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the framebuffer */
        vkDestroyFramebuffer(device, handle, nullptr);
    }

    VkFramebuffer FramebufferVK::getHandle() {
        return handle;
    }

    DepthStencilBuffer FramebufferVK::getDepthStencilBuffer() {
        return depthStencil;
    }

    void FramebufferVK::createDepthStencilBuffer() {
        VkPhysicalDevice physicalDevice = VulkanContext::getCurrent()->getPhysicalDevice()->getHandle();
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkCommandPool commandPool = VulkanContext::getCurrent()->getCommandPool()->getHandle();
        VkQueue queue = VulkanContext::getCurrent()->getDevice()->getQueues().graphics;
        SwapchainData swapchainData = VulkanContext::getCurrent()->getSwapchain()->getSwapchainData();

        /* Initialize format candidates */
        depthStencil.format = VK_FORMAT_UNDEFINED;
        VkImageTiling tiling = VK_IMAGE_TILING_MAX_ENUM;
        vector<VkFormat> candidates = {VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D16_UNORM_S8_UINT};

        /* Find a supported format */
        for (uint32_t i = 0; i < candidates.size(); i++) {
            depthStencil.format = candidates[i];

            /* Get physical device format properties */
            VkFormatProperties formatProperties;
            vkGetPhysicalDeviceFormatProperties(physicalDevice, depthStencil.format, &formatProperties);

            /* Determine image tiling arrangement */
            if (formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
                tiling = VK_IMAGE_TILING_OPTIMAL;
                break;
            } else if (formatProperties.linearTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) {
                tiling = VK_IMAGE_TILING_LINEAR;
                break;
            }
        }
        if (depthStencil.format == VK_FORMAT_UNDEFINED || tiling == VK_IMAGE_TILING_MAX_ENUM) {
            throw runtime_error("Could not find a supported depth stencil format!");
        }

        /* Initialize a image create info */
        VkImageCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        createInfo.imageType = VK_IMAGE_TYPE_2D;
        createInfo.format = depthStencil.format;
        createInfo.extent = {swapchainData.extent.width, swapchainData.extent.height, 1};
        createInfo.mipLevels = 1;
        createInfo.arrayLayers = 1;
        createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        createInfo.tiling = tiling;
        createInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        /* Create the image */
        if (vkCreateImage(device, &createInfo, nullptr, &depthStencil.image) != VK_SUCCESS) {
            throw runtime_error("Could not create a depth stencil image!");
        }

        /* Get memory requirements */
        VkMemoryRequirements memoryRequirements;
        vkGetImageMemoryRequirements(device, depthStencil.image, &memoryRequirements);

        /* Get the device memory properties */
        VkPhysicalDeviceMemoryProperties memoryProperties;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

        /* Determine memory type index */
        uint32_t memoryTypeIndex = -1;
        VkMemoryPropertyFlags flags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++) {
            if ((memoryRequirements.memoryTypeBits & (1 << i)) &&
                (memoryProperties.memoryTypes[i].propertyFlags & flags) == flags) {
                memoryTypeIndex = i;
                break;
            }
        }

        /* Initialize a memory allocate info */
        VkMemoryAllocateInfo allocateInfo = {};
        allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocateInfo.allocationSize = memoryRequirements.size;
        allocateInfo.memoryTypeIndex = memoryTypeIndex;

        /* Allocate the texture image memory */
        if (vkAllocateMemory(device, &allocateInfo, nullptr, &depthStencil.memory) != VK_SUCCESS) {
            throw runtime_error("Could not allocate depth stencil image memory!");
        }

        /* Bind the texture image memory */
        vkBindImageMemory(device, depthStencil.image, depthStencil.memory, 0);

        /* Initialize a image view create info */
        VkImageViewCreateInfo viewInfo = {};
        viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        viewInfo.image = depthStencil.image;
        viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        viewInfo.format = depthStencil.format;
        viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
        viewInfo.subresourceRange.baseMipLevel = 0;
        viewInfo.subresourceRange.levelCount = 1;
        viewInfo.subresourceRange.baseArrayLayer = 0;
        viewInfo.subresourceRange.layerCount = 1;

        /* Create the image view */
        if (vkCreateImageView(device, &viewInfo, nullptr, &depthStencil.imageView) != VK_SUCCESS) {
            throw runtime_error("Could not create a depth stencil image view!");
        }

        /* Initialize a command buffer allocate info */
        VkCommandBufferAllocateInfo cmdAllocateInfo = {};
        cmdAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        cmdAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        cmdAllocateInfo.commandPool = commandPool;
        cmdAllocateInfo.commandBufferCount = 1;

        /* Allocate a temporary command buffer */
        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(device, &cmdAllocateInfo, &commandBuffer);

        /* Initialize a command buffer begin info */
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        /* Begin recording */
        vkBeginCommandBuffer(commandBuffer, &beginInfo);

        /* Initialize a image memory barrier */
        VkImageMemoryBarrier barrier = {};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        barrier.newLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.image = depthStencil.image;
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = 1;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;

        /* Pipeline barrier */
        vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);

        /* End recording */
        vkEndCommandBuffer(commandBuffer);

        /* Initialize a submit info */
        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;

        /* Submit the command */
        vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(queue);

        /* Free the temporary command buffer */
        vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
    }
}
