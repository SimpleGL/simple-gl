/**
 * @file swapchain.cpp
 * @author Heiko Brumme
 * 
 * his files contains the implementation of the swapchain.hpp header.
 */

#include "SimpleGL_vk/swapchain.hpp"

#include <stdexcept>
#include <algorithm>

#include "SimpleGL_vk/vulkan_context.hpp"

using namespace std;

namespace sgl {

    Swapchain::Swapchain(Surface* surface, glm::ivec2 size) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        SwapchainSupportDetails swapchainSupportDetail = VulkanContext::getCurrent()->getPhysicalDevice()->getSwapchainSupportDetails();

        /* Check if the surface has no preferred format */
        VkSurfaceFormatKHR surfaceFormat = swapchainSupportDetail.surfaceFormats[0];
        if (swapchainSupportDetail.surfaceFormats.size() == 1 &&
            swapchainSupportDetail.surfaceFormats[0].format == VK_FORMAT_UNDEFINED) {
            surfaceFormat.format = VK_FORMAT_B8G8R8A8_UNORM;
            surfaceFormat.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
        } else {
            /* Choose a surface format */
            for (uint32_t i = 0; i < swapchainSupportDetail.surfaceFormats.size(); i++) {
                VkSurfaceFormatKHR surfaceFormatCandidate = swapchainSupportDetail.surfaceFormats[i];
                if (surfaceFormatCandidate.format == VK_FORMAT_B8G8R8A8_UNORM &&
                    surfaceFormatCandidate.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                    surfaceFormat = surfaceFormatCandidate;
                    break;
                }
            }
        }

        /* Choose a present mode */
        VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;
        for (uint32_t i = 0; i < swapchainSupportDetail.presentModes.size(); i++) {
            VkPresentModeKHR presentModeCandidate = swapchainSupportDetail.presentModes[i];
            if (presentModeCandidate == VK_PRESENT_MODE_MAILBOX_KHR) {
                presentMode = presentModeCandidate;
                break;
            } else if (presentModeCandidate == VK_PRESENT_MODE_IMMEDIATE_KHR) {
                presentMode = presentModeCandidate;
            }
        }

        /* Check if the extent is defined */
        VkExtent2D extent = {static_cast<uint32_t> (size.x), static_cast<uint32_t> (size.y)};
        if (swapchainSupportDetail.surfaceCapabilities.currentExtent.width != numeric_limits<uint32_t>::max()) {
            extent = swapchainSupportDetail.surfaceCapabilities.currentExtent;
        } else {
            /* Choose a swapchain extent */
            extent.width = max(swapchainSupportDetail.surfaceCapabilities.minImageExtent.width,
                               min(swapchainSupportDetail.surfaceCapabilities.maxImageExtent.width, extent.width));
            extent.height = max(swapchainSupportDetail.surfaceCapabilities.minImageExtent.height,
                                min(swapchainSupportDetail.surfaceCapabilities.maxImageExtent.height, extent.height));
        }

        /* Check for image limits */
        uint32_t imageCount = swapchainSupportDetail.surfaceCapabilities.minImageCount;
        if (swapchainSupportDetail.surfaceCapabilities.maxImageCount > 0 &&
            imageCount > swapchainSupportDetail.surfaceCapabilities.maxImageCount) {
            imageCount = swapchainSupportDetail.surfaceCapabilities.maxImageCount;
        }

        /* Initialize a swapchain create info */
        QueueFamilyIndices queueFamilyIndices = VulkanContext::getCurrent()->getPhysicalDevice()->getQueueFamilyIndices();
        vector<uint32_t> queueFamilyIndicesVector = {
            static_cast<uint32_t> (queueFamilyIndices.graphics),
            static_cast<uint32_t> (queueFamilyIndices.presentation)
        };
        VkSwapchainCreateInfoKHR createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = surface->getHandle();
        createInfo.minImageCount = imageCount;
        createInfo.imageFormat = surfaceFormat.format;
        createInfo.imageColorSpace = surfaceFormat.colorSpace;
        createInfo.imageExtent = extent;
        createInfo.imageArrayLayers = 1;
        createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        if (queueFamilyIndices.graphics != queueFamilyIndices.presentation) {
            createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            createInfo.queueFamilyIndexCount = queueFamilyIndicesVector.size();
            createInfo.pQueueFamilyIndices = queueFamilyIndicesVector.data();
        } else {
            createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        }
        createInfo.preTransform = swapchainSupportDetail.surfaceCapabilities.currentTransform;
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        createInfo.presentMode = presentMode;
        createInfo.clipped = VK_TRUE;
        createInfo.oldSwapchain = VK_NULL_HANDLE;

        /* Create the swapchain */
        if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &handle) != VK_SUCCESS) {
            throw runtime_error("Could not create a swapchain!");
        }

        /* Get the number of available swapchain images */
        vkGetSwapchainImagesKHR(device, handle, &imageCount, nullptr);

        /* Get all swapchain images */
        swapchainData.images.resize(imageCount);
        vkGetSwapchainImagesKHR(device, handle, &imageCount, swapchainData.images.data());

        /* Create the image views */
        swapchainData.imageViews.resize(swapchainData.images.size());
        for (uint32_t i = 0; i < swapchainData.images.size(); i++) {
            /* Initialize a image view create info */
            VkImageViewCreateInfo viewCreateInfo = {};
            viewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            viewCreateInfo.image = swapchainData.images[i];
            viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
            viewCreateInfo.format = surfaceFormat.format;
            viewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            viewCreateInfo.subresourceRange.baseMipLevel = 0;
            viewCreateInfo.subresourceRange.levelCount = 1;
            viewCreateInfo.subresourceRange.baseArrayLayer = 0;
            viewCreateInfo.subresourceRange.layerCount = 1;

            /* Create the image view */
            if (vkCreateImageView(device, &viewCreateInfo, nullptr, &swapchainData.imageViews[i]) != VK_SUCCESS) {
                throw runtime_error("Could not create image views for the swapchain images!");
            }
        }

        /* Initialize the swapchain data */
        swapchainData.imageFormat = surfaceFormat.format;
        swapchainData.extent = extent;
        swapchainData.index = -1;
    }

    Swapchain::~Swapchain(void) {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();

        /* Destroy the image views */
        for (uint32_t i = 0; i < swapchainData.imageViews.size(); i++) {
            vkDestroyImageView(device, swapchainData.imageViews[i], nullptr);
        }

        /* Destroy the swapchain */
        vkDestroySwapchainKHR(device, handle, nullptr);
    }

    VkSwapchainKHR Swapchain::getHandle() {
        return handle;
    }

    SwapchainData Swapchain::getSwapchainData() {
        return swapchainData;
    }

    void Swapchain::acquireNextImage() {
        VkDevice device = VulkanContext::getCurrent()->getDevice()->getHandle();
        VkFence fence = VulkanContext::getCurrent()->getRenderFence()->getHandle();

        /* Wait for the submit fence */
        vkWaitForFences(device, 1, &fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
        vkResetFences(device, 1, &fence);

        /* Acquire the next image */
        VkResult result = vkAcquireNextImageKHR(device, handle, numeric_limits<uint64_t>::max(), VK_NULL_HANDLE, fence, &swapchainData.index);

        /* Error handling */
        if (result == VK_ERROR_OUT_OF_DATE_KHR) {
            // ToDo: Swapchain recreation
        } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
            throw runtime_error("Could not acquire next swapchain image!");
        }
    }

    void Swapchain::present() {
        VkQueue queue = VulkanContext::getCurrent()->getDevice()->getQueues().presentation;

        /* Initialize a present info */
        VkPresentInfoKHR presentInfo = {};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = &handle;
        presentInfo.pImageIndices = &swapchainData.index;

        /* Present the swapchain image */
        VkResult result = vkQueuePresentKHR(queue, &presentInfo);

        /* Error handling */
        if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR) {
            // ToDo: Swapchain recreation
        } else if (result != VK_SUCCESS) {
            throw runtime_error("Could not present swapchain image!");
        }
    }
}
