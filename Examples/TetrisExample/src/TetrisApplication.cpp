/*
 * @file TetrisApplication.cpp
 * @author Heiko Brumme
 * 
 * This files contains the implementation of the TetrisApplication.hpp header.
 */

#include "TetrisApplication.hpp"

#include <random>

using namespace sgl;
using namespace std;
using namespace glm;

TetrisApplication::TetrisApplication(void) : ApplicationVK(508, 650, "SimpleGL Tetris") {
    /* Initialize Tetrominos */
    tetrominos.resize(7, Tetromino());

    /* I-Block */
    tetrominos[0].shape = {
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {1, 1, 1, 1},
        {0, 0, 0, 0}
    };
    tetrominos[0].position = vec2(1.0f, 3.0f);

    /* O-Block */
    tetrominos[1].shape = {
        {2, 2},
        {2, 2}
    };
    tetrominos[1].position = vec2(0.0f, 4.0f);

    /* T-Block */
    tetrominos[2].shape = {
        {0, 0, 0},
        {0, 3, 0},
        {3, 3, 3}
    };
    tetrominos[2].position = vec2(0.0f, 3.0f);

    /* S-Block */
    tetrominos[3].shape = {
        {0, 0, 0},
        {0, 4, 4},
        {4, 4, 0}
    };
    tetrominos[3].position = vec2(0.0f, 3.0f);

    /* Z-Block */
    tetrominos[4].shape = {
        {0, 0, 0},
        {5, 5, 0},
        {0, 5, 5}
    };
    tetrominos[4].position = vec2(0.0f, 3.0f);

    /* J-Block */
    tetrominos[5].shape = {
        {0, 0, 0},
        {6, 0, 0},
        {6, 6, 6}
    };
    tetrominos[5].position = vec2(0.0f, 3.0f);

    /* L-Block */
    tetrominos[6].shape = {
        {0, 0, 0},
        {0, 0, 7},
        {7, 7, 7}
    };
    tetrominos[6].position = vec2(0.0f, 3.0f);

    /* Initialize Tetromino colors */
    colors.resize(7);
    colors[0] = vec3(0.0f, 1.0f, 1.0f); // Cyan
    colors[1] = vec3(1.0f, 1.0f, 0.0f); // Yellow
    colors[2] = vec3(0.5f, 0.0f, 0.5f); // Purple
    colors[3] = vec3(0.0f, 1.0f, 0.0f); // Green
    colors[4] = vec3(1.0f, 0.0f, 0.0f); // Red
    colors[5] = vec3(0.0f, 0.0f, 1.0f); // Blue
    colors[6] = vec3(1.0f, 0.65f, 0.0f); // Orange

    /* A typical Tetris field has 20 rows and 10 columns */
    int rows = 20;
    int columns = 10;

    /* Initialize game field with zeros */
    gameField.resize(rows, vector<int>(columns, 0));

    /* Initialize next Tetrominos*/
    vector<Tetromino> potentialTetrominos;
    for (int i = 0; i < tetrominos.size(); i++) {
        potentialTetrominos.push_back(tetrominos[i]);
    }

    /* Generate current and four next Tetrominos */
    int count = 5;
    random_device rd;
    for (int i = 0; i < count; i++) {
        uniform_int_distribution<int> dist = uniform_int_distribution<int>(0, potentialTetrominos.size() - 1);
        int r = dist(rd);
        nextTetrominos.push_back(potentialTetrominos[r]);
        potentialTetrominos.erase(potentialTetrominos.begin() + r);
    }

    /* Initialize current Tetromino and speed */
    currentTetromino = nextTetrominos[0];
    nextTetrominos.erase(nextTetrominos.begin());
    speed = 3.0f;
    gameOver = false;

    /* Load textures */
    guiTexture = textureImageManager->load("./resource/tetris_gui.png", "tetrisGui");
    blockTexture = textureImageManager->load("./resource/tetromino_block.png", "tetrominoBlock");
}

TetrisApplication::~TetrisApplication(void) {
    /* Nothing to do here */
}

void TetrisApplication::handleEvents(std::vector<sgl::Event*> events) {
    /* Loop over all events */
    for (unsigned i = 0; i < events.size(); i++) {
        if (events[i]->getType() == EventType::KEY_EVENT) {
            KeyEvent* event = dynamic_cast<KeyEvent*> (events[i]);

            /* Pushing escape will result in closing the window */
            if (event->getKey() == Key::KEY_ESCAPE) {
                running = false;
            }

            /* Pushing enter will reset the game if it was over */
            if (event->getKey() == Key::KEY_ENTER && gameOver) {
                gameOver = false;
                gameField = vector<vector<int>>(gameField.size(), vector<int>(gameField[0].size(), 0));
            }

            /* Pushing the left arrow will move the Tetromino one block left */
            if (event->getKey() == Key::KEY_LEFT && event->getAction() != Action::RELEASE) {
                /* Calcualte potential Tetromino position */
                vec2 potentialPos = currentTetromino.position;
                potentialPos.y -= 1.0f;

                /* Check if the movement is valid */
                bool validMove = true;
                for (int row = 0; row < currentTetromino.shape.size(); row++) {
                    for (int column = 0; column < currentTetromino.shape[row].size(); column++) {
                        if (currentTetromino.shape[row][column] > 0) {
                            int i = ceil(potentialPos.x - (currentTetromino.shape.size() - 1 - row));
                            int j = ceil(potentialPos.y + column);
                            if (j < 0 || j > gameField[row].size() - 1 || gameField[i][j] > 0) {
                                validMove = false;
                                break;
                            }
                        }
                    }
                }

                if (validMove) {
                    /* Move the current Tetromino */
                    currentTetromino.position.y -= 1.0f;
                }
            }

            /* Pushing the right arrow will move the Tetromino one block right */
            if (event->getKey() == Key::KEY_RIGHT && event->getAction() != Action::RELEASE) {
                /* Calcualte potential Tetromino position */
                vec2 potentialPos = currentTetromino.position;
                potentialPos.y += 1.0f;

                /* Check if the movement is valid */
                bool validMove = true;
                for (int row = 0; row < currentTetromino.shape.size(); row++) {
                    for (int column = 0; column < currentTetromino.shape[row].size(); column++) {
                        if (currentTetromino.shape[row][column] > 0) {
                            int i = ceil(potentialPos.x - (currentTetromino.shape.size() - 1 - row));
                            int j = ceil(potentialPos.y + column);
                            if (j < 0 || j > gameField[row].size() - 1 || gameField[i][j] > 0) {
                                validMove = false;
                                break;
                            }
                        }
                    }
                }

                if (validMove) {
                    /* Move the current Tetromino */
                    currentTetromino.position.y += 1.0f;
                }
            }

            /* Pushing the up arrow will rotate the Tetromino clockwise */
            if (event->getKey() == Key::KEY_UP && event->getAction() == Action::RELEASE) {
                /* Calculate potential Tetromino shape */
                vector<vector<int>> potentialShape(currentTetromino.shape.size(), vector<int>(currentTetromino.shape[0].size(), 0));
                for (int row = 0; row < currentTetromino.shape.size(); row++) {
                    for (int column = 0; column < currentTetromino.shape[row].size(); column++) {
                        potentialShape[column][currentTetromino.shape.size() - 1 - row] = currentTetromino.shape[row][column];
                    }
                }

                /* Check if the movement is valid */
                bool validMove = true;
                for (int row = 0; row < potentialShape.size(); row++) {
                    for (int column = 0; column < potentialShape[row].size(); column++) {
                        if (potentialShape[row][column] > 0) {
                            int i = ceil(currentTetromino.position.x - (potentialShape.size() - 1 - row));
                            int j = ceil(currentTetromino.position.y + column);
                            if (j < 0 || j > gameField[row].size() - 1 || i > gameField.size() - 1 || gameField[i][j] > 0) {
                                validMove = false;
                                break;
                            }
                        }
                    }
                }

                if (validMove) {
                    /* Move the current Tetromino */
                    currentTetromino.shape = potentialShape;
                }
            }

            /* Pushing the down key will speed up the falling */
            if (event->getKey() == Key::KEY_DOWN) {
                if (event->getAction() == Action::PRESS) {
                    speed *= 10.0f;
                } else if (event->getAction() == Action::RELEASE) {
                    speed /= 10.0f;
                }
            }
        }
    }
}

void TetrisApplication::updateScene(float delta) {
    if (!gameOver) {
        /* Calcualte potential Tetromino position */
        vec2 potentialPos = currentTetromino.position;
        potentialPos.x += speed * delta;

        /* Check if the movement is valid */
        bool validMove = true;
        for (int row = 0; row < currentTetromino.shape.size(); row++) {
            for (int column = 0; column < currentTetromino.shape[row].size(); column++) {
                if (currentTetromino.shape[row][column] > 0) {
                    int i = ceil(potentialPos.x - (currentTetromino.shape.size() - 1 - row));
                    int j = ceil(potentialPos.y + column);
                    if (i > gameField.size() - 1 || gameField[i][j] > 0) {
                        validMove = false;
                        break;
                    }
                }
            }
        }

        if (validMove) {
            /* Move the current Tetromino */
            currentTetromino.position.x += speed * delta;
        } else {
            /* Let the current Tetromino land */
            for (int row = 0; row < currentTetromino.shape.size(); row++) {
                for (int column = 0; column < currentTetromino.shape[row].size(); column++) {
                    if (currentTetromino.shape[row][column] > 0) {
                        vec2 currentPos = currentTetromino.position;
                        int i = ceil(currentPos.x - (currentTetromino.shape.size() - 1 - row));
                        int j = ceil(currentPos.y + column);
                        if (i >= 0 && j >= 0) {
                            gameField[i][j] = currentTetromino.shape[row][column];
                        } else {
                            gameOver = true;
                        }
                    }
                }
            }

            /* Get next Tetromino */
            currentTetromino = nextTetrominos[0];
            nextTetrominos.erase(nextTetrominos.begin());

            /* Generate new next Tetrominos if empty */
            if (nextTetrominos.size() == 0) {
                vector<Tetromino> potentialTetrominos;
                for (int i = 0; i < tetrominos.size(); i++) {
                    potentialTetrominos.push_back(tetrominos[i]);
                }

                /* Generate four next Tetrominos */
                int count = 4;
                random_device rd;
                for (int i = 0; i < count; i++) {
                    uniform_int_distribution<int> dist(0, potentialTetrominos.size() - 1);
                    int r = dist(rd);
                    nextTetrominos.push_back(potentialTetrominos[r]);
                    potentialTetrominos.erase(potentialTetrominos.begin() + r);
                }
            }
        }

        /* Check for filled rows */
        vector<int> clearedRows;
        for (int row = 0; row < gameField.size(); row++) {
            bool isFilled = true;
            for (int column = 0; column < gameField[row].size(); column++) {
                if (gameField[row][column] == 0) {
                    isFilled = false;
                    break;
                }
            }
            if (isFilled) {
                clearedRows.push_back(row);
            }
        }

        /* Remove cleared lines */
        for (int i = 0; i < clearedRows.size(); i++) {
            gameField.erase(gameField.begin() + clearedRows[i]);
            gameField.insert(gameField.begin(), vector<int>(gameField[0].size(), 0));
        }
    }
}

void TetrisApplication::renderScene(float alpha) {
    TextureImage::setCurrent(guiTexture);
    batch->begin();
    renderGUI();
    batch->end();

    TextureImage::setCurrent(blockTexture);
    batch->begin();
    renderGameField();
    renderTetromino();
    renderNextTetromino();
    batch->end();
}

void TetrisApplication::renderGUI() {
    /* Texture coordinates */
    vec2 t1(0.0f, 0.0f);
    vec2 t2(1.0f, 0.0f);
    vec2 t3(1.0f, 1.0f);
    vec2 t4(0.0f, 1.0f);

    /* Vertex coordinates */
    float texWidth = guiTexture->getWidth() / float(window->getWidth());
    float texHeight = guiTexture->getHeight() / float(window->getHeight());
    vec2 v1(-texWidth, -texHeight);
    vec2 v2(texWidth, -texHeight);
    vec2 v3(texWidth, texHeight);
    vec2 v4(-texWidth, texHeight);

    /* First triangle */
    batch->texCoord(t1);
    batch->vertex(v1);
    batch->texCoord(t2);
    batch->vertex(v2);
    batch->texCoord(t3);
    batch->vertex(v3);

    /* Second triangle */
    batch->texCoord(t3);
    batch->vertex(v3);
    batch->texCoord(t4);
    batch->vertex(v4);
    batch->texCoord(t1);
    batch->vertex(v1);
}

void TetrisApplication::renderGameField() {
    /* Texture coordinates */
    vec2 t1(0.0f, 0.0f);
    vec2 t2(1.0f, 0.0f);
    vec2 t3(1.0f, 1.0f);
    vec2 t4(0.0f, 1.0f);

    for (int row = 0; row < gameField.size(); row++) {
        for (int column = 0; column < gameField[row].size(); column++) {
            if (gameField[row][column] > 0) {
                /* Meta data */
                int i = gameField[row][column] - 1;
                int w = blockTexture->getWidth();
                int h = blockTexture->getHeight();
                ivec2 offset(5, 5);
                float halfWidth = float(window->getWidth()) / 2.0f;
                float halfHeight = float(window->getHeight()) / 2.0f;

                /* Pixel coordinates */
                vec2 px1(offset.x + column * w, offset.y + (gameField.size() - 1 - row) * h);
                vec2 px2(offset.x + column * w + w, offset.y + (gameField.size() - 1 - row) * h);
                vec2 px3(offset.x + column * w + w, offset.y + (gameField.size() - 1 - row) * h + h);
                vec2 px4(offset.x + column * w, offset.y + (gameField.size() - 1 - row) * h + h);

                /* Vertex coordinates */
                vec2 v1(px1.x / halfWidth - 1.0f, px1.y / halfHeight - 1.0f);
                vec2 v2(px2.x / halfWidth - 1.0f, px2.y / halfHeight - 1.0f);
                vec2 v3(px3.x / halfWidth - 1.0f, px3.y / halfHeight - 1.0f);
                vec2 v4(px4.x / halfWidth - 1.0f, px4.y / halfHeight - 1.0f);

                /* First triangle */
                batch->color(colors[i]);
                batch->texCoord(t1);
                batch->vertex(v1);
                batch->color(colors[i]);
                batch->texCoord(t2);
                batch->vertex(v2);
                batch->color(colors[i]);
                batch->texCoord(t3);
                batch->vertex(v3);

                /* Second triangle */
                batch->color(colors[i]);
                batch->texCoord(t3);
                batch->vertex(v3);
                batch->color(colors[i]);
                batch->texCoord(t4);
                batch->vertex(v4);
                batch->color(colors[i]);
                batch->texCoord(t1);
                batch->vertex(v1);
            }
        }
    }
}

void TetrisApplication::renderTetromino() {
    /* Texture coordinates */
    vec2 t1(0.0f, 0.0f);
    vec2 t2(1.0f, 0.0f);
    vec2 t3(1.0f, 1.0f);
    vec2 t4(0.0f, 1.0f);

    for (int row = 0; row < currentTetromino.shape.size(); row++) {
        for (int column = 0; column < currentTetromino.shape[row].size(); column++) {
            if (currentTetromino.shape[row][column] > 0) {
                /* Meta data */
                int i = currentTetromino.shape[row][column] - 1;
                int w = blockTexture->getWidth();
                int h = blockTexture->getHeight();
                ivec2 offset(5, 5);
                float halfWidth = float(window->getWidth()) / 2.0f;
                float halfHeight = float(window->getHeight()) / 2.0f;

                /* Pixel coordinates */
                vec2 px1(offset.x + (column + currentTetromino.position.y) * w,
                         offset.y + ((gameField.size() - 1 + currentTetromino.shape.size() - 1) - (row + currentTetromino.position.x)) * h);
                vec2 px2(offset.x + (column + currentTetromino.position.y) * w + w,
                         offset.y + ((gameField.size() - 1 + currentTetromino.shape.size() - 1) - (row + currentTetromino.position.x)) * h);
                vec2 px3(offset.x + (column + currentTetromino.position.y) * w + w,
                         offset.y + ((gameField.size() - 1 + currentTetromino.shape.size() - 1) - (row + currentTetromino.position.x)) * h + h);
                vec2 px4(offset.x + (column + currentTetromino.position.y) * w,
                         offset.y + ((gameField.size() - 1 + currentTetromino.shape.size() - 1) - (row + currentTetromino.position.x)) * h + h);

                /* Vertex coordinates */
                vec2 v1(px1.x / halfWidth - 1.0f, px1.y / halfHeight - 1.0f);
                vec2 v2(px2.x / halfWidth - 1.0f, px2.y / halfHeight - 1.0f);
                vec2 v3(px3.x / halfWidth - 1.0f, px3.y / halfHeight - 1.0f);
                vec2 v4(px4.x / halfWidth - 1.0f, px4.y / halfHeight - 1.0f);

                /* First triangle */
                batch->color(colors[i]);
                batch->texCoord(t1);
                batch->vertex(v1);
                batch->color(colors[i]);
                batch->texCoord(t2);
                batch->vertex(v2);
                batch->color(colors[i]);
                batch->texCoord(t3);
                batch->vertex(v3);

                /* Second triangle */
                batch->color(colors[i]);
                batch->texCoord(t3);
                batch->vertex(v3);
                batch->color(colors[i]);
                batch->texCoord(t4);
                batch->vertex(v4);
                batch->color(colors[i]);
                batch->texCoord(t1);
                batch->vertex(v1);
            }
        }
    }
}

void TetrisApplication::renderNextTetromino() {
    Tetromino nextTetromino = nextTetrominos[0];

    /* Texture coordinates */
    vec2 t1(0.0f, 0.0f);
    vec2 t2(1.0f, 0.0f);
    vec2 t3(1.0f, 1.0f);
    vec2 t4(0.0f, 1.0f);

    /* Remove empty rows */
    vector<vector<int>> renderShape;
    for (int row = 0; row < nextTetromino.shape.size(); row++) {
        bool emptyRow = true;
        for (int column = 0; column < nextTetromino.shape[row].size(); column++) {
            if (nextTetromino.shape[row][column] > 0) {
                emptyRow = false;
                break;
            }
        }
        if (!emptyRow) {
            renderShape.insert(renderShape.begin(), nextTetromino.shape[row]);
        }
    }

    for (int row = 0; row < renderShape.size(); row++) {
        for (int column = 0; column < renderShape[row].size(); column++) {
            if (renderShape[row][column] > 0) {
                /* Meta data */
                int i = renderShape[row][column] - 1;
                int w = blockTexture->getWidth();
                int h = blockTexture->getHeight();
                ivec2 offset(346 + (4 * w - renderShape[0].size() * w) / 2.0f,
                             512 + (2 * h - renderShape.size() * h) / 2.0f);
                float halfWidth = float(window->getWidth()) / 2.0f;
                float halfHeight = float(window->getHeight()) / 2.0f;

                /* Pixel coordinates */
                vec2 px1(offset.x + column * w, offset.y + row * h);
                vec2 px2(offset.x + column * w + w, offset.y + row * h);
                vec2 px3(offset.x + column * w + w, offset.y + row * h + h);
                vec2 px4(offset.x + column * w, offset.y + row * h + h);

                /* Vertex coordinates */
                vec2 v1(px1.x / halfWidth - 1.0f, px1.y / halfHeight - 1.0f);
                vec2 v2(px2.x / halfWidth - 1.0f, px2.y / halfHeight - 1.0f);
                vec2 v3(px3.x / halfWidth - 1.0f, px3.y / halfHeight - 1.0f);
                vec2 v4(px4.x / halfWidth - 1.0f, px4.y / halfHeight - 1.0f);

                /* First triangle */
                batch->color(colors[i]);
                batch->texCoord(t1);
                batch->vertex(v1);
                batch->color(colors[i]);
                batch->texCoord(t2);
                batch->vertex(v2);
                batch->color(colors[i]);
                batch->texCoord(t3);
                batch->vertex(v3);

                /* Second triangle */
                batch->color(colors[i]);
                batch->texCoord(t3);
                batch->vertex(v3);
                batch->color(colors[i]);
                batch->texCoord(t4);
                batch->vertex(v4);
                batch->color(colors[i]);
                batch->texCoord(t1);
                batch->vertex(v1);
            }
        }
    }
}
