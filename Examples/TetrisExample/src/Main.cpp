/*
 * @file Main.cpp
 * @author Heiko Brumme
 * 
 * This files contains the entry point of the Tetris example.
 */

#include <SimpleGL_Backend_Vulkan.hpp>

#include "TetrisApplication.hpp"

/** Entry point of the tetris example. */
int main() {
    sgl::init();
    sgl::initVK();
    sgl::Application* app = new TetrisApplication();
    app->run();
}
