/*
 * @file TetrisApplication.hpp
 * @author Heiko Brumme
 * 
 * This file contains the class for the Tetris example.
 */

#ifndef TETRISAPPLICATION_HPP
#define TETRISAPPLICATION_HPP

#include <SimpleGL_Backend_Vulkan.hpp>

/**
 * @struct Tetromino
 *
 * Structure for storing the shape and the top left corner of a tetromino.
 */
struct Tetromino {

    /** The shape of the tetromino. */
    std::vector<std::vector<int>> shape;

    /**
     * The position of the tetromino. This will alway be the bottom left corner
     * of the shape vector.
     */
    glm::vec2 position;
};

/**
 * @class TetrisApplication
 *
 * This class is a example how to implement Tetris with the framework.
 */
class TetrisApplication : public sgl::ApplicationVK {

private:
    /** Stores the seven standard Tetrominos. */
    std::vector<Tetromino> tetrominos;

    /** Stores the colors for the seven standard Tetrominos. */
    std::vector<glm::vec3> colors;

    /** The game field representation. */
    std::vector<std::vector<int>> gameField;

    /** Stores the current Tetromino. */
    Tetromino currentTetromino;

    /** Stores the next four Tetrominos. */
    std::vector<Tetromino> nextTetrominos;

    /** The Tetromino falling speed per second. */
    float speed;

    /** Tells wheter the game is over or not. */
    bool gameOver;

    /** Stores the background gui texture. */
    sgl::TextureImage* guiTexture;

    /** Stores the texture for a single Tetromino block. */
    sgl::TextureImage* blockTexture;

public:
    /** Creates the tetris example application. */
    TetrisApplication(void);

    /** Deletes the tetris example application. */
    ~TetrisApplication(void);

    /**
     * This method is intended to handle events like user input.
     * Overrides Application::handleEvents.
     *
     * @param events A list of events.
     */
    void handleEvents(std::vector<sgl::Event*> events) override;

    /**
     * This method is intended to update objects of the application.
     * Overrides Application::updateScene.
     *
     * @param delta The delta time.
     */
    void updateScene(float delta) override;

    /**
     * This method is intended to used for drawing the scene.
     * Overrides Application::renderScene.
     *
     * @param alpha The alpha value.
     */
    void renderScene(float alpha) override;

    /** Renders the background GUI. */
    void renderGUI();

    /** Renders the landed blocks of the game field. */
    void renderGameField();

    /** Renders the current Tetromino. */
    void renderTetromino();

    /** Renders the next Tetromino*/
    void renderNextTetromino();
};

#endif /* TETRISAPPLICATION_HPP */
