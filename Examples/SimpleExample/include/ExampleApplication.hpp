/**
 * @file ExampleApplication.hpp
 * @author Heiko Brumme
 *
 * This file contains the class for the framework OpenGL demo.
 */

#ifndef EXAMPLEAPPLICATION_HPP
#define EXAMPLEAPPLICATION_HPP

#include <SimpleGL_Backend_OpenGL.hpp>

/**
 * @struct Element
 *
 * Structure for storing the element indices of the OBJ file.
 */
struct Element {

    /** Index for the vertex. */
    int vertexIndex;

    /** Index for the normal. */
    int normalIndex;
};

/**
 * @class ExampleApplication
 *
 * This class is a example how to use the framework.
 */
class ExampleApplication : public sgl::ApplicationGL {

private:
    /** Stores the material of the Suzanne model. */
    sgl::Material material;

    /** Stores the light for the lighting shaders. */
    sgl::Light light;

    /** Stores the camera. */
    sgl::Camera camera;

    /** The camera speed per second. */
    float cameraSpeed;

    /** The velocity of the camera. */
    glm::vec3 cameraVelocity;

    /** Stores the last cursor position. */
    glm::dvec2 lastCursorPos;

    /** Tells wheter the mouse is pressed. */
    bool mousePressed;

    /** Stores the vertices of the smooth Suzanne model. */
    std::vector<glm::vec3> smoothVertices;

    /** Stores the normals of the smooth Suzanne model. */
    std::vector<glm::vec3> smoothNormals;

    /** Stores the indices of the smooth Suzanne model. */
    std::vector<Element> smoothElements;

    /** Stores the vertices of the flat Suzanne model. */
    std::vector<glm::vec3> flatVertices;

    /** Stores the normals of the flat Suzanne model. */
    std::vector<glm::vec3> flatNormals;

    /** Stores the indices of the flat Suzanne model. */
    std::vector<Element> flatElements;

    /** Stores the example texture. */
    sgl::Texture* texture;

    /** Stores a Gouraud shader program. */
    sgl::ShaderProgram* gouraudShaderProgram;

    /** Stores a Phong shader program. */
    sgl::ShaderProgram* phongShaderProgram;

    /** Tells wheter to draw the model or the texture. */
    bool drawModel;

    /** Tells wheter to draw the smooth or the flat model. */
    bool smoothModel;

    /** Tells wheter to use per fragment or per vertex lighting. */
    bool perFragment;

    /** Tells wheter to use perspective or ortho projection. */
    bool usingPerspective;

public:
    /** Creates the example application. */
    ExampleApplication(void);

    /** Deletes the example application. */
    ~ExampleApplication(void);

    /**
     * This method is intended to handle events like user input.
     * Overrides Application::handleEvents.
     *
     * @param events A list of events.
     */
    void handleEvents(std::vector<sgl::Event*> events) override;

    /**
     * Handles a key event.
     *
     * @param event The event to handle.
     */
    void handleKeyEvent(sgl::KeyEvent* event);

    /**
     * Handles a mouse button event.
     *
     * @param event The event to handle.
     */
    void handleMouseButtonEvent(sgl::MouseButtonEvent* event);

    /**
     * This method is intended to update objects of the application.
     * Overrides Application::updateScene.
     *
     * @param delta The delta time.
     */
    void updateScene(float delta) override;

    /**
     * This method is intended to used for drawing the scene.
     * Overrides Application::renderScene.
     *
     * @param alpha The alpha value.
     */
    void renderScene(float alpha) override;

    /** Renders the Suzanne model. */
    void renderMonkey();

    /** Renders a textured quad. */
    void renderTexture();

    /**
     * Setup for the a shader program.
     *
     * @param program The shader program.
     */
    void setupShader(sgl::ShaderProgram* program);

    /**
     * Sets the shader program light.
     *
     * @param program The shader program.
     */
    void setMaterial(sgl::ShaderProgram* program);

    /**
     * Sets the shader program light.
     *
     * @param program The shader program.
     */
    void setLight(sgl::ShaderProgram* program);

    /**
     * Sets the shader program light.
     *
     * @param program The shader program.
     */
    void setCamera(sgl::ShaderProgram* program);

    /**
     * Loads a Wavefront OBJ file.
     *
     * @param filePath The path to the OBJ file.
     * @param vertices The storage for the vertices.
     * @param normals The storage for the normals.
     * @param elements The storage for the elements.
     */
    void loadOBJ(std::string filePath, std::vector<glm::vec3> &vertices, std::vector<glm::vec3> &normals, std::vector<Element> &elements);
};

#endif /* EXAMPLEAPPLICATION_HPP */
