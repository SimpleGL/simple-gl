/**
 * @file ExampleApplication.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the ExampleApplication.hpp header.
 */

#include "ExampleApplication.hpp"

#include <fstream>

using namespace sgl;
using namespace std;
using namespace glm;

ExampleApplication::ExampleApplication(void) : ApplicationGL(800, 600, "SimpleGL Example Application") {
    /* Load texture */
    texture = textureManager->load("./resource/red_panda.jpg", "exampleTexture");

    /* Load models */
    loadOBJ("./resource/smoothSuzanne.obj", smoothVertices, smoothNormals, smoothElements);
    loadOBJ("./resource/flatSuzanne.obj", flatVertices, flatNormals, flatElements);
    smoothModel = true;
    drawModel = true;

    /* Load custom shaders */
    gouraudShaderProgram = shaderManager->loadShaderProgram("./resource/gouraudVertex.glsl", "./resource/gouraudFragment.glsl", "gouraudVertex", "gouraudFragment");
    phongShaderProgram = shaderManager->loadShaderProgram("./resource/phongVertex.glsl", "./resource/phongFragment.glsl", "phongVertex", "phongFragment");
    perFragment = true;

    /* Setup jade material */
    /* More materials can be found here: http://devernay.free.fr/cours/opengl/materials.html */
    material.ambient = vec4(0.135f, 0.2225f, 0.1575f, 1.0f);
    material.diffuse = vec4(0.54f, 0.89f, 0.63f, 1.0f);
    material.specular = vec4(0.316228f, 0.316228f, 0.316228f, 1.0f);
    material.shininess = 0.1f * 128.0f;

    /* Setup light */
    light.ambient = vec4(0.2f, 0.2f, 0.2f, 1.0f);
    light.diffuse = vec4(0.5f, 0.5f, 0.5f, 1.0f);
    light.specular = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    light.position = vec4(1.2f, 1.0f, 2.0f, 1.0f);

    /* Setup camera */
    camera.position = vec3(0.0f, 0.0f, 3.0f);
    camera.front = vec3(0.0f, 0.0f, -1.0f);
    camera.up = vec3(0.0f, 1.0f, 0.0f);
    cameraSpeed = 5.0f;
    cameraVelocity = vec3(0.0f, 0.0f, 0.0f);
    mousePressed = false;

    /* Set MVP matrix */
    batch->setModel(Model::identity());
    batch->setView(View::lookAt(camera));
    batch->setProjection(Projection::perspective(45.0f, float(window->getWidth()) / float(window->getHeight()), 1.0f, 10.0f));
    usingPerspective = true;

    /* Setup custom shaders */
    setupShader(gouraudShaderProgram);
    setupShader(phongShaderProgram);

    /* Enable blending */
    GL::enable(Capability::BLEND);
    GL::blendFunc(BlendFactor::SRC_ALPHA, BlendFactor::ONE_MINUS_SRC_ALPHA);

    /* Enable depth test */
    GL::enable(Capability::DEPTH_TEST);
    GL::depthFunc(DepthFunction::LEQUAL);

    /* Enable back face culling */
    GL::enable(Capability::CULL_FACE);
    GL::cullFace(CullFaceMode::BACK);

    /* Set clear color to gray */
    GL::clearColor(0.5f, 0.5f, 0.5f, 1.0f);
}

ExampleApplication::~ExampleApplication(void) {
    /* Nothing to do here */
}

void ExampleApplication::handleEvents(std::vector<sgl::Event*> events) {
    /* Loop over all events */
    for (unsigned i = 0; i < events.size(); i++) {
        Event* event = events[i];
        switch (event->getType()) {
            case EventType::KEY_EVENT:
                /* Dynamic cast the event to a key event */
                handleKeyEvent(dynamic_cast<KeyEvent*> (event));
                break;

            case EventType::MOUSE_BUTTON_EVENT:
                /* Dynamic cast the event to a mouse button event */
                handleMouseButtonEvent(dynamic_cast<MouseButtonEvent*> (event));
                break;
        }
    }
}

void ExampleApplication::handleKeyEvent(sgl::KeyEvent* event) {
    /* Pushing escape will result in closing the window */
    if (event->getKey() == Key::KEY_ESCAPE) {
        running = false;
    }

    /* Pushing space resets the model matrix */
    if (event->getKey() == Key::KEY_SPACE && event->getAction() == Action::PRESS) {
        batch->setModel(Model::identity());
    }

    /* Pushing tab will change drawing a texture or the model */
    if (event->getKey() == Key::KEY_TAB && event->getAction() == Action::PRESS) {
        drawModel = !drawModel;
    }

    /* Pushing F will change flat and smooth model */
    if (event->getKey() == Key::KEY_F && event->getAction() == Action::PRESS) {
        smoothModel = !smoothModel;
    }

    /* Pushing L change the lighting */
    if (event->getKey() == Key::KEY_L && event->getAction() == Action::PRESS) {
        perFragment = !perFragment;
    }

    /* Pushing P will change the projection */
    if (event->getKey() == Key::KEY_P && event->getAction() == Action::PRESS) {
        if (!usingPerspective) {
            usingPerspective = true;
            batch->setProjection(Projection::perspective(45.0f, float(window->getWidth()) / float(window->getHeight()), 1.0f, 10.0f));
        } else {
            usingPerspective = false;
            float ratio = float(window->getWidth()) / float(window->getHeight());
            batch->setProjection(Projection::ortho(-ratio, ratio, -1.0f, 1.0f, -10.0f, 10.0f));
        }
    }

    /* Pushing W will result in moving forwards */
    if (event->getKey() == Key::KEY_W) {
        cameraVelocity += cameraSpeed * camera.front;
    }

    /* Pushing S will result in moving backwards */
    if (event->getKey() == Key::KEY_S) {
        cameraVelocity -= cameraSpeed * camera.front;
    }

    /* Pushing A will result in moving left */
    if (event->getKey() == Key::KEY_A) {
        cameraVelocity -= normalize(cross(camera.front, camera.up)) * cameraSpeed;
    }

    /* Pushing D will result in moving right */
    if (event->getKey() == Key::KEY_D) {
        cameraVelocity += normalize(cross(camera.front, camera.up)) * cameraSpeed;
    }
}

void ExampleApplication::handleMouseButtonEvent(sgl::MouseButtonEvent* event) {
    /* Pressing the left mouse button activates rotating */
    if (event->getButton() == MouseButton::MOUSE_BUTTON_LEFT && event->getAction() == Action::PRESS) {
        lastCursorPos = window->getCursorPos();
        mousePressed = true;
    }/* Releasing the left mouse button deactivates rotating */
    else if (event->getButton() == MouseButton::MOUSE_BUTTON_LEFT && event->getAction() == Action::RELEASE) {
        mousePressed = false;
    }
}

void ExampleApplication::updateScene(float delta) {
    /* Update camera */
    if (cameraVelocity != vec3()) {
        /* Add velocity to camera position */
        camera.position += cameraVelocity * delta;
        cameraVelocity = vec3();

        /* Update camera */
        batch->setView(View::lookAt(camera));
        setCamera(gouraudShaderProgram);
        setCamera(phongShaderProgram);
    }

    /* Rotate model if mouse is pressed */
    if (mousePressed) {
        /* Get current cursor Position */
        dvec2 cursorPosition = window->getCursorPos();

        /* Calculate offset */
        vec2 offset = vec2(cursorPosition.x - lastCursorPos.x, lastCursorPos.y - cursorPosition.y);
        lastCursorPos = cursorPosition;

        /* Multiply with sensitivity */
        float sensitivity = 45.0f;
        offset *= sensitivity;

        /* Update model matrix */
        batch->setModel(Model::rotate(batch->getModel(), offset.x * delta, vec3(0, 1, 0)));
        batch->setModel(Model::rotate(batch->getModel(), -offset.y * delta, vec3(1, 0, 0)));
    } else {
        /* Rotate 45� per second */
        float angle = 45.0f * delta;
        batch->setModel(Model::rotate(batch->getModel(), angle, vec3(0, 1, 0)));
    }
}

void ExampleApplication::renderScene(float alpha) {
    if (drawModel) {
        renderMonkey();
    } else {
        renderTexture();
    }
}

void ExampleApplication::renderMonkey() {
    /* Use custom shader program */
    if (perFragment) {
        batch->setShaderProgram(phongShaderProgram);
    } else {
        batch->setShaderProgram(gouraudShaderProgram);
    }

    vector<vec3> vertices, normals;
    vector<Element> elements;

    if (smoothModel) {
        vertices = smoothVertices;
        normals = smoothNormals;
        elements = smoothElements;
    } else {
        vertices = flatVertices;
        normals = flatNormals;
        elements = flatElements;
    }

    /* Draw elements */
    batch->begin(Primitive::TRIANGLES);
    for (unsigned i = 0; i < elements.size(); i++) {
        int normalIndex = elements[i].normalIndex;
        batch->normal(normals[normalIndex]);
        int vertexIndex = elements[i].vertexIndex;
        batch->vertex(vertices[vertexIndex]);
    }
    batch->end();
}

void ExampleApplication::renderTexture() {
    /* Texture coordinates */
    vec2 t1(0.0f, 0.0f);
    vec2 t2(1.0f, 0.0f);
    vec2 t3(1.0f, 1.0f);
    vec2 t4(0.0f, 1.0f);

    /* Vertex coordinates */
    float texWidth = texture->getWidth() / float(window->getWidth());
    float texHeight = texture->getHeight() / float(window->getHeight());
    vec2 v1(-texWidth, -texHeight);
    vec2 v2(texWidth, -texHeight);
    vec2 v3(texWidth, texHeight);
    vec2 v4(-texWidth, texHeight);

    /* Use default shader program */
    batch->resetShaderProgram();

    texture->bind();
    batch->begin(Primitive::TRIANGLES);
    {
        /* First triangle */
        batch->texCoord(t1);
        batch->vertex(v1);
        batch->texCoord(t2);
        batch->vertex(v2);
        batch->texCoord(t3);
        batch->vertex(v3);

        /* Second triangle */
        batch->texCoord(t3);
        batch->vertex(v3);
        batch->texCoord(t4);
        batch->vertex(v4);
        batch->texCoord(t1);
        batch->vertex(v1);
    }
    batch->end();
}

void ExampleApplication::setupShader(sgl::ShaderProgram* program) {
    program->use();
    setMaterial(program);
    setLight(program);
    setCamera(program);
}

void ExampleApplication::setMaterial(sgl::ShaderProgram* program) {
    program->setUniform("material.ambient", material.ambient);
    program->setUniform("material.diffuse", material.diffuse);
    program->setUniform("material.specular", material.specular);
    program->setUniform("material.shininess", material.shininess);
}

void ExampleApplication::setLight(sgl::ShaderProgram* program) {
    program->setUniform("light.ambient", light.ambient);
    program->setUniform("light.diffuse", light.diffuse);
    program->setUniform("light.specular", light.specular);
    program->setUniform("light.position", light.position);
}

void ExampleApplication::setCamera(sgl::ShaderProgram* program) {
    program->setUniform("viewPos", camera.position);
}

void ExampleApplication::loadOBJ(std::string filePath, std::vector<glm::vec3> &vertices, std::vector<glm::vec3> &normals, std::vector<Element> &elements) {
    /* Open file path */
    ifstream fileStream(filePath);
    if (fileStream.is_open()) {
        while (fileStream.good()) {
            string line;
            getline(fileStream, line);

            /* If line starts with "v" it is a vertex */
            if (line.find("v ") == 0) {
                /* Format: v <float x> <float y> <float z> */
                string vertexInfo(line.substr(2));
                string::size_type idx, size_t;
                vec3 vertex;

                /* Get x, y, z coordinates */
                vertex.x = stof(vertexInfo, &idx);
                vertex.y = stof(vertexInfo.substr(idx), &size_t);
                idx += size_t;
                vertex.z = stof(vertexInfo.substr(idx));

                /* Add vertex to list */
                vertices.push_back(vertex);
            }/* If line starts with "vn" it is a normal */
            else if (line.find("vn ") == 0) {
                /* Format: vn <float x> <float y> <float z> */
                string normalInfo(line.substr(3));
                string::size_type idx, size_t;
                vec3 normal;

                /* Get x, y, z coordinates */
                normal.x = stof(normalInfo, &idx);
                normal.y = stof(normalInfo.substr(idx), &size_t);
                idx += size_t;
                normal.z = stof(normalInfo.substr(idx));

                /* Add normal to list */
                normals.push_back(normal);
            }/* If line starts with "f" it is a face e.g. vertex and normal index */
            else if (line.find("f ") == 0) {
                /* Format: f <int v1>//<int vn1> <int v2>//<int vn2> <int v3>//<int vn3> */
                string faceInfo(line.substr(2));
                string::size_type idx, size_t;
                Element e1, e2, e3;
                int v1, v2, v3;
                int vn1, vn2, vn3;

                /* Get first indices */
                v1 = stoi(faceInfo, &idx);
                idx += 2;
                vn1 = stoi(faceInfo.substr(idx), &size_t);
                idx += size_t;

                /* Get second indices */
                v2 = stoi(faceInfo.substr(idx), &size_t);
                idx += size_t + 2;
                vn2 = stoi(faceInfo.substr(idx), &size_t);
                idx += size_t;

                /* Get third indices */
                v3 = stoi(faceInfo.substr(idx), &size_t);
                idx += size_t + 2;
                vn3 = stoi(faceInfo.substr(idx));

                /* Set vertex indices */
                e1.vertexIndex = v1 - 1;
                e2.vertexIndex = v2 - 1;
                e3.vertexIndex = v3 - 1;

                /* Set normal indices */
                e1.normalIndex = vn1 - 1;
                e2.normalIndex = vn2 - 1;
                e3.normalIndex = vn3 - 1;

                /* Add indices to list */
                elements.push_back(e1);
                elements.push_back(e2);
                elements.push_back(e3);
            }
        }
        fileStream.close();
    }
}
