/**
 * @file Main.cpp
 * @author Heiko Brumme
 *
 * This files contains the entry point of the example project.
 */

#include <SimpleGL_Backend_OpenGL.hpp>

#include "ExampleApplication.hpp"

/** Entry point of the example project. */
int main() {
    sgl::init();
    sgl::initGL();
    sgl::Application* app = new ExampleApplication();
    app->run();
}
