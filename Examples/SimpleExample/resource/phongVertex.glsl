#version 150 core

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

in vec3 position;
in vec3 normal;

out vec3 outNormal;
out vec3 fragPos;

void main() {
	/* Calculate vertex position */
	mat4 mvp = projection * view * model;
	gl_Position = mvp * vec4(position, 1.0f);

	/* Calculate frag position */
	outNormal = mat3(transpose(inverse(model))) * normal;
	fragPos = vec3(model * vec4(position, 1.0f));
}
