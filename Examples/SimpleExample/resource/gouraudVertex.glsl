#version 150 core

/* Struct for the material properties */
struct Material {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

/* Struct for the light properties */
struct Light {
	vec4 position;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform Material material;
uniform Light light;
uniform vec3 viewPos;

in vec3 position;
in vec3 normal;

out vec4 lightingColor;

void main() {
	/* Calculate vertex position */
	mat4 mvp = projection * view * model;
	gl_Position = mvp * vec4(position, 1.0f);

	/* Calculate frag position */
    vec3 fragPos = vec3(model * vec4(position, 1.0f));
    vec3 outNormal = mat3(transpose(inverse(model))) * normal;
	
	/* Calculate ambient */
	vec4 ambient = light.ambient * material.ambient;

	/* Calculate diffuse */
	vec3 norm = normalize(outNormal);
	vec3 lightDir = normalize(light.position.xyz - fragPos);
	float diff = max(dot(norm, lightDir), 0.0f);
	vec4 diffuse = light.diffuse * (diff * material.diffuse);

	/* Calculate specular */
	vec3 viewDir = normalize(viewPos - fragPos);
	vec3 reflectDir = reflect(-lightDir, norm); 
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	vec4 specular = light.specular * (spec * material.specular);

	lightingColor = ambient + diffuse + specular;
}
