#version 150 core

in vec4 lightingColor;

out vec4 fragColor;

void main() {
	fragColor = lightingColor;
}
