/**
 * @file logging.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the logging.hpp header.
 */

#include "SimpleGL/logging.hpp"

#include <iostream>

using namespace std;

namespace sgl {

    Logger::Logger(void) {
        /* Nothing to do here */
    }

    void Logger::logInfo(std::string message) {
        cout << message << endl;
    }

    void Logger::logError(std::string message) {
        cerr << message << endl;
    }
}
