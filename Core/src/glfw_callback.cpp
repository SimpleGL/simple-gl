/**
 * @file glfw_callback.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the glfw_callback.hpp header.
 */

#include "SimpleGL/glfw_callback.hpp"

#include "SimpleGL/logging.hpp"
#include "SimpleGL/event.hpp"

using namespace std;

namespace sgl {
    ErrorCallback* ErrorCallback::current = nullptr;

    ErrorCallback* ErrorCallback::getCurrent() {
        return current;
    }

    void ErrorCallback::setCurrent(ErrorCallback* callback) {
        /* Only set if callback is not a nullptr */
        if (callback != nullptr) {
            current = callback;
        }
    }

    void ErrorCallback::dispatch(int error, const char* description) {
        /* Instance shouldn't be null when calling this function */
        if (current) {
            current->invoke(error, string(description));
        }
    }

    ErrorCallback::ErrorCallback(void) {
        /* Nothing to do here */
    }

    ErrorCallback::~ErrorCallback(void) {
        /* Set instance to nullptr if this was the current error callback */
        if (ErrorCallback::current == this) {
            ErrorCallback::current = nullptr;
        }
    }

    /** Default implementation of the error callback. */
    void ErrorCallback::invoke(int error, std::string description) {
        Logger::logError("GLFW error " + to_string(error) + ": " + description);
    }

    KeyCallback* KeyCallback::current = nullptr;

    KeyCallback* KeyCallback::getCurrent() {
        return current;
    }

    void KeyCallback::setCurrent(KeyCallback* callback) {
        /* Only set if callback is not a nullptr */
        if (callback != nullptr) {
            current = callback;
        }
    }

    void KeyCallback::dispatch(GLFWwindow* window, int key, int scancode, int action, int mods) {
        /* Instance shouldn't be null when calling this function */
        if (current) {
            current->invoke(window, static_cast<Key> (key), scancode, static_cast<Action> (action), static_cast<Modifier> (mods));
        }
    }

    KeyCallback::KeyCallback(void) {
        /* Nothing to do here */
    }

    KeyCallback::~KeyCallback(void) {
        /* Set instance to nullptr if this was the current key callback */
        if (KeyCallback::current == this) {
            KeyCallback::current = nullptr;
        }
    }

    /** Default implementation of the key callback. */
    void KeyCallback::invoke(GLFWwindow* window, Key key, int scancode, Action action, Modifier mods) {
        KeyEvent* event = new KeyEvent(key, scancode, action, mods);
        Event::addEvent(event);
    }

    MouseButtonCallback* MouseButtonCallback::current = nullptr;

    MouseButtonCallback* MouseButtonCallback::getCurrent() {
        return current;
    }

    void MouseButtonCallback::setCurrent(MouseButtonCallback* callback) {
        /* Only set if callback is not a nullptr */
        if (callback != nullptr) {
            current = callback;
        }
    }

    void MouseButtonCallback::dispatch(GLFWwindow* window, int button, int action, int mods) {
        /* Instance shouldn't be null when calling this function */
        if (current) {
            current->invoke(window, static_cast<MouseButton> (button), static_cast<Action> (action), static_cast<Modifier> (mods));
        }
    }

    MouseButtonCallback::MouseButtonCallback(void) {
        /* Nothing to do here */
    }

    MouseButtonCallback::~MouseButtonCallback(void) {
        /* Set instance to nullptr if this was the current mouse button callback */
        if (MouseButtonCallback::current == this) {
            MouseButtonCallback::current = nullptr;
        }
    }

    /** Default implementation of the mouse button callback. */
    void MouseButtonCallback::invoke(GLFWwindow* window, MouseButton button, Action action, Modifier mods) {
        MouseButtonEvent* event = new MouseButtonEvent(button, action, mods);
        Event::addEvent(event);
    }
}
