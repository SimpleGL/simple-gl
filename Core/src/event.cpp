/**
 * @file event.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the event.hpp header.
 */

#include "SimpleGL/event.hpp"

namespace sgl {
    std::vector<Event*> Event::events;

    std::vector<Event*> Event::getEvents() {
        return events;
    }

    void Event::addEvent(Event* event) {
        if (event != nullptr) {
            events.push_back(event);
        }
    }

    void Event::clearEvents() {
        /* Clear events */
        events.clear();
    }

    Event::Event(EventType type)
    : type(type) {
        /* Nothing to do here */
    }

    Event::~Event(void) {
        /* Nothing to do here */
    }

    EventType Event::getType() {
        return type;
    }

    KeyEvent::KeyEvent(Key key, int scancode, Action action, Modifier mods)
    : Event(EventType::KEY_EVENT),
    key(key), scancode(scancode), action(action), mods(mods) {
        /* Nothing to do here */
    }

    KeyEvent::~KeyEvent(void) {
        /* Nothing to do here */
    }

    Key KeyEvent::getKey() {
        return key;
    }

    int KeyEvent::getScancode() {
        return scancode;
    }

    Action KeyEvent::getAction() {
        return action;
    }

    Modifier KeyEvent::getMods() {
        return mods;
    }

    MouseButtonEvent::MouseButtonEvent(MouseButton button, Action action, Modifier mods)
    : Event(EventType::MOUSE_BUTTON_EVENT),
    button(button), action(action), mods(mods) {
        /* Nothing to do here */
    }

    MouseButtonEvent::~MouseButtonEvent(void) {
        /* Nothing to do here */
    }

    MouseButton MouseButtonEvent::getButton() {
        return button;
    }

    Action MouseButtonEvent::getAction() {
        return action;
    }

    Modifier MouseButtonEvent::getMods() {
        return mods;
    }
}
