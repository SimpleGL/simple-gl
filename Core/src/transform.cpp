/**
 * @file transform.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the transform.hpp header.
 */

#include "SimpleGL/transform.hpp"

#include <glm/gtc/matrix_transform.hpp>

using namespace glm;

namespace sgl {

    Model::Model(void) {
        /* Nothing to do here */
    }

    glm::mat4 Model::identity() {
        return mat4();
    }

    glm::mat4 Model::scale(float x, float y, float z) {
        return Model::scale(vec3(x, y, z));
    }

    glm::mat4 Model::scale(glm::vec3 vector) {
        return Model::scale(mat4(), vector);
    }

    glm::mat4 Model::scale(glm::mat4 matrix, float x, float y, float z) {
        return Model::scale(matrix, vec3(x, y, z));
    }

    glm::mat4 Model::scale(glm::mat4 matrix, glm::vec3 vector) {
        return glm::scale(matrix, vector);
    }

    glm::mat4 Model::translate(float x, float y, float z) {
        return Model::translate(vec3(x, y, z));
    }

    glm::mat4 Model::translate(glm::vec3 vector) {
        return Model::translate(mat4(), vector);
    }

    glm::mat4 Model::translate(glm::mat4 matrix, float x, float y, float z) {
        return Model::translate(matrix, vec3(x, y, z));
    }

    glm::mat4 Model::translate(glm::mat4 matrix, glm::vec3 vector) {
        return glm::translate(matrix, vector);
    }

    glm::mat4 Model::rotate(float angle, float x, float y, float z) {
        return Model::rotate(angle, vec3(x, y, z));
    }

    glm::mat4 Model::rotate(float angle, glm::vec3 vector) {
        return Model::rotate(mat4(), angle, vector);
    }

    glm::mat4 Model::rotate(glm::mat4 matrix, float angle, float x, float y, float z) {
        return Model::rotate(matrix, angle, vec3(x, y, z));
    }

    glm::mat4 Model::rotate(glm::mat4 matrix, float angle, glm::vec3 vector) {
        /* angle needs to get calculated to radians */
        return glm::rotate(matrix, glm::radians(angle), vector);
    }

    View::View(void) {
        /* Nothing to do here */
    }

    glm::mat4 View::identity() {
        return mat4();
    }

    glm::mat4 View::lookAt(glm::vec3 eye, glm::vec3 center, glm::vec3 up) {
        return glm::lookAt(eye, center, up);
    }

    glm::mat4 View::lookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ) {
        return View::lookAt(vec3(eyeX, eyeY, eyeZ), vec3(centerX, centerY, centerZ), vec3(upX, upY, upZ));
    }

    glm::mat4 View::lookAt(Camera camera) {
        return View::lookAt(camera.position, camera.position + camera.front, camera.up);
    }

    Projection::Projection(void) {
        /* Nothing to do here */
    }

    glm::mat4 Projection::identity() {
        return mat4();
    }

    glm::mat4 Projection::ortho(float left, float right, float bottom, float top, float zNear, float zFar) {
        return glm::ortho(left, right, bottom, top, zNear, zFar);
    }

    glm::mat4 Projection::frustum(float left, float right, float bottom, float top, float zNear, float zFar) {
        return glm::frustum(left, right, bottom, top, zNear, zFar);
    }

    glm::mat4 Projection::perspective(float fovy, float aspect, float zNear, float zFar) {
        /* fovy needs to get calculated to radians */
        return glm::perspective(glm::radians(fovy), aspect, zNear, zFar);
    }
}
