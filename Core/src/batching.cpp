/**
 * @file batching.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the batching.hpp header.
 */

#include "SimpleGL/batching.hpp"

using namespace std;
using namespace glm;

namespace sgl {

    Batch::Batch() {
        /* Set MVP to identity */
        transformation.model = Model::identity();
        transformation.view = View::identity();
        transformation.projection = Projection::identity();
        transformation.changed = true;

        /* Initialize values */
        drawing = false;
        numVertices = 0;
    }

    Batch::~Batch(void) {
        /* Nothing to do here */
    }

    void Batch::begin() {
        /* Start drawing only if the batch isn't already drawing */
        if (!drawing) {
            drawing = true;
            numVertices = 0;
        }
    }

    void Batch::end() {
        /* End drawing only if the batch was drawing */
        if (drawing) {
            drawing = false;
            flush();
        }
    }

    void Batch::vertex(float x, float y) {
        vertex(vec2(x, y));
    }

    void Batch::vertex(float x, float y, float z) {
        vertex(vec3(x, y, z));
    }

    void Batch::vertex(glm::vec2 vertex) {
        Batch::vertex(vec3(vertex, 0.0f));
    }

    void Batch::vertex(glm::vec3 vertex) {
        vertices.push_back(vertex);
        numVertices++;
    }

    void Batch::color(float r, float g, float b) {
        color(vec3(r, g, b));
    }

    void Batch::color(float r, float g, float b, float a) {
        color(vec4(r, g, b, a));
    }

    void Batch::color(glm::vec3 color) {
        Batch::color(vec4(color, 1.0f));
    }

    void Batch::color(glm::vec4 color) {
        colors.push_back(color);
    }

    void Batch::texCoord(float s, float t) {
        texCoord(vec2(s, t));
    }

    void Batch::texCoord(glm::vec2 texCoord) {
        texCoords.push_back(texCoord);
    }

    void Batch::normal(float nx, float ny, float nz) {
        normal(vec3(nx, ny, nz));
    }

    void Batch::normal(glm::vec3 normal) {
        normals.push_back(normal);
    }

    void Batch::setModel(glm::mat4 model) {
        transformation.model = model;
        transformation.changed = true;
    }

    glm::mat4 Batch::getModel() {
        return transformation.model;
    }

    void Batch::setView(glm::mat4 view) {
        transformation.view = view;
        transformation.changed = true;
    }

    glm::mat4 Batch::getView() {
        return transformation.view;
    }

    void Batch::setProjection(glm::mat4 projection) {
        transformation.projection = projection;
        transformation.changed = true;
    }

    glm::mat4 Batch::getProjection() {
        return transformation.projection;
    }
}
