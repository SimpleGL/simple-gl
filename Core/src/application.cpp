/**
 * @file application.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the application.hpp header.
 */

#include "SimpleGL/application.hpp"

#include <GLFW/glfw3.h>

#include "SimpleGL/glfw_general.hpp"

namespace sgl {

    Application::Application() {
        targetUps = 60;
        targetFps = 60;
        running = false;
    }

    Application::~Application(void) {
        /* Delete window */
        delete window;

        /* Terminate GLFW */
        GLFW::terminate();
    }

    void Application::run() {
        init();
        mainLoop();
        dispose();
    }

    void Application::init() {
        TimeUtil::init();
        running = true;
    }

    void Application::mainLoop() {
        /* Initialize time accumulator and update interval */
        float accumulator = 0.0f;
        float interval = 1.0f / float(targetUps);

        /* Main Loop */
        while (running) {
            /* Get delta time and update the time accumulator */
            float delta = TimeUtil::getDelta();
            accumulator += delta;

            /* Handle events */
            handleEvents(Event::getEvents());
            Event::clearEvents();

            /* Update application if enough time has passed */
            while (accumulator >= interval) {
                updateScene(interval);
                TimeUtil::incUPS();
                accumulator -= interval;
            }

            /* Calculate alpha value for interpolation */
            float alpha = accumulator / interval;

            /* Clear screen and render scene */
            clear();
            renderScene(alpha);
            TimeUtil::incFPS();

            /* Update time utility to set FPS and UPS if a whole second has passed */
            TimeUtil::update();

            /* Show informations in the title */
            window->showInfoTitle();

            /* Swap buffers and poll events */
            window->update();

            /* Synchronize to the target FPS */
            window->sync(targetFps);

            /* Check if application should close */
            if (window->shouldClose()) {
                running = false;
            }
        }
    }

    void Application::dispose() {
        /* Nothing to do here */
    }

    double TimeUtil::lastLoopTime = 0.0;
    float TimeUtil::timeCount = 0.0f;
    int TimeUtil::fps = 0;
    int TimeUtil::fpsCount = 0;
    int TimeUtil::ups = 0;
    int TimeUtil::upsCount = 0;

    TimeUtil::TimeUtil(void) {
        /* Nothing to do here */
    }

    void TimeUtil::init() {
        lastLoopTime = getTime();
    }

    double TimeUtil::getTime() {
        return glfwGetTime();
    }

    float TimeUtil::getDelta() {
        /* Delta time is the elapsed time since the last loop and now */
        double now = getTime();
        float delta = float(now - lastLoopTime);
        lastLoopTime = now;

        /* Increment the time counter by the elapsed time */
        timeCount += delta;

        return delta;
    }

    void TimeUtil::update() {
        /* Update FPS and UPS if a whole second has passed */
        if (timeCount > 1.0f) {
            /* Update FPS */
            fps = fpsCount;
            fpsCount = 0;

            /* Update UPS */
            ups = upsCount;
            upsCount = 0;

            /* Subtract one second of the time counter */
            timeCount -= 1.0f;
        }
    }

    void TimeUtil::incFPS() {
        fpsCount++;
    }

    void TimeUtil::incUPS() {
        upsCount++;
    }

    int TimeUtil::getFPS() {
        /* If FPS is 0 return the value of the counter */
        return fps > 0 ? fps : fpsCount;
    }

    int TimeUtil::getUPS() {
        /* If UPS is 0 return the value of the counter */
        return ups > 0 ? ups : upsCount;
    }

    double TimeUtil::getLastLoopTime() {
        return lastLoopTime;
    }
}
