/**
 * @file glfw_general.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the glfw_general.hpp header.
 */

#include "SimpleGL/glfw_general.hpp"

#include <GLFW/glfw3.h>

namespace sgl {

    GLFW::GLFW(void) {
        /* Nothing to do here */
    }

    void GLFW::setErrorCallback(ErrorCallback* callback) {
        ErrorCallback::setCurrent(callback);
        glfwSetErrorCallback(ErrorCallback::dispatch);
    };

    void GLFW::pollEvents() {
        glfwPollEvents();
    }

    void GLFW::terminate() {
        glfwTerminate();
    }
}
