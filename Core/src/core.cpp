/**
 * @file core.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the core.hpp header.
 */

#include "SimpleGL/core.hpp"

#include <cstdlib>

#include <GLFW/glfw3.h>

#include "SimpleGL/glfw_general.hpp"
#include "SimpleGL/glfw_callback.hpp"
#include "SimpleGL/logging.hpp"

namespace sgl {

    void init() {
        /* Setup GLFW error callback */
        ErrorCallback* errorCallback = new ErrorCallback();
        GLFW::setErrorCallback(errorCallback);

        /* Initialize GLFW */
        Logger::logInfo("Initializing GLFW...");
        if (!glfwInit()) {
            Logger::logError("Could not init GLFW!");
            exit(EXIT_FAILURE);
        }

        Logger::logInfo("Initialization was successful.");
    }
}
