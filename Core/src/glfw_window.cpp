/**
 * @file glfw_window.cpp
 * @author Heiko Brumme
 *
 * This files contains the implementation of the glfw_window.hpp header.
 */

#include "SimpleGL/glfw_window.hpp"

#include <thread>

#include "SimpleGL/glfw_general.hpp"
#include "SimpleGL/application.hpp"

using namespace std;
using namespace glm;

namespace sgl {

    Window::Window() {
        /* Nothing to do here */
    }

    Window::~Window(void) {
        glfwDestroyWindow(handle);
    }

    GLFWwindow* Window::getHandle() {
        return handle;
    }

    int Window::getWidth() {
        return size.x;
    }

    void Window::setWidth(int width) {
        if (width > 0) {
            size.x = width;
            glfwSetWindowSize(handle, width, size.y);
            centerWindow();
        }
    }

    int Window::getHeight() {
        return size.y;
    }

    void Window::setHeight(int height) {
        if (height > 0) {
            size.y = height;
            glfwSetWindowSize(handle, size.x, height);
            centerWindow();
        }
    }

    glm::ivec2 Window::getSize() {
        return size;
    }

    void Window::setSize(glm::ivec2 size) {
        if (size.x > 0 && size.y > 0) {
            this->size = size;
            glfwSetWindowSize(handle, size.x, size.y);
            centerWindow();
        }
    }

    std::string Window::getTitle() {
        return title;
    }

    void Window::setTitle(std::string title) {
        this->title = title;
        glfwSetWindowTitle(handle, title.c_str());
    }

    void Window::showInfoTitle() {
        string fps = to_string(TimeUtil::getFPS());
        string ups = to_string(TimeUtil::getUPS());
        string infoTitle = title + " - FPS: " + fps + " | UPS: " + ups;
        glfwSetWindowTitle(handle, infoTitle.c_str());
    }

    void Window::centerWindow() {
        const GLFWvidmode* vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        glfwSetWindowPos(handle, (vidmode->width - size.x) / 2, (vidmode->height - size.y) / 2);
    }

    void Window::setKeyCallback(KeyCallback* callback) {
        KeyCallback::setCurrent(callback);
        glfwSetKeyCallback(handle, KeyCallback::dispatch);
    }

    void Window::setMouseButtonCallback(MouseButtonCallback* callback) {
        MouseButtonCallback::setCurrent(callback);
        glfwSetMouseButtonCallback(handle, MouseButtonCallback::dispatch);
    }

    void Window::update() {
        swapBuffers();
        GLFW::pollEvents();
    }

    bool Window::shouldClose() {
        return glfwWindowShouldClose(handle) == GLFW_TRUE;
    }

    void Window::sync(int fps) {
        /* Execute only if fps is greater than 0 */
        if (fps > 0) {
            /* Calculate target time in seconds */
            double now = TimeUtil::getTime();
            double lastLoopTime = TimeUtil::getLastLoopTime();
            double targetTime = 1.0 / double(fps);

            /* Yield and sleep for 1 µs until the target time matches */
            while (now - lastLoopTime < targetTime) {
                this_thread::yield();
                this_thread::sleep_for(chrono::nanoseconds(1000));

                now = TimeUtil::getTime();
            }
        }
    }

    glm::dvec2 Window::getCursorPos() {
        dvec2 position;
        glfwGetCursorPos(handle, &position.x, &position.y);
        return position;
    }

    double Window::getCursorX() {
        return getCursorPos().x;
    }

    double Window::getCursorY() {
        return getCursorPos().y;
    }
}
