/**
 * @file batching.hpp
 * @author Heiko Brumme
 *
 * This file contains the base class for batch rendering.
 */

#ifndef BATCHING_HPP
#define BATCHING_HPP

#include "SimpleGL_Export.h"

#include <vector>

#include <glm/glm.hpp>

#include "transform.hpp"

namespace sgl {

    /**
     * @class Batch
     *
     * This class defines methods for batch rendering.
     */
    class SIMPLEGL_EXPORT Batch {

    protected:
        /** The transformation for the batch. */
        Transformation transformation;

        /** Tells if the batch is drawing. */
        bool drawing;

        /** The counter for the number of vertices. */
        int numVertices;

        /** The storage for vertices. */
        std::vector<glm::vec3> vertices;

        /** The storage for colors. */
        std::vector<glm::vec4> colors;

        /** The storage for texture coordinates. */
        std::vector<glm::vec2> texCoords;

        /** The storage for normals. */
        std::vector<glm::vec3> normals;

        /** Initializes a new batch. */
        Batch(void);

    public:
        /** Destroys a batch. */
        ~Batch(void);

        /** Starts drawing with the default mode. */
        virtual void begin();

        /** Ends the drawing, this should also flush the batch. */
        virtual void end();

        /** Flushes the batch, could be used if the batch would overflow. */
        virtual void flush() = 0;

        /**
         * Defines a new vertex at specified coordinates.
         *
         * @param x The X-Coordinate.
         * @param y The Y-Coordinate.
         */
        void vertex(float x, float y);

        /**
         * Defines a new vertex at specified coordinates.
         *
         * @param x The X-Coordinate.
         * @param y The Y-Coordinate.
         * @param z The Z-Coordinate.
         */
        void vertex(float x, float y, float z);

        /**
         * Defines a new vertex at specified coordinates.
         *
         * @param vertex The vertex to define.
         */
        void vertex(glm::vec2 vertex);

        /**
         * Defines a new vertex at specified coordinates.
         *
         * @param vertex The vertex to define.
         */
        void vertex(glm::vec3 vertex);

        /**
         * Defines a new color with specified components.
         *
         * @param r The red component.
         * @param g The green component.
         * @param b The blue component.
         */
        void color(float r, float g, float b);

        /**
         * Defines a new color with specified components.
         *
         * @param r The red component.
         * @param g The green component.
         * @param b The blue component.
         * @param a The alpha component.
         */
        void color(float r, float g, float b, float a);

        /**
         * Defines a new color with specified components.
         *
         * @param color The color to define.
         */
        void color(glm::vec3 color);

        /**
         * Defines a new color with specified components.
         *
         * @param color The color to define.
         */
        void color(glm::vec4 color);

        /**
         * Defines a new texture coordinate with specified values.
         *
         * @param s The S-Coordinate.
         * @param t The T-Coordinate.
         */
        void texCoord(float s, float t);

        /**
         * Defines a new texture coordinate with specified values.
         *
         * @param texCoord The texture coordinate to define.
         */
        void texCoord(glm::vec2 texCoord);

        /**
         * Defines a new normal with specified values.
         *
         * @param nx The X-Coordinate.
         * @param ny The Y-Coordinate.
         * @param nz The Z-Coordinate.
         */
        void normal(float nx, float ny, float nz);

        /**
         * Defines a new normal with specified values.
         *
         * @param normal The normal to define.
         */
        void normal(glm::vec3 normal);

        /**
         * Sets the model matrix.
         *
         * @param model The model matrix.
         */
        void setModel(glm::mat4 model);

        /**
         * Gets the model matrix.
         *
         * @return The current model matrix.
         */
        glm::mat4 getModel();

        /**
         * Sets the view matrix.
         *
         * @param view The view matrix.
         */
        void setView(glm::mat4 view);

        /**
         * Gets the view matrix.
         *
         * @return The current view matrix.
         */
        glm::mat4 getView();

        /**
         * Sets the projection matrix.
         *
         * @param projection The projection matrix.
         */
        void setProjection(glm::mat4 projection);

        /**
         * Gets the projection matrix.
         *
         * @return The current projection matrix.
         */
        glm::mat4 getProjection();
    };
}

#endif /* BATCHING_HPP */
