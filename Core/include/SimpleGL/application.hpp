/**
 * @file application.hpp
 * @author Heiko Brumme
 *
 * This file contains the abstract Application class.
 */

#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include "SimpleGL_Export.h"

#include <vector>

#include "glfw_window.hpp"
#include "event.hpp"

namespace sgl {

    /**
     * @class Application
     *
     * This is the base class for an application.
     */
    class SIMPLEGL_EXPORT Application {

    protected:
        /** Tells if the application is running. */
        bool running;

        /** Stores the desired UPS. */
        int targetUps;

        /** Stores the desired FPS. */
        int targetFps;

        /** The window of this application. */
        Window* window;

        /** Initializes a new application. */
        Application(void);

    public:
        /** Destroys the application. */
        ~Application(void);

        /** Starts the application. */
        void run();

        /** Initializes the application. */
        void init();

        /** The main loop is a fixed timestep loop. */
        void mainLoop();

        /**
         * This method is intended to handle events like user input.
         *
         * @param events A list of events.
         */
        virtual void handleEvents(std::vector<Event*> events) = 0;

        /**
         * This method is intended to update objects of the application.
         *
         * @param delta The delta time.
         */
        virtual void updateScene(float delta) = 0;

        /** This method is intended to clear the color and depth buffers. */
        virtual void clear() = 0;

        /**
         * This method is intended to used for drawing the scene.
         *
         * @param alpha The alpha value.
         */
        virtual void renderScene(float alpha) = 0;

        /** Disposes the application. */
        virtual void dispose();
    };

    /**
     * @class TimeUtil
     *
     * This class contains static methods for time calculations.
     */
    class SIMPLEGL_EXPORT TimeUtil {

    private:
        /** The application time since the last loop. */
        static double lastLoopTime;

        /** The counter for FPS and UPS calculations. */
        static float timeCount;

        /** The current frame per second. */
        static int fps;

        /** The counter for the FPS. */
        static int fpsCount;

        /** The current updates per second. */
        static int ups;

        /** The counter for the UPS. */
        static int upsCount;

        /** Disable instantiation. */
        TimeUtil(void);

    public:
        /** Initializes the time utility. */
        static void init();

        /**
         * Returns the application time in seconds.
         *
         * @return The application time.
         */
        static double getTime();

        /**
         * Returns the time that have passed since the last loop.
         *
         * @return The delta time.
         */
        static float getDelta();

        /** Updates FPS and UPS if a whole second has passed. */
        static void update();

        /** Increments the FPS counter by one. */
        static void incFPS();

        /** Increments the UPS counter by one. */
        static void incUPS();

        /**
         * Returns the current frames per second.
         *
         * @return The current FPS.
         */
        static int getFPS();

        /**
         * Returns the current updates per second.
         *
         * @return The current UPS.
         */
        static int getUPS();

        /**
         * Returns the last loop time.
         *
         * @return The last loop time.
         */
        static double getLastLoopTime();
    };
}

#endif /* APPLICATION_HPP */
