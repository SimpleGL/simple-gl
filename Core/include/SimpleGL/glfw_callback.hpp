/**
 * @file glfw_callback.hpp
 * @author Heiko Brumme
 *
 * This file contains classes to wrap GLFW callbacks.
 */

#ifndef GLFW_CALLBACK_HPP
#define GLFW_CALLBACK_HPP

#include "SimpleGL_Export.h"

#include <string>

#include <GLFW/glfw3.h>

#include "input.hpp"

namespace sgl {

    /**
     * @class ErrorCallback
     * 
     * This class wraps a GLFW error callback.
     */
    class SIMPLEGL_EXPORT ErrorCallback {

    protected:
        /** Stores the current instance of the error callback. */
        static ErrorCallback* current;

    public:
        /**
         * Returns the current error callback.
         * 
         * @return The current error callback.
         */
        static ErrorCallback* getCurrent();

        /**
         * Sets the current error callback.
         * 
         * @param callback The error callback to set.
         */
        static void setCurrent(ErrorCallback* callback);

        /** 
         * Dispatches the member function.
         *
         * @param error An error code.
         * @param description A UTF-8 encoded string describing the error.
         */
        static void dispatch(int error, const char* description);

        /** Creates an error callback. */
        ErrorCallback(void);

        /** Destroys an error callback. */
        ~ErrorCallback(void);

        /** 
         * This function gets called whenever a GLFW error occurs.
         *
         * @param error An error code.
         * @param description Description of the error.
         */
        virtual void invoke(int error, std::string description);
    };

    /**
     * @class KeyCallback
     * 
     * This class wraps a GLFW key callback.
     */
    class SIMPLEGL_EXPORT KeyCallback {

    protected:
        /** Stores the current instance of the key callback. */
        static KeyCallback* current;

    public:
        /**
         * Returns the current key callback.
         * 
         * @return The current key callback.
         */
        static KeyCallback* getCurrent();

        /**
         * Sets the current key callback.
         * 
         * @param callback The key callback to set.
         */
        static void setCurrent(KeyCallback* callback);

        /**
         * Dispatches the member function.
         *
         * @param window The window which received the event.
         * @param key The key of the event.
         * @param scancode The system-specific scancode for the key.
         * @param action The action of the event.
         * @param mods A bit field that describes which modifier keys were active.
         */
        static void dispatch(GLFWwindow* window, int key, int scancode, int action, int mods);

        /** Creates a key callback. */
        KeyCallback(void);

        /** Destroys a key callback. */
        ~KeyCallback(void);

        /**
         * This function gets called whenever a key is pressed or released.
         *
         * @param window The window which received the event.
         * @param key The key of the event.
         * @param scancode The system-specific scancode for the key.
         * @param action The action of the event.
         * @param mods A bit field that describes which modifier keys were active.
         */
        virtual void invoke(GLFWwindow* window, Key key, int scancode, Action action, Modifier mods);
    };

    /**
     * @class MouseButtonCallback
     * 
     * This class wraps a GLFW mouse button callback.
     */
    class SIMPLEGL_EXPORT MouseButtonCallback {

    protected:
        /** Stores the current instance of the mouse button callback. */
        static MouseButtonCallback* current;

    public:
        /**
         * Returns the current mouse button callback.
         * 
         * @return The current mouse button callback.
         */
        static MouseButtonCallback* getCurrent();

        /**
         * Sets the current mouse button callback.
         * 
         * @param callback The mouse button callback to set.
         */
        static void setCurrent(MouseButtonCallback* callback);

        /**
         * Dispatches the member function.
         *
         * @param window The window which received the event.
         * @param button The button of the event.
         * @param action The action of the event.
         * @param mods A bit field that describes which modifier keys were active.
         */
        static void dispatch(GLFWwindow* window, int button, int action, int mods);

        /** Creates a mouse button callback. */
        MouseButtonCallback(void);

        /** Creates a mouse button callback. */
        ~MouseButtonCallback(void);

        /**
         * This function gets called whenever a mouse button is pressed or released.
         *
         * @param window The window which received the event.
         * @param button The button of the event.
         * @param action The action of the event.
         * @param mods A bit field that describes which modifier keys were active.
         */
        virtual void invoke(GLFWwindow* window, MouseButton button, Action action, Modifier mods);
    };
}

#endif /* GLFW_CALLBACK_HPP */
