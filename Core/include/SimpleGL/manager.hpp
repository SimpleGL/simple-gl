/**
 * @file manager.hpp
 * @author Heiko Brumme
 *
 * This file contains the base class for managing objects.
 */

#ifndef MANAGER_HPP
#define MANAGER_HPP

#include "SimpleGL_Export.h"

#include <map>
#include <string>

namespace sgl {

    /**
     * @class Manager
     *
     * This class provides an abstract class for managing objects.
     */
    template <class T>
    class Manager {

    protected:
        /** Contains the objects to manage. */
        std::map<std::string, T*> objects;

        /** Initializes a new manager. */
        Manager(void);

    public:
        /** Deletes a manager and disposes all managed objects. */
        ~Manager(void);

        /**
         * Stores a object with specified name.
         *
         * @param name The name for the object.
         * @param value The object to store.
         */
        void put(std::string name, T* value);

        /**
         * Gets a managed object from the storage.
         *
         * @param name The name of the object.
         *
         * @return The stored object associated to specified name.
         */
        T* get(std::string name);

        /**
         * Removes and disposes an object from the storage.
         *
         * @param name The name of the object.
         */
        virtual void remove(std::string name);

        /** Removes all objects and disposes them. */
        virtual void clear();
    };
}

#include "manager.tcc"

#endif /* MANAGER_HPP */
