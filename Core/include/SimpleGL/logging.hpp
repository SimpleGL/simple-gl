/**
 * @file logging.hpp
 * @author Heiko Brumme
 *
 * This file contains classes to do some sort of output.
 */

#ifndef LOGGING_HPP
#define LOGGING_HPP

#include "SimpleGL_Export.h"

#include <string>

namespace sgl {

    /**
     * @class Logger
     *
     * This class is used to log information and errors.
     */
    class SIMPLEGL_EXPORT Logger {

    private:
        /** Disable instantiation. */
        Logger(void);

    public:
        /**
         * Logs an information string to cout.
         *
         * @param message The message to log.
         */
        static void logInfo(std::string message);

        /**
         * Logs an error string to cerr.
         *
         * @param message The message to log.
         */
        static void logError(std::string message);
    };
}

#endif /* LOGGING_HPP */
