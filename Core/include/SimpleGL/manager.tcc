/**
 * @file manager.tcc
 * @author Heiko Brumme
 *
 * This files contains the template implementation of the manager.hpp header.
 */

#include "SimpleGL/manager.hpp"

using namespace std;

namespace sgl {

    template <class T>
    Manager<T>::Manager(void) {
        /* Nothing to do here */
    }

    template <class T>
    Manager<T>::~Manager(void) {
        /* Clear map */
        clear();
    }

    template <class T>
    void Manager<T>::put(std::string name, T* value) {
        /* If key already exist, dispose the old object */
        if (objects.count(name)) {
            remove(name);
        }

        /* Insert new object */
        objects.insert(make_pair(name, value));
    }

    template <class T>
    T* Manager<T>::get(std::string name) {
        return objects.at(name);
    }

    template <class T>
    void Manager<T>::remove(std::string name) {
        objects.erase(name);
    }

    template <class T>
    void Manager<T>::clear() {
        objects.clear();
    }
}
