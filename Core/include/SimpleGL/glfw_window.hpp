/**
 * @file glfw_window.hpp
 * @author Heiko Brumme
 *
 * This file contains classes to generate a GLFW window.
 */

#ifndef GLFW_WINDOW_HPP
#define GLFW_WINDOW_HPP

#include "SimpleGL_Export.h"

#include <string>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "glfw_callback.hpp"

namespace sgl {

    /**
     * @class Window
     *
     * This class provides a basic GLFW window.
     */
    class SIMPLEGL_EXPORT Window {

    protected:
        /** The handle for the GLFW window. */
        GLFWwindow* handle;

        /** The size of the drawing area. */
        glm::ivec2 size;

        /** The title for the GLFW window. */
        std::string title;

        /** Initializes a new window. */
        Window(void);

    public:
        /** Destroys the window. */
        ~Window(void);

        /**
         * Returns the handle of the window.
         * 
         * @return The handle of the window.
         */
        GLFWwindow* getHandle();

        /**
         * Returns the width of the drawing area.
         *
         * @return The width of the drawing area.
         */
        int getWidth();

        /**
         * Sets the width of the drawing area.
         *
         * @param width The new width of the drawing area.
         */
        void setWidth(int width);

        /**
         * Returns the height of the drawing area.
         *
         * @return The height of the drawing area.
         */
        int getHeight();

        /**
         * Sets the height of the drawing area.
         *
         * @param height The new height of the drawing area.
         */
        void setHeight(int height);

        /**
         * Returns the size of the drawing area.
         * 
         * @return The size of the drawing area.
         */
        glm::ivec2 getSize();

        /**
         * Sets the size of the drawing area.
         * 
         * @param size The new size of the drawing area.
         */
        void setSize(glm::ivec2 size);

        /**
         * Returns the window title.
         *
         * @return The Window title.
         */
        std::string getTitle();

        /**
         * Sets this windows title.
         *
         * @param title The new title of the window.
         */
        void setTitle(std::string title);

        /** Shows informations in the title. */
        void showInfoTitle();

        /** Centers the window on the primary screen. */
        void centerWindow();

        /**
         * Set the key callback.
         *
         * @param callback Pointer to the key callback.
         */
        void setKeyCallback(KeyCallback* callback);

        /**
         * Set the mouse button callback.
         *
         * @param callback Pointer to the mouse button callback.
         */
        void setMouseButtonCallback(MouseButtonCallback* callback);

        /** Swaps the framebuffers of this window. */
        virtual void swapBuffers() = 0;

        /** Convenience method to swap buffers and poll events in one call. */
        void update();

        /**
         * Checks the close flag for the window.
         *
         * @return true, if the window should get closed, else false.
         */
        bool shouldClose();

        /**
         * Syncs the window framerate to specified FPS.
         *
         * @param fps The desired FPS.
         */
        void sync(int fps);

        /**
         * Gets the cursor position.
         *
         * @return A vector that stores the position.
         */
        glm::dvec2 getCursorPos();

        /**
         * Gets the cursor's  X-position.
         *
         * @return X coordinate of the cursor.
         */
        double getCursorX();

        /**
         * Gets the cursor's Y-position.
         *
         * @return Y coordinate of the cursor.
         */
        double getCursorY();
    };
}

#endif /* GLFW_WINDOW_HPP */
