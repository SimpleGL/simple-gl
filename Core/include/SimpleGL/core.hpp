/**
 * @file core.hpp
 * @author Heiko Brumme
 *
 * This file contains some general functions for the core framework.
 */

#ifndef CORE_HPP
#define CORE_HPP

#include "SimpleGL_Export.h"

/**
 * @namespace sgl
 *
 * Generic namespace for the SimpleGL framework.
 */
namespace sgl {

    /** Initializes SimpleGL and should be called before anything else. */
    SIMPLEGL_EXPORT void init();
}

#endif /* CORE_HPP */
