/**
 * @file glfw_general.hpp
 * @author Heiko Brumme
 *
 * This file contains general methods for GLFW.
 */

#ifndef GLFW_GENERAL_HPP
#define GLFW_GENERAL_HPP

#include "SimpleGL_Export.h"

#include "glfw_callback.hpp"

namespace sgl {

    /**
     * @class GLFW
     *
     * This static class wraps general GLFW functions.
     */
    class SIMPLEGL_EXPORT GLFW {

    private:
        /** Disable instantiation. */
        GLFW(void);

    public:
        /**
         * Set the error callback.
         *
         * @param callback Pointer to the error callback.
         */
        static void setErrorCallback(ErrorCallback* callback);

        /** Polls the events and calls the registered callbacks. */
        static void pollEvents();

        /** Terminates GLFW, this should only be called at the end of the application. */
        static void terminate();
    };
}

#endif /* GLFW_GENERAL_HPP */
