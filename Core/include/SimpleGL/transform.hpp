/**
 * @file transform.hpp
 * @author Heiko Brumme
 *
 * This file contains classes for calculating the MVP matrix.
 */

#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#include "SimpleGL_Export.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

namespace sgl {

    /**
     * @struct Camera
     *
     * This struct defines a camera.
     */
    struct SIMPLEGL_EXPORT Camera {

        /** The position of the camera. */
        glm::vec3 position;

        /** The front side direction of the camera. */
        glm::vec3 front;

        /** The up vector if the camera. */
        glm::vec3 up;
    };

    /**
     * @struct Transformation
     *
     * This struct contains information to calculate the Model-View-Projection-Matrix.
     */
    struct SIMPLEGL_EXPORT Transformation {

        /** The model matrix. */
        glm::mat4 model;

        /** The view matrix. */
        glm::mat4 view;

        /** The projection matrix. */
        glm::mat4 projection;

        /** The clip matrix. */
        glm::mat4 clip;

        /**
         * Tells if the transformation has changed.
         * This should be set to true after setting a new model, view or projection matrix.
         */
        bool changed;
    };

    /**
     * @class Model
     *
     * This class contains static methods to calculate model matrices.
     */
    class SIMPLEGL_EXPORT Model {

    private:
        /** Disable instantiation. */
        Model(void);

    public:
        /**
         * Returns an identity matrix.
         *
         * @return The identity matrix.
         */
        static glm::mat4 identity();

        /**
         * Calculates a nonuniform scaling matrix.
         *
         * @param x Specifies the scale factor along the x axis.
         * @param y Specifies the scale factor along the y axis.
         * @param z Specifies the scale factor along the z axes.
         *
         * @return The calculated scaling matrix.
         */
        static glm::mat4 scale(float x, float y, float z);

        /**
         * Calculates a nonuniform scaling matrix.
         *
         * @param vector Specifies the scale factors along the x, y, and z axes.
         *
         * @return The calculated scaling matrix.
         */
        static glm::mat4 scale(glm::vec3 vector);

        /**
         * Calculates a nonuniform scaling matrix.
         *
         * @param matrix This matrix is multiplied by the scale matrix.
         * @param x Specifies the scale factor along the x axis.
         * @param y Specifies the scale factor along the y axis.
         * @param z Specifies the scale factor along the z axes.
         *
         * @return The calculated scaling matrix.
         */
        static glm::mat4 scale(glm::mat4 matrix, float x, float y, float z);

        /**
         * Calculates a nonuniform scaling matrix.
         *
         * @param matrix This matrix is multiplied by the scale matrix.
         * @param vector Specifies the scale factors along the x, y, and z axes.
         *
         * @return The calculated scaling matrix.
         */
        static glm::mat4 scale(glm::mat4 matrix, glm::vec3 vector);

        /**
         * Calculates a translation matrix.
         *
         * @param x Specifies the X-coordinates of a translation vector.
         * @param y Specifies the Y-coordinates of a translation vector.
         * @param z Specifies the Z-coordinates of a translation vector.
         *
         * @return The calculated translation matrix.
         */
        static glm::mat4 translate(float x, float y, float z);

        /**
         * Calculates a translation matrix.
         *
         * @param vector Specifies the translation vector.
         *
         * @return The calculated translation matrix.
         */
        static glm::mat4 translate(glm::vec3 vector);

        /**
         * Calculates a translation matrix.
         *
         * @param matrix This matrix is multiplied by the translation matrix.
         * @param x Specifies the X-coordinates of a translation vector.
         * @param y Specifies the Y-coordinates of a translation vector.
         * @param z Specifies the Z-coordinates of a translation vector.
         *
         * @return The calculated translation matrix.
         */
        static glm::mat4 translate(glm::mat4 matrix, float x, float y, float z);

        /**
         * Calculates a translation matrix.
         *
         * @param matrix This matrix is multiplied by the translation matrix.
         * @param vector Specifies the translation vector.
         *
         * @return The calculated translation matrix.
         */
        static glm::mat4 translate(glm::mat4 matrix, glm::vec3 vector);

        /**
         * Calculates a rotation of angle degrees around the vector (x y z).
         *
         * @param angle Specifies the angle of rotation, in degrees.
         * @param x Specifies the X-coordinates of a vector.
         * @param y Specifies the Y-coordinates of a vector.
         * @param z Specifies the Z-coordinates of a vector.
         *
         * @return The calculated rotation matrix.
         */
        static glm::mat4 rotate(float angle, float x, float y, float z);

        /**
         * Calculates a rotation of angle degrees around the vector (x y z).
         *
         * @param angle Specifies the angle of rotation, in degrees.
         * @param vector Specify the x, y, and z coordinates of a vector.
         *
         * @return The calculated rotation matrix.
         */
        static glm::mat4 rotate(float angle, glm::vec3 vector);

        /**
         * Calculates a rotation of angle degrees around the vector (x y z).
         *
         * @param matrix This matrix is multiplied by the rotation matrix.
         * @param angle Specifies the angle of rotation, in degrees.
         * @param x Specifies the X-coordinates of a vector.
         * @param y Specifies the Y-coordinates of a vector.
         * @param z Specifies the Z-coordinates of a vector.
         *
         * @return The calculated rotation matrix.
         */
        static glm::mat4 rotate(glm::mat4 matrix, float angle, float x, float y, float z);

        /**
         * Calculates a rotation of angle degrees around the vector (x y z).
         *
         * @param matrix This matrix is multiplied by the rotation matrix.
         * @param angle Specifies the angle of rotation, in degrees.
         * @param vector Specify the x, y, and z coordinates of a vector.
         *
         * @return The calculated rotation matrix.
         */
        static glm::mat4 rotate(glm::mat4 matrix, float angle, glm::vec3 vector);
    };

    /**
     * @class View
     *
     * This class contains static methods to calculate view matrices.
     */
    class SIMPLEGL_EXPORT View {

    private:
        /** Disable instantiation. */
        View(void);

    public:
        /**
         * Returns an identity matrix.
         *
         * @return The identity matrix.
         */
        static glm::mat4 identity();

        /**
         * Calculates a viewing matrix derived from an eye point, a reference point indicating the center of the scene, and an UP vector.
         *
         * @param eye Specifies the position of the eye point.
         * @param center Specifies the position of the reference point.
         * @param up Specifies the direction of the up vector.
         *
         * @return The calculated lookAt-Matrix.
         */
        static glm::mat4 lookAt(glm::vec3 eye, glm::vec3 center, glm::vec3 up);

        /**
         * Calculates a viewing matrix derived from an eye point, a reference point indicating the center of the scene, and an UP vector.
         *
         * @param eyeX Specifies the X-position of the eye point.
         * @param eyeY Specifies the Y-position of the eye point.
         * @param eyeZ Specifies the Z-position of the eye point.
         * @param centerX Specifies the X-position of the reference point.
         * @param centerY Specifies the Y-position of the reference point.
         * @param centerZ Specifies the Z-position of the reference point.
         * @param upX Specifies the X-direction of the up vector.
         * @param upY Specifies the Y-direction of the up vector.
         * @param upZ Specifies the Z-direction of the up vector.
         *
         * @return The calculated lookAt-Matrix.
         */
        static glm::mat4 lookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ);

        /**
         * Calculates a viewing matrix from a camera.
         *
         * @param camera The camera to use.
         */
        static glm::mat4 lookAt(Camera camera);
    };

    /**
     * @class Projection
     *
     * This class contains static methods to calculate projection matrices.
     */
    class SIMPLEGL_EXPORT Projection {

    private:
        /** Disable instantiation. */
        Projection(void);

    public:
        /**
         * Returns an identity matrix.
         *
         * @return The identity matrix.
         */
        static glm::mat4 identity();

        /**
         * Calculates a transformation that produces a parallel projection.
         *
         * @param left Specify the coordinates for the left vertical clipping planes.
         * @param right Specify the coordinates for the right vertical clipping planes.
         * @param bottom Specify the coordinates for the bottom horizontal clipping planes.
         * @param top Specify the coordinates for the top horizontal clipping planes.
         * @param zNear Specify the distances to the nearer depth clipping planes. This value is negative if the plane is to be behind the viewer.
         * @param zFar Specify the distances to the farther depth clipping planes. This value is negative if the plane is to be behind the viewer.
         *
         * @return The calculated orthographic projection matrix.
         */
        static glm::mat4 ortho(float left, float right, float bottom, float top, float zNear, float zFar);

        /**
         * Calculates a perspective matrix that produces a perspective projection.
         *
         * @param left Specify the coordinates for the left vertical clipping planes.
         * @param right Specify the coordinates for the right vertical clipping planes.
         * @param bottom Specify the coordinates for the bottom horizontal clipping planes.
         * @param top Specify the coordinates for the top horizontal clipping planes.
         * @param zNear Specify the distances to the near depth clipping planes. Must be positive.
         * @param zFar Specify the distances to the far depth clipping planes. Must be positive.
         *
         * @return The calculated frustum projection matrix.
         */
        static glm::mat4 frustum(float left, float right, float bottom, float top, float zNear, float zFar);

        /**
         * Calculates a viewing frustum into the world coordinate system.
         *
         * @param fovy Specifies the field of view angle, in degrees, in the y direction.
         * @param aspect Specifies the aspect ratio that determines the field of view in the x direction. The aspect ratio is the ratio of x (width) to y (height).
         * @param zNear Specifies the distance from the viewer to the near clipping plane (always positive).
         * @param zFar Specifies the distance from the viewer to the far clipping plane (always positive).
         *
         * @return The calculated perspective matrix.
         */
        static glm::mat4 perspective(float fovy, float aspect, float zNear, float zFar);
    };
}

#endif /* TRANSFORM_HPP */
