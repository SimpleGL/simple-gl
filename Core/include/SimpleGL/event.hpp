/**
 * @file event.hpp
 * @author Heiko Brumme
 *
 * This file contains classes for events.
 */

#ifndef EVENT_HPP
#define EVENT_HPP

#include "SimpleGL_Export.h"

#include <vector>

#include "input.hpp"

namespace sgl {

    /**
     * @enum EventType
     *
     * This enum contains different event types.
     */
    enum class EventType {

        /** Constant for a key event.
         * This event gets created by the key callback.
         */
        KEY_EVENT,

        /** Constant for a mouse button event.
         * This event get created by the mouse button callback.
         */
        MOUSE_BUTTON_EVENT
    };

    /**
     * @class Event
     *
     * This abstract class defines a event for the application.
     */
    class SIMPLEGL_EXPORT Event {

    protected:
        /** Stores the events that got received. */
        static std::vector<Event*> events;

        /** Stores the event type. */
        EventType type;

    public:
        /**
         * Returns the list of events.
         *
         * @return The list of events.
         */
        static std::vector<Event*> getEvents();

        /**
         * Adds an event to the list.
         *
         * @param event The event to add.
         */
        static void addEvent(Event* event);

        /** Clears the event list, this should be called after every loop. */
        static void clearEvents();

        /**
         * Creates a new event.
         *
         * @param type The event type of this event.
         */
        Event(EventType type);

        /** Deletes a event. */
        virtual ~Event(void);

        /**
         * Returns the event type.
         *
         * @return The event type.
         */
        EventType getType();
    };

    /**
     * @class KeyEvent
     *
     * This class defines a key event, created by the KeyCallback.
     */
    class SIMPLEGL_EXPORT KeyEvent : public Event {

    private:
        /** Stores the key of this event. */
        Key key;

        /** Stores the system-specific scancode for the key. */
        int scancode;

        /** Stores the action of this event. */
        Action action;

        /** Stores the modifier bits of this event. */
        Modifier mods;

    public:
        /**
         * Creates a new key event.

         * @param key The key of the event.
         * @param scancode The system-specific scancode for the key.
         * @param action The action of the event.
         * @param mods A bit field that describes which modifier keys were active.
         */
        KeyEvent(Key key, int scancode, Action action, Modifier mods);

        /** Deletes a key event. */
        ~KeyEvent(void) override;

        /**
         * Returns the key of the event.
         *
         * @return The key of the event.
         */
        Key getKey();

        /**
         * Returns the system-specific scancode for the key.
         *
         * @return The system-specific scancode for the key.
         */
        int getScancode();

        /**
         * Returns the action of the event.
         *
         * @return The action of the event.
         */
        Action getAction();

        /**
         * Returns the modifier bits of the event.
         *
         * @return The modifier bits of the event.
         */
        Modifier getMods();
    };

    /**
     * @class MouseButtonEvent
     *
     * This class defines a mouse button event, created by the MouseButtonCallback.
     */
    class SIMPLEGL_EXPORT MouseButtonEvent : public Event {

    private:
        /** Stores the mouse button of this event. */
        MouseButton button;

        /** Stores the action of this event. */
        Action action;

        /** Stores the modifier bits of this event. */
        Modifier mods;

    public:
        /**
         * Creates a new mouse button event.
         *
         * @param button The button of the event.
         * @param action The action of the event.
         * @param mods A bit field that describes which modifier keys were active.
         */
        MouseButtonEvent(MouseButton button, Action action, Modifier mods);

        /** Deletes a mouse button event. */
        ~MouseButtonEvent(void) override;

        /**
         * Returns the mouse button of the event.
         *
         * @return The mouse button of the event.
         */
        MouseButton getButton();

        /**
         * Returns the action of the event.
         *
         * @return The action of the event.
         */
        Action getAction();

        /**
         * Returns the modifier bits of the event.
         *
         * @return The modifier bits of the event.
         */
        Modifier getMods();
    };
}

#endif /* EVENT_HPP */
