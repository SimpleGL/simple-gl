/**
 * @file lighting.hpp
 * @author Heiko Brumme
 *
 * This file contains the structures for lighting.
 */

#ifndef LIGHTING_HPP
#define LIGHTING_HPP

#include <glm/glm.hpp>

namespace sgl {

    /**
     * @struct Material
     *
     * This structure defines a material.
     */
    struct Material {

        /** The ambient reflection factor. */
        glm::vec4 ambient;

        /** The diffuse reflection factor. */
        glm::vec4 diffuse;

        /** The specular reflection factor. */
        glm::vec4 specular;

        /** The Phong-Exponent. */
        float shininess;

        /** The Self-Illuminance. */
        glm::vec4 emission;
    };

    /**
     * @struct Light
     *
     * This structure defines a light.
     */
    struct Light {

        /** The ambient reflection factor. */
        glm::vec4 ambient;

        /** The diffuse reflection factor. */
        glm::vec4 diffuse;

        /** The specular reflection factor. */
        glm::vec4 specular;

        /** The light position. */
        glm::vec4 position;

        /** The spot direction. */
        glm::vec3 direction;

        /** The spot exponent. */
        float exponent;

        /** The spot angle, in degrees. */
        float cutOff;

        /** The constant attenuation. */
        float constant;

        /** The linear attenuation. */
        float linear;

        /** The quadratic attenuation. */
        float quadratic;
    };
}

#endif /* LIGHTING_HPP */
