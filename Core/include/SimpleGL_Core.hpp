/**
 * @file SimpleGL_Core.hpp
 * @author Heiko Brumme
 *
 * This file is meant to get included by projects using this framework.
 *
 * In this file all the headers of the core framework should get included, so
 * that the user just have to include this header.
 */

#ifndef SIMPLEGL_CORE_HPP
#define SIMPLEGL_CORE_HPP

/* SimpleGL core */
#include "SimpleGL/core.hpp"
#include "SimpleGL/logging.hpp"
#include "SimpleGL/application.hpp"
#include "SimpleGL/input.hpp"
#include "SimpleGL/event.hpp"
#include "SimpleGL/batching.hpp"
#include "SimpleGL/transform.hpp"
#include "SimpleGL/lighting.hpp"
#include "SimpleGL/manager.hpp"

/* GLFW wrapper */
#include "SimpleGL/glfw_general.hpp"
#include "SimpleGL/glfw_window.hpp"
#include "SimpleGL/glfw_callback.hpp"

#endif /* SIMPLEGL_CORE_HPP */
