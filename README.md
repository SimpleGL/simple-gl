# SimpleGL - A framework for platform independent rendering

This repository hosts the SimpleGL project. You can find the online documentation of this framework [here](http://simplegl.bitbucket.io/).

SimpleGL is licensed under the MIT License, which can be found [here](LICENSE.md).

- For an overview of the source of the core framework you may take a look into the [Core](Core/) folder.
- You can find the source for the backends in the [Backends](Backends/) folder.
- The documentation source can be found in the [Documentation](https://bitbucket.org/SimpleGL/simplegl.bitbucket.io/) submodule.
- Some example projects can be found in the [Examples](Examples/) folder.
