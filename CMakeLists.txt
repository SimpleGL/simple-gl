cmake_minimum_required(VERSION 3.0.2)
add_compile_options(-std=c++11)

# Project configuration
project(SimpleGL)
set(SimpleGL_VERSION_MAJOR 1)
set(SimpleGL_VERSION_MINOR 1)
set(SimpleGL_VERSION_REVISION 0)
set(SimpleGL_Core_DIR ${PROJECT_SOURCE_DIR}/Core)
set(SimpleGL_Backends_DIR ${PROJECT_SOURCE_DIR}/Backends)
set(SimpleGL_Examples_DIR ${PROJECT_SOURCE_DIR}/Examples)
set(SimpleGL_Documentation_DIR ${PROJECT_SOURCE_DIR}/Documentation)
set(SimpleGL_OUTPUT_DIR ${PROJECT_SOURCE_DIR}/out)

# Options
option(BUILD_SHARED_LIBS "Build SimpleGL as an shared library" ON)
option(SimpleGL_BUILD_EXAMPLES "Build the SimpleGL example applications" ON)
option(SimpleGL_BUILD_DOCUMENTATION "Build the SimpleGL documentation (requires Doxygen)" OFF)

# Add the core framework
add_subdirectory(${SimpleGL_Core_DIR})

# Add the backends
add_subdirectory(${SimpleGL_Backends_DIR})

# Add the examples
if(SimpleGL_BUILD_EXAMPLES)
    add_subdirectory(${SimpleGL_Examples_DIR})
endif(SimpleGL_BUILD_EXAMPLES)

# Add the documentation
if(SimpleGL_BUILD_DOCUMENTATION)
    add_subdirectory(${SimpleGL_Documentation_DIR})
endif(SimpleGL_BUILD_DOCUMENTATION)
